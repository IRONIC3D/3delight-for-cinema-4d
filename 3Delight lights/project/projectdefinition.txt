// Supported platforms - can be [Win64;OSX]
Platform=Win64;OSX

// Type of project - can be [Lib;DLL;App]
Type=DLL

// API dependencies
APIS=cinema.framework;misc.framework;core.framework;

// C4D component
C4D=true

stylecheck.level=0
//stylecheck.enum-registration=false
//stylecheck.enum-class=false

// Custom ID
ModuleId=com.3delightfc4d.lights

// Headers VS
AdditionalIncludeDirectories=\
    $(DELIGHT)/include;\
    ../../3Delight/API/include;\
    ../../3Delight/source;\
    ../../3Delight shaders/source;\
    ../../common

// Headers XCode
HEADER_SEARCH_PATHS=\
    /Applications/3Delight/include;\
    ../../3Delight/API/include;\
    ../../3Delight/source;\
    ../../3Delight shaders/source;\
    ../../common

// the following files should be added to the Visual Studio project
Include.Win=\
    ../../common/3DelightEnvironment.cpp;\
    ../../common/mnemonic.cpp;\
    ../../common/mn_wordlist.cpp;

// the following files should be added to the Xcode project
Include.OSX=\
    ../../common/3DelightEnvironment.cpp;\
    ../../common/mnemonic.cpp;\
    ../../common/mn_wordlist.cpp;

DontUncrustify="main.cpp"
