#include "IDs.h"
#include "c4d.h"
#include "c4d_symbols.h"
#include "dlgobofilter.h"

class DLGoboFilter : public ShaderData
{
public:
	virtual Bool Init(GeListNode* node);
	virtual Bool GetDDescription(GeListNode* node,
								 Description* description,
								 DESCFLAGS_DESC& flags);
	virtual Bool Message(GeListNode* node, Int32 type, void* data);
	virtual Vector Output(BaseShader* sh, ChannelData* sd);
	static NodeData* Alloc(void)
	{
		return NewObjClear(DLGoboFilter);
	}
};

Bool DLGoboFilter::Init(GeListNode* node)
{
	BaseContainer* data = ((BaseShader*) node)->GetDataInstance();
	data->SetFloat(GOBO_DENSITY, 1.0);
	data->SetBool(GOBO_INVERT, FALSE);
	data->SetFloat(GOBO_SCALE_START, 1.0);
	data->SetFloat(GOBO_SCALE_END, 1.0);
	data->SetFloat(GOBO_OFFSET_START, 0.0);
	data->SetFloat(GOBO_OFFSET_END, 0.0);
	data->SetInt32(S_WRAP_MODE, S_WRAP_CLAMP);
	data->SetInt32(T_WRAP_MODE, T_WRAP_CLAMP);
	data->SetInt32(GOBO_USE_FILTER, TRUE);
	data->SetInt32(IS_SELECTED, FALSE);
	return true;
}

Bool DLGoboFilter::GetDDescription(GeListNode* node,
								   Description* description,
								   DESCFLAGS_DESC& flags)
{
	description->LoadDescription(DL_GOBOFILTER);
	flags |= DESCFLAGS_DESC::LOADED;
	BaseContainer* dldata = ((BaseObject*) node)->GetDataInstance();
	return TRUE;
}

Bool DLGoboFilter::Message(GeListNode* node, Int32 type, void* data)
{
	if (!node) {
		return false;
	}

	BaseDocument* doc = GetActiveDocument();
	BaseContainer* dldata = ((BaseMaterial*) node)->GetDataInstance();
	return true;
}

Vector
DLGoboFilter::Output(BaseShader* sh, ChannelData* sd)
{
	return Vector(0, 0, 0);
}

Bool RegisterGoboFilter(void)
{
	return RegisterShaderPlugin(DL_GOBOFILTER,
								GeLoadString(ID_GOBO_NAME),
								PLUGINFLAG_HIDE,
								DLGoboFilter::Alloc,
								"dlgobofilter"_s,
								0);
}
