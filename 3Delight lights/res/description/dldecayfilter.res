CONTAINER dldecayfilter
{
	NAME dldecayfilter;
	INCLUDE Obase;

	GROUP DECAY_FILTER
	{
		SEPARATOR {LINE;}
		GROUP
		{			
			COLUMNS 2;
			LONG DECAY_TYPE
			{
				SCALE_H;
				CYCLE
				{
					DECAY_TYPE_DISTANCE_LIGHT;
					DECAY_TYPE_DISTANCE_LIGHT_PLANE;
					DECAY_TYPE_ANGLE_AXIS;
					DECAY_TYPE_DISTANCE_AXIS;
				}
			}
			SEPARATOR{}
			
			REAL DECAY_RANGE_START {MIN 0;}
			REAL DECAY_RANGE_END {MIN 0;}
			SEPARATOR{}
			
			STATICTEXT{}
			STATICTEXT{}
		}
		GROUP
		{
			SPLINE DECAY_RAMP
			{
			  SHOWGRID_H;
			  SHOWGRID_V;
			  EDIT_H;
			  EDIT_V;
			  NO_FLOATING_WINDOW;
			}
		}
	}
}
