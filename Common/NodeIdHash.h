#ifndef NODE_ID_TO_HANDLE_H
#define NODE_ID_TO_HANDLE_H
// Human readable node names via mnemonic hashes.
// See https:// github.com/singpolyma/mnemonicode
#include <cassert>
#include "mnemonic.h"

#ifdef DEBUG
static __int128 stable_id_part = 0;
#    define FIRST_96_BITS(x) ((*(__int128*) x) & 0x0000FFFFFFFFFFFF)
#endif

const char b64_chars[] =
	"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

template<typename T>
inline T
B64Encode(const unsigned char* in, size_t len)
{
	if (in == NULL || len == 0) {
		return T();
	}

	size_t e_len = len;

	if (len % 3 != 0) {
		e_len += 3 - (len % 3);
	}

	e_len /= 3;
	e_len *= 4;
	assert(e_len < 256);
	char out[256];
	out[e_len] = '\0';

	for (size_t i = 0, j = 0; i < len; i += 3, j += 4) {
		size_t v = in[i];
		v = i + 1 < len ? v << 8 | in[i + 1] : v << 8;
		v = i + 2 < len ? v << 8 | in[i + 2] : v << 8;
		out[j] = b64_chars[(v >> 18) & 0x3F];
		out[j + 1] = b64_chars[(v >> 12) & 0x3F];

		if (i + 1 < len) {
			out[j + 2] = b64_chars[(v >> 6) & 0x3F];

		} else {
			out[j + 2] = '=';
		}

		if (i + 2 < len) {
			out[j + 3] = b64_chars[v & 0x3F];

		} else {
			out[j + 3] = '=';
		}
	}

	return T(out);
}

template<typename T>
inline T
NodeIdToHandle(BaseList2D* node)
{
	if (!node) {
		return T();
	}

	Int mem_size = 0;
	const Char* mem;
	// Returns a 128 bit node ID. The first 96 bits seem to always be the same.
	node->FindUniqueID(MAXON_CREATOR_ID, mem, mem_size);
#ifdef DEBUG
	// FIXME: once we're reasoanbly sure we always get the same first 96 bits
	// this section can be removed.
	assert(16 == mem_size);

	// Check if the first 96 bits really match anything we've seen before.
	if (!stable_id_part) {
		stable_id_part = FIRST_96_BITS(mem);

	} else {
		if (stable_id_part != FIRST_96_BITS(mem)) {
			printf("[3DfC4D] WARNING: DIFFERING NODE ID PART!");
		}

		// else: we're good
	}

	char buffer[256];
	// We ignore the first 96 bits (12 bytes) of the ID (see above).
	int len = mn_encode(
				  (void*) (mem + 12), mem_size - 12, buffer, sizeof(buffer), "x_x_x");
	return T(buffer, len) + "::";
#else
	return B64Encode<T>((const unsigned char*) mem, mem_size) + "::";
#endif
}

#endif
