#ifndef BASEOBJECTTRANSLATOR_H_
#define BASEOBJECTTRANSLATOR_H_
#include <vector>
#include "DL_API.h"
#include "c4d.h"
//#include "Edge.h"
#include <map>
#include <string>
class BaseObjectTranslator : public DL_TranslatorPlugin
{
public:
	virtual void CreateNSINodes(const char* Handle,
								const char* ParentTransformHandle,
								BaseList2D* C4DNode,
								BaseDocument* doc,
								DL_SceneParser* parser);
	virtual void SampleAttributes(DL_SampleInfo* info,
								  const char* Handle,
								  BaseList2D* C4DNode,
								  BaseDocument* doc,
								  DL_SceneParser* parser);
	PolygonObject* ParseObjectToPolygon(BaseObject* obj);
};
#endif // !BASEOBJECTTRANSLATOR_H_
