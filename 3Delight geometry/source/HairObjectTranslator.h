#pragma once

#include "DL_API.h"
#include "c4d.h"
#include "lib_hair.h"

class HairObjectTranslator : public DL_TranslatorPlugin
{
public:
	// virtual void Init(BaseList2D* C4DNode, BaseDocument* doc, DL_SceneParser*
	// parser);
	virtual void CreateNSINodes(const char* Handle,
								const char* ParentTransformHandle,
								BaseList2D* C4DNode,
								BaseDocument* doc,
								DL_SceneParser* parser);
	virtual void SampleAttributes(DL_SampleInfo* info,
								  const char* Handle,
								  BaseList2D* C4DNode,
								  BaseDocument* doc,
								  DL_SceneParser* parser);
};
