#include <string.h>
#include "DL_API.h"
#include "HairObjectTranslator.h"
#include "InstanceObjectTranslator.h"
#include "PolygonObjectTranslator.h"
#include "c4d.h"

Bool PluginStart(void)
{
	return true;
}

void PluginEnd(void) {}

Bool PluginMessage(Int32 id, void* data)
{
	switch (id) {
	case C4DPL_INIT_SYS:
		if (!g_resource.Init()) {
			return FALSE; // don't start plugin without resource
		}

		return TRUE;
		break;

	case DL_LOAD_PLUGINS:
		DL_PluginManager* pm = (DL_PluginManager*) data;
		// pm->RegisterHook(AllocateHook<SkyHook>);
		pm->RegisterTranslator(Opolygon,
							   AllocateTranslator<PolygonObjectTranslator>);
		pm->RegisterTranslator(Ohair, AllocateTranslator<HairObjectTranslator>);
		pm->RegisterTranslator(Oinstance,
							   AllocateTranslator<InstanceObjectTranslator>);
		// ApplicationOutput("Ohair = " + String::IntToString(Ohair));
		break;
	}

	return FALSE;
}
