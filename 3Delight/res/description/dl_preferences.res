CONTAINER dl_preferences
{
	NAME dl_preferences;


	GROUP
	{
        LONG DL_RENCER_VIEW 
		{ 

        	CYCLE
        	{
        		DELIGHT_DISPLAY_VIEW;
        		CINEMA4D_RENDER_VIEW;
        		//CUSTOM_FRAME_BUFFER;
        	}
        }
       LONG DL_LIVE_RENDER_COARSENESS 
		{
			CYCLE
        	{
        		TWO_PIXELS;
        		FOUR_PIXELS;
        		EIGHT_PIXELS;
        	}
		}
		
        LONG DL_SCANNING 
		{
			CYCLE
        	{
        		CIRCLE;
        		SPIRAL;
        		HORIZONTAL;
        		VERTICAL;
        		ZIGZAG;
        	}
		}
		
		BOOL DL_PROGRESSIVE_REFINEMENT{}
	}
}
