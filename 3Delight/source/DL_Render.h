#ifndef NORRSKEN_H
#define NORRSKEN_H

#include "DL_API.h"
#include "c4d.h"

bool DL_RenderFrame(BaseDocument* doc,
					long frame,
					RENDER_MODE mode = DL_INTERACTIVE,
					bool progressive = false);

#endif