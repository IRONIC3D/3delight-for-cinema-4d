#include "Node.h"
#include "PluginManager.h"
#include "c4d.h"

extern PluginManager PM;

class TranslatorDeleter
{
public:
	void operator()(DL_Translator* translator)
	{
		if (translator) {
			translator->Free();
		}
	}
};

Node::Node(BaseList2D* listnode)
{
	c4d_node = listnode;
	hdata.deformationSamples = 2;
	hdata.transformSamples = 2;
	handle = "";
	cache_parent_handle = "";
	// dirty_checksum = -1;
	TranslatorDeleter d;

	if (listnode) {
		translator = DL_TranslatorPtr(PM.GetTranslator(listnode->GetType()), d);

	} else {
		translator = DL_TranslatorPtr(NULL);
	}
}

DL_Translator*
Node::GetTranslator()
{
	return translator.get();
}

BaseList2D*
Node::GetC4DNode()
{
	return c4d_node;
}
