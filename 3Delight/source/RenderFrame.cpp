#include "DL_Render.h"
#include "IDs.h"
#include "c4d.h"
#include "dlrendersettings.h"

class RenderFrame : public CommandData
{
public:
	virtual Bool Execute(BaseDocument* doc, GeDialog* parentManager);
};

Bool RenderFrame::Execute(BaseDocument* doc, GeDialog* parentManager)
{
	BaseTime t = doc->GetTime();
	long frame = t.GetFrame(doc->GetFps());
	DL_RenderFrame(doc, frame, DL_INTERACTIVE, true);
	return true;
}

Bool RegisterRenderFrame(void)
{
	return RegisterCommandPlugin(ID_RENDERFRAME,
								 "Render frame"_s,
								 PLUGINFLAG_HIDEPLUGINMENU,
								 AutoBitmap("camera.tif"_s),
								 String(),
								 NewObjClear(RenderFrame));
}