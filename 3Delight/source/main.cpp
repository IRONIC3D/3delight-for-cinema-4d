#include "main.h"

void Create3DelightMenu(BaseContainer* bc)
{
	if (!bc) {
		return;
	}

	BrowseContainer browse(bc);
	Int32 id = 0;
	GeData* dat = nullptr;

	while (browse.GetNext(&id, &dat)) {
		if (id == MENURESOURCE_SUBMENU) {
			BaseContainer* container = dat->GetContainer();
			String subtitle = container->GetString(MENURESOURCE_SUBTITLE);

			if (container && subtitle == "IDS_EDITOR_PIPELINE") {
				BaseContainer sc;
				sc.InsData(MENURESOURCE_SUBTITLE, String("3Delight"));
				BaseContainer MaterialsSubMenu;
				MaterialsSubMenu.InsData(MENURESOURCE_SUBTITLE, String("Materials"));
				MaterialsSubMenu.InsData(MENURESOURCE_COMMAND,
										 "PLUGIN_CMD_" + String::IntToString(DL_PRINCIPLED_COMMAND));
				MaterialsSubMenu.InsData(MENURESOURCE_COMMAND,
										 "PLUGIN_CMD_" + String::IntToString(DL_STANDARD_COMMAND));
				MaterialsSubMenu.InsData(MENURESOURCE_COMMAND,
										 "PLUGIN_CMD_" + String::IntToString(DL_GLASS_COMMAND));
				MaterialsSubMenu.InsData(MENURESOURCE_COMMAND,
										 "PLUGIN_CMD_" + String::IntToString(DL_HAIRANDFUR_COMMAND));
				MaterialsSubMenu.InsData(MENURESOURCE_COMMAND,
										 "PLUGIN_CMD_" + String::IntToString(DL_METAL_COMMAND));
				MaterialsSubMenu.InsData(MENURESOURCE_COMMAND,
										 "PLUGIN_CMD_" + String::IntToString(DL_SKIN_COMMAND));
				MaterialsSubMenu.InsData(MENURESOURCE_COMMAND,
										 "PLUGIN_CMD_" + String::IntToString(DL_SUBSTANCE_COMMAND));
				MaterialsSubMenu.InsData(MENURESOURCE_COMMAND,
										 "PLUGIN_CMD_" + String::IntToString(DL_CARPAINT_COMMAND));
				MaterialsSubMenu.InsData(MENURESOURCE_COMMAND,
										 "PLUGIN_CMD_" + String::IntToString(DL_THIN_COMMAND));
				MaterialsSubMenu.InsData(MENURESOURCE_COMMAND,
										 "PLUGIN_CMD_" + String::IntToString(DL_TOON_COMMAND));
				MaterialsSubMenu.InsData(MENURESOURCE_COMMAND,
										 "PLUGIN_CMD_" + String::IntToString(DL_LAYERED_COMMAND));
				MaterialsSubMenu.InsData(
					MENURESOURCE_COMMAND,
					"PLUGIN_CMD_" + String::IntToString(DL_RANDOM_MATERIAL_COMMAND));
				MaterialsSubMenu.InsData(MENURESOURCE_COMMAND,
										 "PLUGIN_CMD_" + String::IntToString(ID_AOV_GROUP));
				BaseContainer VolumesSubMenu;
				VolumesSubMenu.InsData(MENURESOURCE_SUBTITLE, String("Volumes"));
				VolumesSubMenu.InsData(MENURESOURCE_COMMAND,
									   "PLUGIN_CMD_" + String::IntToString(DL_OPENVDB));
				VolumesSubMenu.InsData(MENURESOURCE_COMMAND,
									   "PLUGIN_CMD_" + String::IntToString(DL_ATMOSPHERE_MAT));
				BaseContainer LightsSubMenu;
				BaseContainer AreaLightSubMenu;
				AreaLightSubMenu.InsData(MENURESOURCE_SUBTITLE, String("Area Light"));
				AreaLightSubMenu.InsData(MENURESOURCE_SUBTITLE_ICONID, ID_AREALIGHT);
				AreaLightSubMenu.InsData(MENURESOURCE_COMMAND,
										 "PLUGIN_CMD_" + String::IntToString(ID_AREALIGHT_SQUARE));
				AreaLightSubMenu.InsData(MENURESOURCE_COMMAND,
										 "PLUGIN_CMD_" + String::IntToString(ID_AREALIGHT_DISC));
				AreaLightSubMenu.InsData(MENURESOURCE_COMMAND,
										 "PLUGIN_CMD_" + String::IntToString(ID_AREALIGHT_SPHERE));
				AreaLightSubMenu.InsData(MENURESOURCE_COMMAND,
										 "PLUGIN_CMD_" + String::IntToString(ID_AREALIGHT_CYLINDER));
				LightsSubMenu.InsData(MENURESOURCE_SUBTITLE, String("Lights"));
				LightsSubMenu.InsData(MENURESOURCE_COMMAND,
									  "PLUGIN_CMD_" + String::IntToString(ID_DIRECTIONAL_LIGHT));
				LightsSubMenu.InsData(MENURESOURCE_COMMAND,
									  "PLUGIN_CMD_" + String::IntToString(ID_POINTLIGHT));
				LightsSubMenu.InsData(MENURESOURCE_COMMAND,
									  "PLUGIN_CMD_" + String::IntToString(ID_SPOTLIGHT));
				LightsSubMenu.InsData(MENURESOURCE_SUBMENU, AreaLightSubMenu);
				LightsSubMenu.InsData(MENURESOURCE_COMMAND,
									  "PLUGIN_CMD_" + String::IntToString(ID_ENVIRONMENTLIGHT));
				LightsSubMenu.InsData(MENURESOURCE_COMMAND,
									  "PLUGIN_CMD_" + String::IntToString(ID_INCANDESCENCELIGHT));
				LightsSubMenu.InsData(MENURESOURCE_COMMAND,
									  "PLUGIN_CMD_" + String::IntToString(ID_LIGHTGROUP));
				BaseContainer RenderSubMenu;
				RenderSubMenu.InsData(MENURESOURCE_SUBTITLE, String("Render"));
				RenderSubMenu.InsData(MENURESOURCE_COMMAND,
									  "PLUGIN_CMD_" + String::IntToString(ID_RENDER_COMMAND));
				RenderSubMenu.InsData(MENURESOURCE_COMMAND,
									  "PLUGIN_CMD_" + String::IntToString(ID_BATCH_RENDER_COMMAND));
				RenderSubMenu.InsData(MENURESOURCE_COMMAND,
									  "PLUGIN_CMD_" + String::IntToString(ID_NSI_EXPORT_COMMAND));
				RenderSubMenu.InsData(
					MENURESOURCE_COMMAND,
					"PLUGIN_CMD_" + String::IntToString(ID_INTERACTIVE_RENDERING_START));
				RenderSubMenu.InsData(
					MENURESOURCE_COMMAND,
					"PLUGIN_CMD_" + String::IntToString(ID_INTERACTIVE_RENDERING_STOP));
				// RenderSubMenu.InsData(MENURESOURCE_COMMAND, "PLUGIN_CMD_" +
				// String::IntToString(ID_RENDERSEQUENCE));
				BaseContainer StandInSubMenu;
				StandInSubMenu.InsData(MENURESOURCE_SUBTITLE, String("Stand-In"));
				StandInSubMenu.InsData(MENURESOURCE_COMMAND,
									   "PLUGIN_CMD_" + String::IntToString(STAND_IN_NODE_CREATE));
				StandInSubMenu.InsData(MENURESOURCE_COMMAND,
									   "PLUGIN_CMD_" + String::IntToString(STANDIN_EXPORT));
				sc.InsData(MENURESOURCE_SUBMENU, MaterialsSubMenu);
				sc.InsData(MENURESOURCE_SUBMENU, LightsSubMenu);
				sc.InsData(MENURESOURCE_SUBMENU, VolumesSubMenu);
				sc.InsData(MENURESOURCE_SEPERATOR, "");
				sc.InsData(MENURESOURCE_SUBMENU, RenderSubMenu);
				sc.InsData(MENURESOURCE_SEPERATOR, "");
				sc.InsData(MENURESOURCE_SUBMENU, StandInSubMenu);
				sc.InsData(MENURESOURCE_SEPERATOR, "");
				sc.InsData(MENURESOURCE_COMMAND,
						   "PLUGIN_CMD_" + String::IntToString(C4DMatConvert));
				sc.InsData(MENURESOURCE_SEPERATOR, "");
				sc.InsData(MENURESOURCE_COMMAND,
						   "PLUGIN_CMD_" + String::IntToString(ID_CREATE_SHELF));
				sc.InsData(MENURESOURCE_COMMAND,
						   "PLUGIN_CMD_" + String::IntToString(PREFERENCES_ID));
				sc.InsData(MENURESOURCE_SEPERATOR, "");
				sc.InsData(MENURESOURCE_COMMAND,
						   "PLUGIN_CMD_" + String::IntToString(HELP_ID));
				sc.InsData(MENURESOURCE_COMMAND,
						   "PLUGIN_CMD_" + String::IntToString(ABOUT_ID));
				bc->InsDataAfter(MENURESOURCE_STRING, sc, dat);
			}
		}
	}
}
Bool PluginStart(void)
{
	// if (!RegisterCustomGUITest()) return FALSE;
	if (!RegisterPreferences()) {
		return FALSE;
	}

	if (!RegisterRenderFrame()) {
		return FALSE;
	}

	if (!RegisterRenderSequence()) {
		return FALSE;
	}

	if (!RegisterCustomListView()) {
		return FALSE;
	}

	if (!RegisterCustomMultiLight()) {
		return FALSE;
	}

	if (!RegisterInteractiveRenderingStart()) {
		return FALSE;
	}

	if (!RegisterInteractiveRenderingStop()) {
		return FALSE;
	}

	if (!Register3DelightPlugin()) {
		return FALSE;
	}

	// if (!Register3DelightCloudPlugin()) return FALSE;
	if (!RegisterInteractiveRenderManager()) {
		return FALSE;
	}

	if (!RegisterRenderManager()) {
		return FALSE;
	}

	if (!Register3DelightCommand()) {
		return FALSE;
	}

	if (!Register3DelightBatchRenderCommand()) {
		return FALSE;
	}

	if (!RegisterNSIExportCommand()) {
		return FALSE;
	}

	if (!RegisterCreateShelf()) {
		return FALSE;
	}

	if (!RegisterAbout()) {
		return FALSE;
	}

	if (!RegisterHelp()) {
		return FALSE;
	}

	// if (!RegisterPreferences()) return FALSE;
	if (!RegisterDisplay()) {
		return FALSE;
	}

	if (!RegisterDL_CameraTag()) {
		return FALSE;
	}

	if (!RegisterDL_MotionBlurTag()) {
		return FALSE;
	}

	if (!RegisterDL_VisibilityTag()) {
		return FALSE;
	}

	if (!RegisterDL_SubdivisionSurfaceTag()) {
		return FALSE;
	}

	if (!RegisterStandinNode()) {
		return FALSE;
	}

	if (!RegisterStandInExport()) {
		return FALSE;
	}

	// Register 3Delight display driver for rendering to C4D bitmaps
	NSI::DynamicAPI& api = NSIAPI::Instance().API();
	decltype(&DspyRegisterDriverTable) PDspyRegisterDriverTable = nullptr;
	api.LoadFunction(PDspyRegisterDriverTable, "DspyRegisterDriverTable");

	if (PDspyRegisterDriverTable == nullptr) {
		CriticalOutput("The 3Delight dynamic library could not be found. "
					   "The 3Delight for Cinema4D plug-in failed to initialize.");
		return false;
	}

	PtDspyDriverFunctionTable table;
	memset(&table, 0, sizeof(table));
	table.Version = 1;
	table.pOpen = &BmpDspyImageOpen;
	table.pQuery = &BmpDspyImageQuery;
	table.pWrite = &BmpDspyImageData;
	table.pClose = &BmpDspyImageClose;
	PDspyRegisterDriverTable("C4D_bitmap", &table);
	return true;
}

void PluginEnd(void)
{
	NSIAPI::DeleteInstance();
}

Bool PluginMessage(Int32 id, void* data)
{
	switch (id) {
	case C4DPL_INIT_SYS:
		if (!g_resource.Init()) {
			return false; // don't start plugin without resource
		}

		return true;

	case C4DPL_STARTACTIVITY: {
		// Send message to load translators from other plugins
		GePluginMessage(DL_LOAD_PLUGINS, (void*) &PM);
		break;
	}

	case DL_LOAD_PLUGINS: {
		DL_PluginManager* pm = (DL_PluginManager*) data;
		pm->RegisterHook(AllocateHook<CameraHook>);
		pm->RegisterHook(AllocateHook<RenderOptionsHook>);
		pm->RegisterTranslator(ID_DISPLAY, AllocateTranslator<DisplayTranslator>);
		pm->RegisterTranslator(ID_DL_VISIBILITYTAG,
							   AllocateTranslator<VisibilityTagTranslator>);
		pm->RegisterTranslator(STAND_IN_NODE_CREATE,
							   AllocateTranslator<StandInNode_Translator>);
		// pm->RegisterTranslator(,AllocateTranslator<QualityValuesParser>);
		break;
	}

	case C4DPL_BUILDMENU: {
		BaseContainer* bc = GetMenuResource("M_EDITOR"_s);

		if (!bc) {
			return FALSE;
		}

		Create3DelightMenu(bc);
		break;
	}
	}

	return false;
}
