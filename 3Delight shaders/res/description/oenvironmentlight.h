#ifndef _Oenvironmentlight_H_
#define _Oenvironmentlight_H_

enum {
	SHADER_PATH = 0,
	ENVIRONMENT_INTENSITY = 1000,

	ENVIRONMENT_LIGHT,

	POPUP_ENVIRONMENT_INTENSITY,
	ENVIRONMENT_INTENSITY_GROUP_PARAM,
	ENVIRONMENT_INTENSITY_SHADER,
	ENVIRONMENT_INTENSITY_SHADER_TEMP,

	POPUP_ENVIRONMENT_EXPOSURE,
	ENVIRONMENT_EXPOSURE,
	ENVIRONMENT_EXPOSURE_GROUP_PARAM,
	ENVIRONMENT_EXPOSURE_SHADER,
	ENVIRONMENT_EXPOSURE_SHADER_TEMP,

	POPUP_ENVIRONMENT_TEXTURE,
	ENVIRONMENT_TEXTURE,
	ENVIRONMENT_TEXTURE_GROUP_PARAM,
	ENVIRONMENT_TEXTURE_SHADER,
	ENVIRONMENT_TEXTURE_SHADER_TEMP,

	POPUP_ENVIRONMENT_TINT,
	ENVIRONMENT_TINT,
	ENVIRONMENT_TINT_GROUP_PARAM,
	ENVIRONMENT_TINT_SHADER,
	ENVIRONMENT_TINT_SHADER_TEMP,

	ENVIRONMENT_MAPPING,
	ENVIRONMENT_MAPPING_SPHERICAL = 0,
	ENVIRONMENT_MAPPING_ANGULAR = 1,
	ENVIRONMENT_SEEN_BY_CAMERA = 1050,
	ENVIRONMENT_PRELIT,
	ENVIRONMENT_RADIUS,
	ENVIRONMENT_ILLUMINATES_BY_DEFAULT,
	LINE1,
	LIGHT_LINKING,
	DUMB_VALUE
};

#endif