#pragma once
enum {
	Shader_Path = 0,
	HAIR_COLOR_PAGE = 100,
	HAIR_COLOR,
	HAIR_AND_FUR,
	HAIR_AND_FUR_PAGE,
	POPUP_HAIR_COLOR_MELANIN,
	HAIR_COLOR_MELANIN,
	HAIR_COLOR_MELANIN_GROUP_PARAM,
	HAIR_COLOR_MELANIN_SHADER,
	HAIR_COLOR_MELANIN_SHADER_TEMP,

	POPUP_HAIR_COLOR_MELANIN_RED,
	HAIR_COLOR_MELANIN_RED,
	HAIR_COLOR_MELANIN_RED_GROUP_PARAM,
	HAIR_COLOR_MELANIN_RED_SHADER,
	HAIR_COLOR_MELANIN_RED_SHADER_TEMP,

	POPUP_HAIR_COLOR_DYE,
	HAIR_COLOR_DYE,
	HAIR_COLOR_DYE_GROUP_PARAM,
	HAIR_COLOR_DYE_SHADER,
	HAIR_COLOR_DYE_SHADER_TEMP,

	POPUP_HAIR_COLOR_DYE_WEIGHT,
	HAIR_COLOR_DYE_WEIGHT,
	HAIR_COLOR_DYE_WEIGHT_GROUP_PARAM,
	HAIR_COLOR_DYE_WEIGHT_SHADER,
	HAIR_COLOR_DYE_WEIGHT_SHADER_TEMP,

	HAIR_LOOK_PAGE,
	HAIR_LOOK,

	POPUP_HAIR_LOOK_SPECULAR_LEVEL,
	HAIR_LOOK_SPECULAR_LEVEL,
	HAIR_LOOK_SPECULAR_LEVEL_GROUP_PARAM,
	HAIR_LOOK_SPECULAR_LEVEL_SHADER,
	HAIR_LOOK_SPECULAR_LEVEL_SHADER_TEMP,

	POPUP_HAIR_LOOK_ROUGHNESS_ALONG,
	HAIR_LOOK_ROUGHNESS_ALONG,
	HAIR_LOOK_ROUGHNESS_ALONG_GROUP_PARAM,
	HAIR_LOOK_ROUGHNESS_ALONG_SHADER,
	HAIR_LOOK_ROUGHNESS_ALONG_SHADER_TEMP,

	POPUP_HAIR_LOOK_ROUGHNESS_AROUND,
	HAIR_LOOK_ROUGHNESS_AROUND,
	HAIR_LOOK_ROUGHNESS_AROUND_GROUP_PARAM,
	HAIR_LOOK_ROUGHNESS_AROUND_SHADER,
	HAIR_LOOK_ROUGHNESS_AROUND_SHADER_TEMP,

	POPUP_HAIR_LOOK_SYNTHETIC_FIBER,
	HAIR_LOOK_SYNTHETIC_FIBER,
	HAIR_LOOK_SYNTHETIC_FIBER_GROUP_PARAM,
	HAIR_LOOK_SYNTHETIC_FIBER_SHADER,
	HAIR_LOOK_SYNTHETIC_FIBER_SHADER_TEMP,

	HAIR_VARIATION_PAGE,
	HAIR_VARIATION,

	POPUP_HAIR_VARIATION_MELANIN,
	HAIR_VARIATION_MELANIN,
	HAIR_VARIATION_MELANIN_GROUP_PARAM,
	HAIR_VARIATION_MELANIN_SHADER,
	HAIR_VARIATION_MELANIN_SHADER_TEMP,

	POPUP_HAIR_VARIATION_MELANIN_RED,
	HAIR_VARIATION_MELANIN_RED,
	HAIR_VARIATION_MELANIN_RED_GROUP_PARAM,
	HAIR_VARIATION_MELANIN_RED_SHADER,
	HAIR_VARIATION_MELANIN_RED_SHADER_TEMP,

	POPUP_HAIR_VARIATION_WHITE_HAIR,
	HAIR_VARIATION_WHITE_HAIR,
	HAIR_VARIATION_WHITE_HAIR_GROUP_PARAM,
	HAIR_VARIATION_WHITE_HAIR_SHADER,
	HAIR_VARIATION_WHITE_HAIR_SHADER_TEMP,

	POPUP_HAIR_VARIATION_DYE_HUE,
	HAIR_VARIATION_DYE_HUE,
	HAIR_VARIATION_DYE_HUE_GROUP_PARAM,
	HAIR_VARIATION_DYE_HUE_SHADER,
	HAIR_VARIATION_DYE_HUE_SHADER_TEMP,

	POPUP_HAIR_VARIATION_DYE_SATURATION,
	HAIR_VARIATION_DYE_SATURATION,
	HAIR_VARIATION_DYE_SATURATION_GROUP_PARAM,
	HAIR_VARIATION_DYE_SATURATION_SHADER,
	HAIR_VARIATION_DYE_SATURATION_SHADER_TEMP,

	POPUP_HAIR_VARIATION_DYE_VALUE,
	HAIR_VARIATION_DYE_VALUE,
	HAIR_VARIATION_DYE_VALUE_GROUP_PARAM,
	HAIR_VARIATION_DYE_VALUE_SHADER,
	HAIR_VARIATION_DYE_VALUE_SHADER_TEMP,

	POPUP_HAIR_VARIATION_ROUGHNESS,
	HAIR_VARIATION_ROUGHNESS,
	HAIR_VARIATION_ROUGHNESS_GROUP_PARAM,
	HAIR_VARIATION_ROUGHNESS_SHADER,
	HAIR_VARIATION_ROUGHNESS_SHADER_TEMP,

	POPUP_HAIR_VARIATION_SPECULAR_LEVEL,
	HAIR_VARIATION_SPECULAR_LEVEL,
	HAIR_VARIATION_SPECULAR_LEVEL_GROUP_PARAM,
	HAIR_VARIATION_SPECULAR_LEVEL_SHADER,
	HAIR_VARIATION_SPECULAR_LEVEL_SHADER_TEMP,

	HAIR_BOOST_PAGE,
	HAIR_BOOST,

	POPUP_HAIR_BOOST_GLOSSINESS,
	HAIR_BOOST_GLOSSINESS,
	HAIR_BOOST_GLOSSINESS_GROUP_PARAM,
	HAIR_BOOST_GLOSSINESS_SHADER,
	HAIR_BOOST_GLOSSINESS_SHADER_TEMP,

	POPUP_HAIR_BOOST_SPECULAR_LEVEL,
	HAIR_BOOST_SPECULAR_LEVEL,
	HAIR_BOOST_SPECULAR_LEVEL_GROUP_PARAM,
	HAIR_BOOST_SPECULAR_LEVEL_SHADER,
	HAIR_BOOST_SPECULAR_LEVEL_SHADER_TEMP,

	POPUP_HAIR_BOOST_TRANSMISSION,
	HAIR_BOOST_TRANSMISSION,
	HAIR_BOOST_TRANSMISSION_GROUP_PARAM,
	HAIR_BOOST_TRANSMISSION_SHADER,
	HAIR_BOOST_TRANSMISSION_SHADER_TEMP,

	POPUP_HAIR_LOOK_MEDULLA,
	HAIR_LOOK_MEDULLA,
	HAIR_LOOK_MEDULLA_GROUP_PARAM,
	HAIR_LOOK_MEDULLA_SHADER,
	HAIR_LOOK_MEDULLA_SHADER_TEMP,

	AOV_PAGE,
	DL_AOV_GROUP = 1055640,
	DUMP_VALUE
};
