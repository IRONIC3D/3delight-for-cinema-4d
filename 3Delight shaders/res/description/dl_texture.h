#pragma once
enum {
	Shader_Path = 0,
	OPENVDB_NODE = 100,
	SHADER_GROUP,
	TEXTURE_GROUP,
	TEXTURE_FILE,
	TEXTURE_FILE_BUTTON,

	TEXTURE_COLOR_SPACE,
	TEXTURE_COLOR_SPACE_LINEAR = 0,
	TEXTURE_COLOR_SPACE_SRGB = 1,
	TEXTURE_COLOR_SPACE_REC = 2,

	POPUP_TEXTURE_DEFAULT_COLOR = 110,
	TEXTURE_DEFAULT_COLOR,
	TEXTURE_DEFAULT_COLOR_SHADER,
	TEXTURE_DEFAULT_COLOR_SHADER_GROUP_PARAM,
	TEXTURE_DEFAULT_COLOR_SHADER_TEMP,

	COLOR_CORRECT_GROUP,
	COLOR_CORRECT_TOON,
	COLOR_CORRECT_TOON_OFF = 0,
	COLOR_CORRECT_TOON_NORMALIZE = 1,

	POPUP_COLOR_CORRECT_COL_GAIN = 120,
	COLOR_CORRECT_COL_GAIN,
	COLOR_CORRECT_COL_GAIN_GROUP_PARAM,
	COLOR_CORRECT_COL_GAIN_SHADER,
	COLOR_CORRECT_COL_GAIN_SHADER_TEMP,

	POPUP_COLOR_CORRECT_COL_OFFSET,
	COLOR_CORRECT_COL_OFFSET,
	COLOR_CORRECT_COL_OFFSET_SHADER_GROUP_PARAM,
	COLOR_CORRECT_COL_OFFSET_SHADER,
	COLOR_CORRECT_COL_OFFSET_SHADER_TEMP,

	POPUP_COLOR_CORRECT_ALPHA_GAIN,
	COLOR_CORRECT_ALPHA_GAIN,
	COLOR_CORRECT_ALPHA_GAIN_SHADER_GROUP_PARAM,
	COLOR_CORRECT_ALPHA_GAIN_SHADER,
	COLOR_CORRECT_ALPHA_GAIN_SHADER_TEMP,

	POPUP_COLOR_CORRECT_ALPHA_OFFSET,
	COLOR_CORRECT_ALPHA_OFFSET,
	COLOR_CORRECT_ALPHA_OFFSET_SHADER_GROUP_PARAM,
	COLOR_CORRECT_ALPHA_OFFSET_SHADER,
	COLOR_CORRECT_ALPHA_OFFSET_SHADER_TEMP,

	COLOR_CORRECT_IS_ALPHA_LUMIN,
	COLOR_CORRECT_INVERT,

	TILE_REMOVAL_GROUP,
	TILE_REMOVAL_BOOL,

	POPUP_TILE_REMOVAL_SOFTNESS,
	TILE_REMOVAL_SOFTNESS,
	TILE_REMOVAL_SOFTNESS_SHADER_GROUP_PARAM,
	TILE_REMOVAL_SOFTNESS_SHADER,
	TILE_REMOVAL_SOFTNESS_SHADER_TEMP,

	POPUP_TILE_REMOVAL_OFFSET,
	TILE_REMOVAL_OFFSET,
	TILE_REMOVAL_OFFSET_SHADER_GROUP_PARAM,
	TILE_REMOVAL_OFFSET_SHADER,
	TILE_REMOVAL_OFFSET_SHADER_TEMP,

	POPUP_TILE_REMOVAL_ROTATION,
	TILE_REMOVAL_ROTATION,
	TILE_REMOVAL_ROTATION_SHADER_GROUP_PARAM,
	TILE_REMOVAL_ROTATION_SHADER,
	TILE_REMOVAL_ROTATION_SHADER_TEMP,

	POPUP_TILE_REMOVAL_SCALE,
	TILE_REMOVAL_SCALE,
	TILE_REMOVAL_SCALE_SHADER_GROUP_PARAM,
	TILE_REMOVAL_SCALE_SHADER,
	TILE_REMOVAL_SCALE_SHADER_TEMP,

	IMAGE_SEQUENCE_GROUP,
	USE_IMAGE_SEQUENCE,
	IMAGE_SEQUENCE_FRAME,
	IMAGE_SEQUENCE_FRAME_OFFSET,

	FILTERING_GROUP,
	FILTER_VAL,
	BLUR_VAL,

	DUMMY_VALUE

};
