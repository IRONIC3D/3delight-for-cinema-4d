#include "IDs.h"
#include "TextureUI_Functions.h"
#include "c4d.h"
#include "c4d_symbols.h"
#include "dl_color_blend.h"

class DLColorBlend : public ShaderData
{
public:
	virtual Bool Init(GeListNode* node);
	virtual Vector Output(BaseShader* sh, ChannelData* sd);
	virtual Bool GetDDescription(GeListNode* node,
								 Description* description,
								 DESCFLAGS_DESC& flags);
	virtual Bool Message(GeListNode* node, Int32 type, void* data);
	static NodeData* Alloc(void)
	{
		return NewObjClear(DLColorBlend);
	}
};

Bool DLColorBlend::Init(GeListNode* node)
{
	BaseContainer* data = ((BaseShader*) node)->GetDataInstance();
	data->SetInt32(BLEND_MODE, BLEND_MODE_OVER);
	data->SetFloat(BLEND_FACTOR, 1.0);
	data->SetVector(BLEND_FOREGROUND, HSVToRGB(Vector(0, 0, 1)));
	data->SetVector(BLEND_BACKGROUND, HSVToRGB(Vector(0, 0, 0)));
	return true;
}

Bool DLColorBlend::GetDDescription(GeListNode* node,
								   Description* description,
								   DESCFLAGS_DESC& flags)
{
	description->LoadDescription(DL_COLORBLEND);
	flags |= DESCFLAGS_DESC::LOADED;
	BaseContainer* dldata = ((BaseObject*) node)->GetDataInstance();
	HideAndShowTextures(BLEND_FACTOR_GROUP_PARAM,
						BLEND_FACTOR,
						BLEND_FACTOR_SHADER,
						BLEND_FACTOR_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(BLEND_FOREGROUND_GROUP_PARAM,
						BLEND_FOREGROUND,
						BLEND_FOREGROUND_SHADER,
						BLEND_FOREGROUND_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(BLEND_BACKGROUND_GROUP_PARAM,
						BLEND_BACKGROUND,
						BLEND_BACKGROUND_SHADER,
						BLEND_BACKGROUND_SHADER_TEMP,
						node,
						description,
						dldata);
	return TRUE;
}

Bool DLColorBlend::Message(GeListNode* node, Int32 type, void* data)
{
	if (!node) {
		return false;
	}

	BaseContainer* dldata = ((BaseMaterial*) node)->GetDataInstance();

	if (type == MSG_DESCRIPTION_POPUP) {
		DescriptionPopup* dp = (DescriptionPopup*) data;
		int clicked_button_id = dp->_descId[0].id;

		switch (clicked_button_id) {
		case POPUP_BLEND_FACTOR:
			FillPopupMenu(dldata, dp, BLEND_FACTOR_GROUP_PARAM);
			break;

		case POPUP_BLEND_FOREGROUND:
			FillPopupMenu(dldata, dp, BLEND_FOREGROUND_GROUP_PARAM);
			break;

		case POPUP_BLEND_BACKGROUND:
			FillPopupMenu(dldata, dp, BLEND_BACKGROUND_GROUP_PARAM);
			break;

		default:
			break;
		}
	}

	return true;
}

Vector
DLColorBlend::Output(BaseShader* sh, ChannelData* sd)
{
	return Vector(0, 0, 0);
}

Bool RegisterColorBlendTexture(void)
{
	return RegisterShaderPlugin(DL_COLORBLEND,
								"Color Blend"_s,
								0,
								DLColorBlend::Alloc,
								"dl_color_blend"_s,
								0);
}
