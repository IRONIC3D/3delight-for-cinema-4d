#include "DLToon_Translator.h"
#include "3DelightEnvironment.h"
#include "dl_toon.h"

Delight_Toon::Delight_Toon()
{
	is_3delight_material = true;
	const char* delightpath = DelightEnv::getDelightEnvironment();
	c_shaderpath =
		delightpath + (std::string) "/osl" + (std::string) "/dlToon.oso";

	if (c_shaderpath.c_str() && c_shaderpath[0]) {
		m_ids_to_names[TOON_SHADER_PATH] = std::make_pair("", &c_shaderpath[0]);
	}

	m_ids_to_names[TOON_PHYSICAL_LAYER] =
		std::make_pair("outColor", "physical_layer");
	m_ids_to_names[TOON_TINT] = std::make_pair("", "color_tint");
	m_ids_to_names[TOON_TINT_SHADER] = std::make_pair("outColor", "color_tint");
	m_ids_to_names[TOON_COLOR] = std::make_pair("", "color_curve");
	m_ids_to_names[TOON_WEIGHT] = std::make_pair("", "diffuse_weight");
	m_ids_to_names[TOON_WEIGHT_SHADER] =
		std::make_pair("outColor[0]", "diffuse_weight");
	m_ids_to_names[TOON_OPACITY] = std::make_pair("", "opacity");
	m_ids_to_names[TOON_OPACITY_SHADER] =
		std::make_pair("outColor[0]", "opacity");
	m_ids_to_names[SPECULAR_COLOR] = std::make_pair("", "specular_color");
	m_ids_to_names[SPECULAR_COLOR_SHADER] =
		std::make_pair("outColor", "specular_color");
	m_ids_to_names[SPECULAR_WEIGHT] = std::make_pair("", "specular_weight");
	m_ids_to_names[SPECULAR_WEIGHT_SHADER] =
		std::make_pair("outColor[0]", "specular_weight");
	m_ids_to_names[SPECULAR_ROUGHNESS] = std::make_pair("", "specular_roughness");
	m_ids_to_names[SPECULAR_ROUGHNESS_SHADER] =
		std::make_pair("outColor[0]", "specular_roughness");
	m_ids_to_names[OUTLINES_FADE] = std::make_pair("", "outline_fade");
	m_ids_to_names[OUTLINES_FADE_SHADER] =
		std::make_pair("outColor[0]", "outline_fade");
	m_ids_to_names[SILHOUETTES_ENABLE] =
		std::make_pair("", "outline_silhouettes_enable");
	m_ids_to_names[SILHOUETTES_COLOR] =
		std::make_pair("", "outline_silhouettes_color");
	m_ids_to_names[SILHOUETTES_COLOR_SHADER] =
		std::make_pair("outColor", "outline_silhouettes_color");
	m_ids_to_names[SILHOUETTES_TINT_COLOR] =
		std::make_pair("", "outline_silhouettes_tint_color");
	m_ids_to_names[SILHOUETTES_OPACITY] =
		std::make_pair("", "outline_silhouettes_opacity");
	m_ids_to_names[SILHOUETTES_OPACITY_SHADER] =
		std::make_pair("outColor[0]", "outline_silhouettes_opacity");
	m_ids_to_names[SILHOUETTES_WIDTH] =
		std::make_pair("", "outline_silhouettes_width");
	m_ids_to_names[SILHOUETTES_WIDTH_SHADER] =
		std::make_pair("outColor[0]", "outline_silhouettes_width");
	m_ids_to_names[SILHOUETTES_SENSITIVITY] =
		std::make_pair("", "outline_silhouettes_sensitivity");
	m_ids_to_names[SILHOUETTES_SENSITIVITY_SHADER] =
		std::make_pair("outColor[0]", "outline_silhouettes_sensitivity");
	m_ids_to_names[FOLDS_ENABLE] = std::make_pair("", "outline_folds_enable");
	m_ids_to_names[FOLDS_COLOR] = std::make_pair("", "outline_folds_color");
	m_ids_to_names[FOLDS_COLOR_SHADER] =
		std::make_pair("outColor", "outline_folds_color");
	m_ids_to_names[FOLDS_TINT_COLOR] =
		std::make_pair("", "outline_folds_tint_color");
	m_ids_to_names[FOLDS_OPACITY] = std::make_pair("", "outline_folds_opacity");
	m_ids_to_names[FOLDS_OPACITY_SHADER] =
		std::make_pair("outColor[0]", "outline_folds_opacity");
	m_ids_to_names[FOLDS_WIDTH] = std::make_pair("", "outline_folds_width");
	m_ids_to_names[FOLDS_WIDTH_SHADER] =
		std::make_pair("outColor[0]", "outline_folds_width");
	m_ids_to_names[FOLDS_SENSITIVITY] =
		std::make_pair("", "outline_folds_sensitivity");
	m_ids_to_names[FOLDS_SENSITIVITY_SHADER] =
		std::make_pair("outColor[0]", "outline_folds_sensitivity");
	m_ids_to_names[CREASES_ENABLE] = std::make_pair("", "outline_creases_enable");
	m_ids_to_names[CREASES_COLOR] = std::make_pair("", "outline_creases_color");
	m_ids_to_names[CREASES_COLOR_SHADER] =
		std::make_pair("outColor", "outline_creases_color");
	m_ids_to_names[CREASES_TINT_COLOR] =
		std::make_pair("", "outline_creases_tint_color");
	m_ids_to_names[CREASES_OPACITY] =
		std::make_pair("", "outline_creases_opacity");
	m_ids_to_names[CREASES_OPACITY_SHADER] =
		std::make_pair("outColor[0]", "outline_creases_opacity");
	m_ids_to_names[CREASES_WIDTH] = std::make_pair("", "outline_creases_width");
	m_ids_to_names[CREASES_WIDTH_SHADER] =
		std::make_pair("outColor[0]", "outline_creases_width");
	m_ids_to_names[OBJECTS_ENABLE] = std::make_pair("", "outline_objects_enable");
	m_ids_to_names[OBJECTS_COLOR] = std::make_pair("", "outline_objects_color");
	m_ids_to_names[OBJECTS_COLOR_SHADER] =
		std::make_pair("outColor", "outline_objects_color");
	m_ids_to_names[OBJECTS_TINT_COLOR] =
		std::make_pair("", "outline_objects_tint_color");
	m_ids_to_names[OBJECTS_OPACITY] =
		std::make_pair("", "outline_objects_opacity");
	m_ids_to_names[OBJECTS_OPACITY_SHADER] =
		std::make_pair("outColor[0]", "outline_objects_opacity");
	m_ids_to_names[OBJECTS_WIDTH] = std::make_pair("", "outline_objects_width");
	m_ids_to_names[OBJECTS_WIDTH_SHADER] =
		std::make_pair("outColor[0]", "outline_objects_width");
	m_ids_to_names[TEXTURE_ENABLE] = std::make_pair("", "outline_texture_enable");
	m_ids_to_names[TEXTURE_COLOR_ATTR] =
		std::make_pair("", "outline_texture_color");
	m_ids_to_names[TEXTURE_COLOR_SHADER] =
		std::make_pair("outColor", "outline_texture_color");
	m_ids_to_names[TEXTURE_FILE_ATTR] =
		std::make_pair("", "outline_texture_source");
	m_ids_to_names[TEXTURE_FILE_SHADER] =
		std::make_pair("outColor", "outline_texture_source");
	m_ids_to_names[TEXTURE_TINT_COLOR] =
		std::make_pair("", "outline_texture_tint_color");
	m_ids_to_names[TEXTURE_OPACITY] =
		std::make_pair("", "outline_texture_opacity");
	m_ids_to_names[TEXTURE_OPACITY_SHADER] =
		std::make_pair("outColor[0]", "outline_texture_opacity");
	m_ids_to_names[TEXTURE_WIDTH_ATTR] =
		std::make_pair("", "outline_texture_width");
	m_ids_to_names[TEXTURE_WIDTH_SHADER] =
		std::make_pair("outColor[0]", "outline_texture_width");
	m_ids_to_names[TEXTURE_SENSITIVITY] =
		std::make_pair("", "outline_texture_sensitivity");
	m_ids_to_names[TEXTURE_SENSITIVITY_SHADER] =
		std::make_pair("outColor[0]", "outline_texture_sensitivity");
	m_ids_to_names[BUMP_TYPE] = std::make_pair("", "disp_normal_bump_type");
	m_ids_to_names[BUMP_VALUE] =
		std::make_pair("outColor", "disp_normal_bump_value");
	m_ids_to_names[BUMP_INTENSITY] =
		std::make_pair("", "disp_normal_bump_intensity");
	m_ids_to_names[DL_AOV_GROUP] = std::make_pair("outColor", "aovGroup");
}
