#include "DLMetal_Translator.h"
#include "3DelightEnvironment.h"
#include "dl_metal.h"

Delight_Metal::Delight_Metal()
{
	is_3delight_material = true;
	const char* delightpath = DelightEnv::getDelightEnvironment();
	c_shaderpath =
		delightpath + (std::string) "/osl" + (std::string) "/dlMetal.oso";

	if (c_shaderpath.c_str() && c_shaderpath[0]) {
		m_ids_to_names[Shader_Path] = std::make_pair("", &c_shaderpath[0]);
	}

	m_ids_to_names[COATING_LAYER_THICKNESS] =
		std::make_pair("", "coating_thickness");
	m_ids_to_names[COATING_LAYER_THICKNESS_SHADER] =
		std::make_pair("outColor[0]", "coating_thickness");
	m_ids_to_names[COATING_LAYER_COLOR] = std::make_pair("", "coating_color");
	m_ids_to_names[COATING_LAYER_COLOR_SHADER] =
		std::make_pair("outColor", "coating_color");
	m_ids_to_names[COATING_LAYER_ROUGHNESS] =
		std::make_pair("", "coating_roughness");
	m_ids_to_names[COATING_LAYER_ROUGHNESS_SHADER] =
		std::make_pair("outColor[0]", "coating_roughness");
	m_ids_to_names[COATING_LAYER_SPECULAR_LEVEL] =
		std::make_pair("", "coating_specular_level");
	m_ids_to_names[COATING_LAYER_SPECULAR_LEVEL_SHADER] =
		std::make_pair("outColor[0]", "coating_specular_level");
	m_ids_to_names[METAL_LAYER_COLOR] = std::make_pair("", "i_color");
	m_ids_to_names[METAL_LAYER_COLOR_SHADER] =
		std::make_pair("outColor", "i_color");
	m_ids_to_names[METAL_LAYER_EDGE_COLOR] = std::make_pair("", "edge_color");
	m_ids_to_names[METAL_LAYER_EDGE_COLOR_SHADER] =
		std::make_pair("outColor", "edge_color");
	m_ids_to_names[METAL_LAYER_ROUGHNESS] = std::make_pair("", "roughness");
	m_ids_to_names[METAL_LAYER_ROUGHNESS_SHADER] =
		std::make_pair("outColor[0]", "roughness");
	m_ids_to_names[METAL_LAYER_ANISOTROPY] = std::make_pair("", "anisotropy");
	m_ids_to_names[METAL_LAYER_ANISOTROPY_SHADER] =
		std::make_pair("outColor[0]", "anisotropy");
	m_ids_to_names[METAL_LAYER_ANISOTROPY_DIRECTION] =
		std::make_pair("", "anisotropy_direction");
	m_ids_to_names[METAL_LAYER_ANISOTROPY_DIRECTION_SHADER] =
		std::make_pair("outColor", "anisotropy_direction");
	m_ids_to_names[METAL_LAYER_OPACITY] = std::make_pair("", "opacity");
	m_ids_to_names[METAL_LAYER_OPACITY_SHADER] =
		std::make_pair("outColor[0]", "opacity");
	m_ids_to_names[METAL_LAYER_TEMPERED_METAL] =
		std::make_pair("", "tempered_metal_on");
	m_ids_to_names[METAL_LAYER_OXIDE_THICKNESS] =
		std::make_pair("", "oxide_thickness");
	m_ids_to_names[METAL_LAYER_OXIDE_THICKNESS_SHADER] =
		std::make_pair("outColor[0]", "oxide_thickness");
	m_ids_to_names[METAL_LAYER_OXIDE_IOR] = std::make_pair("", "oxide_ior");
	m_ids_to_names[METAL_LAYER_OXIDE_IOR_SHADER] =
		std::make_pair("outColor[0]", "oxide_ior");
	m_ids_to_names[BUMP_TYPE] = std::make_pair("", "disp_normal_bump_type");
	m_ids_to_names[BUMP_VALUE] =
		std::make_pair("outColor", "disp_normal_bump_value");
	m_ids_to_names[BUMP_INTENSITY] =
		std::make_pair("", "disp_normal_bump_intensity");
	m_ids_to_names[BUMP_LAYERS_AFFECTED] =
		std::make_pair("", "normal_bump_affect_layer");
	m_ids_to_names[DL_AOV_GROUP] = std::make_pair("outColor", "aovGroup");
}
