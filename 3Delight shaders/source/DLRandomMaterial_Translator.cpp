#include "DLRandomMaterial_Translator.h"
#include "3DelightEnvironment.h"
#include "dl_random_material.h"

Delight_RandomMaterial::Delight_RandomMaterial()
{
	is_3delight_material = true;
	const char* delightpath = DelightEnv::getDelightEnvironment();
	c_shaderpath = (std::string) delightpath + "/osl" + "/dlRandomMaterial.oso";

	if (c_shaderpath.c_str() && c_shaderpath[0]) {
		m_ids_to_names[ShADER_PATH] = std::make_pair("", &c_shaderpath[0]);
	}

	m_ids_to_names[INPUT_MATERIAL1] = std::make_pair("outColor", "material_1");
	m_ids_to_names[INPUT_IMPORTANCE1] = std::make_pair("", "importance_1");
	m_ids_to_names[INPUT_IMPORTANCE1_SHADER] =
		std::make_pair("outColor[0]", "importance_1");
	m_ids_to_names[INPUT_MATERIAL2] = std::make_pair("outColor", "material_2");
	m_ids_to_names[INPUT_IMPORTANCE2] = std::make_pair("", "importance_2");
	m_ids_to_names[INPUT_IMPORTANCE2_SHADER] =
		std::make_pair("outColor[0]", "importance_2");
	m_ids_to_names[INPUT_MATERIAL3] = std::make_pair("outColor", "material_3");
	m_ids_to_names[INPUT_IMPORTANCE3] = std::make_pair("", "importance_3");
	m_ids_to_names[INPUT_IMPORTANCE3_SHADER] =
		std::make_pair("outColor[0]", "importance_3");
	m_ids_to_names[INPUT_MATERIAL4] = std::make_pair("outColor", "material_4");
	m_ids_to_names[INPUT_IMPORTANCE4] = std::make_pair("", "importance_4");
	m_ids_to_names[INPUT_IMPORTANCE4_SHADER] =
		std::make_pair("outColor[0]", "importance_4");
	m_ids_to_names[INPUT_MATERIAL5] = std::make_pair("outColor", "material_5");
	m_ids_to_names[INPUT_IMPORTANCE5] = std::make_pair("", "importance_5");
	m_ids_to_names[INPUT_IMPORTANCE5_SHADER] =
		std::make_pair("outColor[0]", "importance_5");
	m_ids_to_names[SEED] = std::make_pair("", "seed");
	m_ids_to_names[DL_AOV_GROUP] = std::make_pair("outColor", "aovGroup");
}
