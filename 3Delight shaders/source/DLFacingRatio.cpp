#include "IDs.h"
#include "TextureUI_Functions.h"
#include "c4d.h"
#include "c4d_symbols.h"
#include "dl_facing_ratio.h"

class DLFacingRatio : public ShaderData
{
public:
	virtual Bool Init(GeListNode* node);
	virtual Bool GetDDescription(GeListNode* node,
								 Description* description,
								 DESCFLAGS_DESC& flags);
	virtual Bool Message(GeListNode* node, Int32 type, void* data);
	virtual Vector Output(BaseShader* sh, ChannelData* sd);
	static NodeData* Alloc(void)
	{
		return NewObjClear(DLFacingRatio);
	}
};

Bool DLFacingRatio::Init(GeListNode* node)
{
	BaseContainer* data = ((BaseShader*) node)->GetDataInstance();
	data->SetVector(FACING_RATIO_COLOR_EDGE,
					HSVToRGB(Vector(288.0 / 360.0, 0, 1)));
	data->SetVector(FACING_RATIO_COLOR_CENTER,
					HSVToRGB(Vector(288.0 / 360.0, 0, 0)));
	data->SetFloat(FACING_RATIO_BIAS, 0.5);
	data->SetFloat(FACING_RATIO_CONTRAST, 0.5);
	data->SetInt32(BUMP_TYPE, BUMP_MAP);
	data->SetFloat(BUMP_INTENSITY, 1);
	return true;
}

Bool DLFacingRatio::GetDDescription(GeListNode* node,
									Description* description,
									DESCFLAGS_DESC& flags)
{
	description->LoadDescription(DL_FACINGRATIO);
	flags |= DESCFLAGS_DESC::LOADED;
	BaseContainer* dldata = ((BaseObject*) node)->GetDataInstance();
	HideAndShowTextures(FACING_RATIO_COLOR_EDGE_GROUP_PARAM,
						FACING_RATIO_COLOR_EDGE,
						FACING_RATIO_COLOR_EDGE_SHADER,
						FACING_RATIO_COLOR_EDGE_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(FACING_RATIO_COLOR_CENTERGROUP_PARAM,
						FACING_RATIO_COLOR_CENTER,
						FACING_RATIO_COLOR_CENTER_SHADER,
						FACING_RATIO_COLOR_CENTER_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(FACING_RATIO_BIAS_GROUP_PARAM,
						FACING_RATIO_BIAS,
						FACING_RATIO_BIAS_SHADER,
						FACING_RATIO_BIAS_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(FACING_RATIO_CONTRAST_GROUP_PARAM,
						FACING_RATIO_CONTRAST,
						FACING_RATIO_CONTRAST_SHADER,
						FACING_RATIO_CONTRAST_SHADER_TEMP,
						node,
						description,
						dldata);
	return TRUE;
}

Bool DLFacingRatio::Message(GeListNode* node, Int32 type, void* data)
{
	if (!node) {
		return false;
	}

	BaseContainer* dldata = ((BaseMaterial*) node)->GetDataInstance();

	if (type == MSG_DESCRIPTION_POPUP) {
		DescriptionPopup* dp = (DescriptionPopup*) data;
		int clicked_button_id = dp->_descId[0].id;

		switch (clicked_button_id) {
		case POPUP_FACING_RATIO_COLOR_EDGE:
			FillPopupMenu(dldata, dp, FACING_RATIO_COLOR_EDGE_GROUP_PARAM);
			break;

		case POPUP_FACING_RATIO_COLOR_CENTER:
			FillPopupMenu(dldata, dp, FACING_RATIO_COLOR_CENTERGROUP_PARAM);
			break;

		case POPUP_FACING_RATIO_BIAS:
			FillPopupMenu(dldata, dp, FACING_RATIO_BIAS_GROUP_PARAM);
			break;

		case POPUP_FACING_RATIO_CONTRAST:
			FillPopupMenu(dldata, dp, FACING_RATIO_CONTRAST_GROUP_PARAM);
			break;

		default:
			break;
		}
	}

	return true;
}

Vector
DLFacingRatio::Output(BaseShader* sh, ChannelData* sd)
{
	return Vector(0, 0, 0);
}

Bool RegisterFacingRatioTexture(void)
{
	return RegisterShaderPlugin(DL_FACINGRATIO,
								"Facing Ratio"_s,
								0,
								DLFacingRatio::Alloc,
								"dl_facing_ratio"_s,
								0);
}
