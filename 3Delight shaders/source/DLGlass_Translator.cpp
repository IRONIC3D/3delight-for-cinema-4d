#include "DLGlass_Translator.h"
#include "3DelightEnvironment.h"
#include "dl_glass.h"

Delight_Glass::Delight_Glass()
{
	is_3delight_material = true;
	const char* delightpath = DelightEnv::getDelightEnvironment();
	c_shaderpath =
		delightpath + (std::string) "/osl" + (std::string) "/dlGlass.oso";

	if (c_shaderpath.c_str() && c_shaderpath[0]) {
		m_ids_to_names[Shader_Path] = std::make_pair("", &c_shaderpath[0]);
	}

	m_ids_to_names[REFLECTION_COLOR] = std::make_pair("", "reflect_color");
	m_ids_to_names[REFLECTION_COLOR_SHADER] =
		std::make_pair("outColor", "reflect_color");
	m_ids_to_names[REFLECTION_ROUGHNESS] =
		std::make_pair("", "reflect_roughness");
	m_ids_to_names[REFLECTION_ROUGHNESS_SHADER] =
		std::make_pair("outColor[0]", "reflect_roughness");
	m_ids_to_names[REFLECTION_IOR] = std::make_pair("", "reflect_ior");
	m_ids_to_names[REFLECTION_IOR_SHADER] =
		std::make_pair("outColor[0]", "reflect_ior");
	m_ids_to_names[REFLECTION_THIN_FILM] = std::make_pair("", "thin_film");
	m_ids_to_names[REFLECTION_FILM_THICKNESS] =
		std::make_pair("", "film_thickness");
	m_ids_to_names[REFLECTION_FILM_THICKNESS_SHADER] =
		std::make_pair("outColor[0]", "film_thickness");
	m_ids_to_names[REFLECTION_FILM_IOR] = std::make_pair("", "film_ior");
	m_ids_to_names[REFLECTION_FILM_IOR_SHADER] =
		std::make_pair("outColor[0]", "film_ior");
	m_ids_to_names[REFRACTION_COLOR] = std::make_pair("", "i_color");
	m_ids_to_names[REFRACTION_COLOR_SHADER] =
		std::make_pair("outColor", "i_color");
	m_ids_to_names[REFRACTION_ROUGHNESS] =
		std::make_pair("", "refract_roughness");
	m_ids_to_names[REFRACTION_ROUGHNESS_SHADER] =
		std::make_pair("outColor", "refract_roughness");
	m_ids_to_names[REFRACTION_IOR] = std::make_pair("", "refract_ior");
	m_ids_to_names[REFRACTION_IOR_SHADER] =
		std::make_pair("outColor[0]", "refract_ior");
	m_ids_to_names[VOLUMETRIC_ENABLE] = std::make_pair("", "volumetric_enable");
	m_ids_to_names[VOLUMETRIC_DENSITY] = std::make_pair("", "volumetric_density");
	m_ids_to_names[VOLUMETRIC_SCATTERING] = std::make_pair("", "volumetric_scattering_color");
	m_ids_to_names[VOLUMETRIC_TRANSPARENCY] = std::make_pair("", "volumetric_transparency_color");
	m_ids_to_names[INCANDESCENCE_COLOR] = std::make_pair("", "incandescence");
	m_ids_to_names[INCANDESCENCE_COLOR_SHADER] = std::make_pair("outColor", "incandescence");
	m_ids_to_names[INCANDESCENCE_INTENSITY] = std::make_pair("", "incandescence_intensity");
	m_ids_to_names[BUMP_TYPE] = std::make_pair("", "disp_normal_bump_type");
	m_ids_to_names[BUMP_VALUE] =
		std::make_pair("outColor", "disp_normal_bump_value");
	m_ids_to_names[BUMP_INTENSITY] =
		std::make_pair("", "disp_normal_bump_intensity");
	m_ids_to_names[DL_AOV_GROUP] = std::make_pair("outColor", "aovGroup");
}
