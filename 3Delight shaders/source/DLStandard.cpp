#include "DL_Matpreview.h"
#include "IDs.h"
#include "TextureUI_Functions.h"
#include "c4d.h"
#include "dl_standard.h"

class DL_Standard : public MaterialData
{
	INSTANCEOF(DL_CarPaint, MaterialData)

private:
	Vector color;

public:
	virtual Bool Init(GeListNode* node);
	virtual INITRENDERRESULT InitRender(BaseMaterial* mat,
										const InitRenderStruct& irs);
	virtual Bool GetDEnabling(GeListNode* node,
							  const DescID& id,
							  const GeData& t_data,
							  DESCFLAGS_ENABLE flags,
							  const BaseContainer* itemdesc);
	virtual Bool GetDDescription(GeListNode* node,
								 Description* description,
								 DESCFLAGS_DESC& flags);
	virtual Bool Message(GeListNode* node, Int32 type, void* data);
	virtual void CalcSurface(BaseMaterial* mat, VolumeData* vd);
	static NodeData* Alloc(void)
	{
		return NewObjClear(DL_Standard);
	}
};

Bool DL_Standard::Init(GeListNode* node)
{
	DL_SetDefaultMatpreview((BaseMaterial*) node);
	BaseContainer* data = ((BaseMaterial*) node)->GetDataInstance();
	data->SetFloat(BASE_VALUE, 0.8);
	data->SetVector(BASE_COLOR, HSVToRGB(Vector(0, 0, 1)));
	data->SetFloat(BASE_ROUGHNESS, 0);
	data->SetFloat(SPECULAR_VALUE, 1.0);
	data->SetVector(SPECULAR_COLOR, HSVToRGB(Vector(0, 0, 1)));
	data->SetFloat(SPECULAR_ROUGHNESS, 0.1);
	data->SetFloat(SPECULAR_METALNESS, 0);
	data->SetFloat(SPECULAR_IOR, 1.5);
	data->SetFloat(SPECULAR_ANISOTROPY, 0);
	data->SetFloat(SPECULAR_ROTATION, 0);
	data->SetFloat(COAT_VALUE, 0);
	data->SetVector(COAT_COLOR, HSVToRGB(Vector(0, 0, 1)));
	data->SetFloat(COAT_ROUGHNESS, 0.1);
	data->SetFloat(COAT_IOR, 1.5);
	data->SetFloat(COAT_AFFECT_COLOR, 0);
	data->SetFloat(COAT_AFFECT_ROUGHNESS, 0);
	data->SetFloat(THIN_FILM_THICKNESS, 0);
	data->SetFloat(THIN_FILM_IOR, 1.5);
	data->SetFloat(SUBSURFACE_VALUE, 0);
	data->SetVector(SUBSURFACE_COLOR, HSVToRGB(Vector(0, 0, 1)));
	data->SetVector(SUBSURFACE_RADIUS, HSVToRGB(Vector(0, 0, 1)));
	data->SetFloat(SUBSURFACE_SCALE, 1.0);
	data->SetFloat(SUBSURFACE_ANISOTROPY, 0);
	data->SetFloat(TRANSMISSION_VALUE, 0);
	data->SetVector(TRANSMISSION_COLOR, HSVToRGB(Vector(0, 0, 1)));
	data->SetVector(TRANSMISSION_SCATTER, HSVToRGB(Vector(0, 0, 0)));
	data->SetFloat(TRANSMISSION_DEPTH, 1.0);
	data->SetFloat(EMISSION_VALUE, 0);
	data->SetVector(EMISSION_COLOR, HSVToRGB(Vector(0, 0, 1)));
	data->SetVector(GEOMETRY_OPACITY, HSVToRGB(Vector(0, 0, 1)));
	data->SetInt32(GEOMETRY_THIN_WALLED, false);
	return true;
}

Bool DL_Standard::GetDEnabling(GeListNode* node,
							   const DescID& id,
							   const GeData& t_data,
							   DESCFLAGS_ENABLE flags,
							   const BaseContainer* itemdesc)
{
	if (!node) {
		return false;
	}

	BaseDocument* doc = GetActiveDocument();
	BaseContainer* dldata = ((BaseObject*) node)->GetDataInstance();

	switch (id[0].id) {
	default:
		break;
	}

	return true;
}

Bool DL_Standard::GetDDescription(GeListNode* node,
								  Description* description,
								  DESCFLAGS_DESC& flags)
{
	description->LoadDescription(DL_STANDARD);
	flags |= DESCFLAGS_DESC::LOADED;
	BaseContainer* dldata = ((BaseObject*) node)->GetDataInstance();
	HideAndShowTextures(BASE_VALUE_PARAM,
						BASE_VALUE,
						BASE_VALUE_SHADER,
						BASE_VALUE_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(BASE_COLOR_PARAM,
						BASE_COLOR,
						BASE_COLOR_SHADER,
						BASE_COLOR_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(BASE_ROUGHNESS_PARAM,
						BASE_ROUGHNESS,
						BASE_ROUGHNESS_SHADER,
						BASE_ROUGHNESS_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(SPECULAR_VALUE_PARAM,
						SPECULAR_VALUE,
						SPECULAR_VALUE_SHADER,
						SPECULAR_VALUE_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(SPECULAR_COLOR_PARAM,
						SPECULAR_COLOR,
						SPECULAR_COLOR_SHADER,
						SPECULAR_COLOR_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(SPECULAR_ROUGHNESS_PARAM,
						SPECULAR_ROUGHNESS,
						SPECULAR_ROUGHNESS_SHADER,
						SPECULAR_ROUGHNESS_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(SPECULAR_METALNESS_PARAM,
						SPECULAR_METALNESS,
						SPECULAR_METALNESS_SHADER,
						SPECULAR_METALNESS_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(SPECULAR_IOR_PARAM,
						SPECULAR_IOR,
						SPECULAR_IOR_SHADER,
						SPECULAR_IOR_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(SPECULAR_ANISOTROPY_PARAM,
						SPECULAR_ANISOTROPY,
						SPECULAR_ANISOTROPY_SHADER,
						SPECULAR_ANISOTROPY_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(SPECULAR_ROTATION_PARAM,
						SPECULAR_ROTATION,
						SPECULAR_ROTATION_SHADER,
						SPECULAR_ROTATION_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(COAT_VALUE_PARAM,
						COAT_VALUE,
						COAT_VALUE_SHADER,
						COAT_VALUE_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(COAT_COLOR_PARAM,
						COAT_COLOR,
						COAT_COLOR_SHADER,
						COAT_COLOR_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(COAT_ROUGHNESS_PARAM,
						COAT_ROUGHNESS,
						COAT_ROUGHNESS_SHADER,
						COAT_ROUGHNESS_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(COAT_IOR_PARAM,
						COAT_IOR,
						COAT_IOR_SHADER,
						COAT_IOR_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(COAT_AFFECT_COLOR_PARAM,
						COAT_AFFECT_COLOR,
						COAT_AFFECT_COLOR_SHADER,
						COAT_AFFECT_COLOR_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(COAT_AFFECT_ROUGHNESS_PARAM,
						COAT_AFFECT_ROUGHNESS,
						COAT_AFFECT_ROUGHNESS_SHADER,
						COAT_AFFECT_ROUGHNESS_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(THIN_FILM_THICKNESS_PARAM,
						THIN_FILM_THICKNESS,
						THIN_FILM_THICKNESS_SHADER,
						THIN_FILM_THICKNESS_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(THIN_FILM_IOR_PARAM,
						THIN_FILM_IOR,
						THIN_FILM_IOR_SHADER,
						THIN_FILM_IOR_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(SUBSURFACE_VALUE_PARAM,
						SUBSURFACE_VALUE,
						SUBSURFACE_VALUE_SHADER,
						SUBSURFACE_VALUE_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(SUBSURFACE_COLOR_PARAM,
						SUBSURFACE_COLOR,
						SUBSURFACE_COLOR_SHADER,
						SUBSURFACE_COLOR_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(SUBSURFACE_RADIUS_PARAM,
						SUBSURFACE_RADIUS,
						SUBSURFACE_RADIUS_SHADER,
						SUBSURFACE_RADIUS_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(SUBSURFACE_SCALE_PARAM,
						SUBSURFACE_SCALE,
						SUBSURFACE_SCALE_SHADER,
						SUBSURFACE_SCALE_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(SUBSURFACE_ANISOTROPY_PARAM,
						SUBSURFACE_ANISOTROPY,
						SUBSURFACE_ANISOTROPY_SHADER,
						SUBSURFACE_ANISOTROPY_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(TRANSMISSION_VALUE_PARAM,
						TRANSMISSION_VALUE,
						TRANSMISSION_VALUE_SHADER,
						TRANSMISSION_VALUE_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(TRANSMISSION_COLOR_PARAM,
						TRANSMISSION_COLOR,
						TRANSMISSION_COLOR_SHADER,
						TRANSMISSION_COLOR_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(TRANSMISSION_SCATTER_PARAM,
						TRANSMISSION_SCATTER,
						TRANSMISSION_SCATTER_SHADER,
						TRANSMISSION_SCATTER_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(TRANSMISSION_DEPTH_PARAM,
						TRANSMISSION_DEPTH,
						TRANSMISSION_DEPTH_SHADER,
						TRANSMISSION_DEPTH_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(EMISSION_VALUE_PARAM,
						EMISSION_VALUE,
						EMISSION_VALUE_SHADER,
						EMISSION_VALUE_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(EMISSION_COLOR_PARAM,
						EMISSION_COLOR,
						EMISSION_COLOR_SHADER,
						EMISSION_COLOR_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(GEOMETRY_OPACITY_PARAM,
						GEOMETRY_OPACITY,
						GEOMETRY_OPACITY_SHADER,
						GEOMETRY_OPACITY_SHADER_TEMP,
						node,
						description,
						dldata);
	return TRUE;
}

Bool DL_Standard::Message(GeListNode* node, Int32 type, void* data)
{
	if (!node) {
		return false;
	}

	BaseContainer* dldata = ((BaseMaterial*) node)->GetDataInstance();

	if (type == MSG_DESCRIPTION_POPUP) {
		DescriptionPopup* dp = (DescriptionPopup*) data;
		int clicked_button_id = dp->_descId[0].id;

		switch (clicked_button_id) {
		case POPUP_BASE_VALUE:
			FillPopupMenu(dldata, dp, BASE_VALUE_PARAM);
			break;

		case POPUP_BASE_COLOR:
			FillPopupMenu(dldata, dp, BASE_COLOR_PARAM);
			break;

		case POPUP_BASE_ROUGHNESS:
			FillPopupMenu(dldata, dp, BASE_ROUGHNESS_PARAM);
			break;

		case POPUP_SPECULAR_VALUE:
			FillPopupMenu(dldata, dp, SPECULAR_VALUE_PARAM);
			break;

		case POPUP_SPECULAR_COLOR:
			FillPopupMenu(dldata, dp, SPECULAR_COLOR_PARAM);
			break;

		case POPUP_SPECULAR_ROUGHNESS:
			FillPopupMenu(dldata, dp, SPECULAR_ROUGHNESS_PARAM);
			break;

		case POPUP_SPECULAR_METALNESS:
			FillPopupMenu(dldata, dp, SPECULAR_METALNESS_PARAM);
			break;

		case POPUP_SPECULAR_IOR:
			FillPopupMenu(dldata, dp, SPECULAR_IOR_PARAM);
			break;

		case POPUP_SPECULAR_ANISOTROPY:
			FillPopupMenu(dldata, dp, SPECULAR_ANISOTROPY_PARAM);
			break;

		case POPUP_SPECULAR_ROTATION:
			FillPopupMenu(dldata, dp, SPECULAR_ROTATION_PARAM);
			break;

		case POPUP_COAT_VALUE:
			FillPopupMenu(dldata, dp, COAT_VALUE_PARAM);
			break;

		case POPUP_COAT_COLOR:
			FillPopupMenu(dldata, dp, COAT_COLOR_PARAM);
			break;

		case POPUP_COAT_ROUGHNESS:
			FillPopupMenu(dldata, dp, COAT_ROUGHNESS_PARAM);
			break;

		case POPUP_COAT_IOR:
			FillPopupMenu(dldata, dp, COAT_IOR_PARAM);
			break;

		case POPUP_COAT_AFFECT_COLOR:
			FillPopupMenu(dldata, dp, COAT_AFFECT_COLOR_PARAM);
			break;

		case POPUP_COAT_AFFECT_ROUGHNESS:
			FillPopupMenu(dldata, dp, COAT_AFFECT_ROUGHNESS_PARAM);
			break;

		case POPUP_THIN_FILM_THICKNESS:
			FillPopupMenu(dldata, dp, THIN_FILM_THICKNESS_PARAM);
			break;

		case POPUP_THIN_FILM_IOR:
			FillPopupMenu(dldata, dp, THIN_FILM_IOR_PARAM);
			break;

		case POPUP_SUBSURFACE_VALUE:
			FillPopupMenu(dldata, dp, SUBSURFACE_VALUE_PARAM);
			break;

		case POPUP_SUBSURFACE_COLOR:
			FillPopupMenu(dldata, dp, SUBSURFACE_COLOR_PARAM);
			break;

		case POPUP_SUBSURFACE_RADIUS:
			FillPopupMenu(dldata, dp, SUBSURFACE_RADIUS_PARAM);
			break;

		case POPUP_SUBSURFACE_SCALE:
			FillPopupMenu(dldata, dp, SUBSURFACE_SCALE_PARAM);
			break;

		case POPUP_SUBSURFACE_ANISOTROPY:
			FillPopupMenu(dldata, dp, SUBSURFACE_ANISOTROPY_PARAM);
			break;

		case POPUP_TRANSMISSION_VALUE:
			FillPopupMenu(dldata, dp, TRANSMISSION_VALUE_PARAM);
			break;

		case POPUP_TRANSMISSION_COLOR:
			FillPopupMenu(dldata, dp, TRANSMISSION_COLOR_PARAM);
			break;

		case POPUP_TRANSMISSION_SCATTER:
			FillPopupMenu(dldata, dp, TRANSMISSION_SCATTER_PARAM);
			break;

		case POPUP_TRANSMISSION_DEPTH:
			FillPopupMenu(dldata, dp, TRANSMISSION_DEPTH_PARAM);
			break;

		case POPUP_EMISSION_VALUE:
			FillPopupMenu(dldata, dp, EMISSION_VALUE_PARAM);
			break;

		case POPUP_EMISSION_COLOR:
			FillPopupMenu(dldata, dp, EMISSION_COLOR_PARAM);
			break;

		case POPUP_GEOMETRY_OPACITY:
			FillPopupMenu(dldata, dp, GEOMETRY_OPACITY_PARAM);
			break;

		default:
			break;
		}
	}

	return true;
}

// Initializes resources for rendering.
INITRENDERRESULT
DL_Standard::InitRender(BaseMaterial* mat, const InitRenderStruct& irs)
{
	return INITRENDERRESULT::OK;
}

void DL_Standard::CalcSurface(BaseMaterial* mat, VolumeData* vd)
{
	Vector diff, spec;
	vd->IlluminanceSimple(&diff, &spec, 0, 0, 0);
	vd->col = 0.8 * diff;
}

Bool RegisterDLStandard(void)
{
	return RegisterMaterialPlugin(DL_STANDARD,
								  "Standard"_s,
								  PLUGINFLAG_HIDE,
								  DL_Standard::Alloc,
								  "dl_standard"_s,
								  0);
}
