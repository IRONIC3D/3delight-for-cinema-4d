#include "IDs.h"
#include "TextureUI_Functions.h"
#include "c4d.h"
#include "c4d_symbols.h"
#include "dl_worley_noise.h"

static void
initGradient(BaseContainer& bc, Int32 id)
{
	GeData data(CUSTOMDATATYPE_GRADIENT, DEFAULTVALUE);
	Gradient* gradient =
		(Gradient*) data.GetCustomDataType(CUSTOMDATATYPE_GRADIENT);

	if (gradient) {
		GradientKnot k1, k2;

		if (id == NOISE_COLORS_INSIDE_CELL) {
			k1.col = HSVToRGB(Vector(267 / 360.0, 0.97, 0.0));
			k1.pos = 0.0;
			k2.col = HSVToRGB(Vector(267.0 / 360.0, 0.0, 1.0));
			k2.pos = 1.0;
			gradient->InsertKnot(k1);
			gradient->InsertKnot(k2);

		} else if (id == NOISE_COLORS_ACROSS_CELL) {
			k1.col = HSVToRGB(Vector(267.0 / 360.0, 0.0, 1.0));
			k1.pos = 0.0;
			gradient->InsertKnot(k1);
		}
	}

	bc.SetData(id, data);
}

class DLWorleyNoise : public ShaderData
{
public:
	virtual Bool Init(GeListNode* node);
	virtual Vector Output(BaseShader* sh, ChannelData* sd);
	virtual Bool GetDDescription(GeListNode* node,
								 Description* description,
								 DESCFLAGS_DESC& flags);
	virtual Bool Message(GeListNode* node, Int32 type, void* data);
	static NodeData* Alloc(void)
	{
		return NewObjClear(DLWorleyNoise);
	}
};

Bool DLWorleyNoise::Init(GeListNode* node)
{
	BaseContainer* data = ((BaseShader*) node)->GetDataInstance();
	data->SetInt32(WORLEY_NOISE_OUTPUT_TYPE, WORLEY_NOISE_OUTPUT_TYPE_CELL_VALUE);
	data->SetInt32(WORLEY_NOISE_DISTANCE_TYPE,
				   WORLEY_NOISE_DISTANCE_TYPE_EUCLIDEAN);
	data->SetFloat(WORLEY_NOISE_MINKOWSKI_NUMBER, 3.0);
	data->SetFloat(WORLEY_NOISE_SCALE, 1.0);
	data->SetFloat(WORLEY_NOISE_DENSITY, 1.0);
	data->SetFloat(WORLEY_NOISE_RANDOM_POSITION, 1.0);
	data->SetFloat(WORLEY_NOISE_RANDOM_BRIGHTNESS, 0.0);
	data->SetFloat(WORLEY_NOISE_TIME, 0.0);
	data->SetInt32(WORLEY_NOISE_SPACE, WORLEY_NOISE_SPACE_WORLD);
	data->SetFloat(WORLEY_NOISE_DISTORTION_INTENSITY, 1.0);
	data->SetInt32(WORLEY_NOISE_LAYERS, 1);
	data->SetInt32(WORLEY_NOISE_BLEND_MODE, WORLEY_NOISE_BLEND_MODE_OVER);
	data->SetFloat(WORLEY_NOISE_PERSISTENCE, 0.7);
	data->SetFloat(WORLEY_NOISE_LAYERS_SCALE, 0.45);
	data->SetFloat(WORLEY_NOISE_LAYERS_DENSITY, 0.8);
	data->SetFloat(WORLEY_NOISE_ADJUST_AMPLITUDE, 1.0);
	data->SetFloat(WORLEY_NOISE_ADJUST_CONTRAST, 1.0);
	data->SetBool(WORLEY_NOISE_ADJUST_INVERT, false);
	initGradient(*data, NOISE_COLORS_INSIDE_CELL);
	initGradient(*data, NOISE_COLORS_ACROSS_CELL);
	return true;
}

Bool DLWorleyNoise::GetDDescription(GeListNode* node,
									Description* description,
									DESCFLAGS_DESC& flags)
{
	description->LoadDescription(DL_WorleyNoise);
	flags |= DESCFLAGS_DESC::LOADED;
	BaseContainer* dldata = ((BaseObject*) node)->GetDataInstance();
	HideAndShowTextures(WORLEY_NOISE_MINKOWSKI_NUMBER_GROUP_PARAM,
						WORLEY_NOISE_MINKOWSKI_NUMBER,
						WORLEY_NOISE_MINKOWSKI_NUMBER_SHADER,
						WORLEY_NOISE_MINKOWSKI_NUMBER_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(WORLEY_NOISE_COLOR_GROUP_PARAM,
						WORLEY_NOISE_COLOR,
						WORLEY_NOISE_COLOR_SHADER,
						WORLEY_NOISE_COLOR_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(WORLEY_NOISE_SCALE_GROUP_PARAM,
						WORLEY_NOISE_SCALE,
						WORLEY_NOISE_SCALE_SHADER,
						WORLEY_NOISE_SCALE_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(WORLEY_NOISE_DENSITY_GROUP_PARAM,
						WORLEY_NOISE_DENSITY,
						WORLEY_NOISE_DENSITY_SHADER,
						WORLEY_NOISE_DENSITY_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(WORLEY_NOISE_RANDOM_POSITION_GROUP_PARAM,
						WORLEY_NOISE_RANDOM_POSITION,
						WORLEY_NOISE_RANDOM_POSITION_SHADER,
						WORLEY_NOISE_RANDOM_POSITION_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(WORLEY_NOISE_RANDOM_BRIGHTNESS_GROUP_PARAM,
						WORLEY_NOISE_RANDOM_BRIGHTNESS,
						WORLEY_NOISE_RANDOM_BRIGHTNESS_SHADER,
						WORLEY_NOISE_RANDOM_BRIGHTNESS_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(WORLEY_NOISE_TIME_GROUP_PARAM,
						WORLEY_NOISE_TIME,
						WORLEY_NOISE_TIME_SHADER,
						WORLEY_NOISE_TIME_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(WORLEY_NOISE_BACKGROUND_COLOR_GROUP_PARAM,
						WORLEY_NOISE_BACKGROUND_COLOR,
						WORLEY_NOISE_BACKGROUND_COLOR_SHADER,
						WORLEY_NOISE_BACKGROUND_COLOR_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(WORLEY_NOISE_DISTORTION_GROUP_PARAM,
						WORLEY_NOISE_DISTORTION,
						WORLEY_NOISE_DISTORTION_SHADER,
						WORLEY_NOISE_DISTORTION_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(WORLEY_NOISE_DISTORTION_INTENSITY_GROUP_PARAM,
						WORLEY_NOISE_DISTORTION_INTENSITY,
						WORLEY_NOISE_DISTORTION_INTENSITY_SHADER,
						WORLEY_NOISE_DISTORTION_INTENSITY_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(WORLEY_NOISE_BORDERS_THICKNESS_GROUP_PARAM,
						WORLEY_NOISE_BORDERS_THICKNESS,
						WORLEY_NOISE_BORDERS_THICKNESS_SHADER,
						WORLEY_NOISE_BORDERS_THICKNESS_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(WORLEY_NOISE_BORDERS_SMOOTHNESSS_GROUP_PARAM,
						WORLEY_NOISE_BORDERS_SMOOTHNESS,
						WORLEY_NOISE_BORDERS_SMOOTHNESS_SHADER,
						WORLEY_NOISE_BORDERS_SMOOTHNESS_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(WORLEY_NOISE_PERSISTENCE_GROUP_PARAM,
						WORLEY_NOISE_PERSISTENCE,
						WORLEY_NOISE_PERSISTENCE_SHADER,
						WORLEY_NOISE_PERSISTENCE_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(WORLEY_NOISE_LAYERS_SCALE_GROUP_PARAM,
						WORLEY_NOISE_LAYERS_SCALE,
						WORLEY_NOISE_LAYERS_SCALE_SHADER,
						WORLEY_NOISE_LAYERS_SCALE_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(WORLEY_NOISE_LAYERS_DENSITY_GROUP_PARAM,
						WORLEY_NOISE_LAYERS_DENSITY,
						WORLEY_NOISE_LAYERS_DENSITY_SHADER,
						WORLEY_NOISE_LAYERS_DENSITY_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(WORLEY_NOISE_ADJUST_AMPLITUDE_GROUP_PARAM,
						WORLEY_NOISE_ADJUST_AMPLITUDE,
						WORLEY_NOISE_ADJUST_AMPLITUDE_SHADER,
						WORLEY_NOISE_ADJUST_AMPLITUDE_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(WORLEY_NOISE_ADJUST_CONTRAST__GROUP_PARAM,
						WORLEY_NOISE_ADJUST_CONTRAST,
						WORLEY_NOISE_ADJUST_CONTRAST_SHADER,
						WORLEY_NOISE_ADJUST_CONTRAST_SHADER_TEMP,
						node,
						description,
						dldata);
	return TRUE;
}

Bool DLWorleyNoise::Message(GeListNode* node, Int32 type, void* data)
{
	if (!node) {
		return false;
	}

	BaseDocument* doc = GetActiveDocument();
	BaseContainer* dldata = ((BaseMaterial*) node)->GetDataInstance();

	if (type == MSG_DESCRIPTION_POPUP) {
		DescriptionPopup* dp = (DescriptionPopup*) data;
		int clicked_button_id = dp->_descId[0].id;

		switch (clicked_button_id) {
		case POPUP_WORLEY_NOISE_MINKOWSKI_NUMBER:
			FillPopupMenu(dldata, dp, WORLEY_NOISE_MINKOWSKI_NUMBER_GROUP_PARAM);
			break;

		case POPUP_WORLEY_NOISE_COLOR:
			FillPopupMenu(dldata, dp, WORLEY_NOISE_COLOR_GROUP_PARAM);
			break;

		case POPUP_WORLEY_NOISE_SCALE:
			FillPopupMenu(dldata, dp, WORLEY_NOISE_SCALE_GROUP_PARAM);
			break;

		case POPUP_WORLEY_NOISE_DENSITY:
			FillPopupMenu(dldata, dp, WORLEY_NOISE_DENSITY_GROUP_PARAM);
			break;

		case POPUP_WORLEY_NOISE_RANDOM_POSITION:
			FillPopupMenu(dldata, dp, WORLEY_NOISE_RANDOM_POSITION_GROUP_PARAM);
			break;

		case POPUP_WORLEY_NOISE_RANDOM_BRIGHTNESS:
			FillPopupMenu(dldata, dp, WORLEY_NOISE_RANDOM_POSITION_GROUP_PARAM);
			break;

		case POPUP_WORLEY_NOISE_TIME:
			FillPopupMenu(dldata, dp, WORLEY_NOISE_TIME_GROUP_PARAM);
			break;

		case POPUP_WORLEY_NOISE_BACKGROUND_COLOR:
			FillPopupMenu(dldata, dp, WORLEY_NOISE_BACKGROUND_COLOR_GROUP_PARAM);
			break;

		case POPUP_WORLEY_NOISE_DISTORTION:
			FillPopupMenu(dldata, dp, WORLEY_NOISE_DISTORTION_GROUP_PARAM);
			break;

		case POPUP_WORLEY_NOISE_DISTORTION_INTENSITY:
			FillPopupMenu(
				dldata, dp, WORLEY_NOISE_DISTORTION_INTENSITY_GROUP_PARAM);
			break;

		case POPUP_WORLEY_NOISE_BORDERS_THICKNESS:
			FillPopupMenu(dldata, dp, WORLEY_NOISE_BORDERS_THICKNESS_GROUP_PARAM);
			break;

		case POPUP_WORLEY_NOISE_BORDERS_SMOOTHNESS:
			FillPopupMenu(dldata, dp, WORLEY_NOISE_BORDERS_SMOOTHNESSS_GROUP_PARAM);
			break;

		case POPUP_WORLEY_NOISE_PERSISTENCE:
			FillPopupMenu(dldata, dp, WORLEY_NOISE_PERSISTENCE_GROUP_PARAM);
			break;

		case POPUP_WORLEY_NOISE_LAYERS_SCALE:
			FillPopupMenu(dldata, dp, WORLEY_NOISE_LAYERS_SCALE_GROUP_PARAM);
			break;

		case POPUP_WORLEY_NOISE_LAYERS_DENSITY:
			FillPopupMenu(dldata, dp, WORLEY_NOISE_LAYERS_DENSITY_GROUP_PARAM);
			break;

		case POPUP_WORLEY_NOISE_ADJUST_AMPLITUDE:
			FillPopupMenu(dldata, dp, WORLEY_NOISE_ADJUST_AMPLITUDE_GROUP_PARAM);
			break;

		case POPUP_WORLEY_NOISE_ADJUST_CONTRAST:
			FillPopupMenu(dldata, dp, WORLEY_NOISE_ADJUST_CONTRAST__GROUP_PARAM);
			break;

		default:
			break;
		}
	}

	return true;
}

Vector
DLWorleyNoise::Output(BaseShader* sh, ChannelData* sd)
{
	return Vector(0, 0, 0);
}

Bool RegisterWorleyNoiseTexture(void)
{
	return RegisterShaderPlugin(DL_WorleyNoise,
								"Worley Noise"_s,
								0,
								DLWorleyNoise::Alloc,
								"dl_worley_noise"_s,
								0);
}
