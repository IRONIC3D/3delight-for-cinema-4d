#include "IDs.h"
#include "TextureUI_Functions.h"
#include "c4d.h"
#include "c4d_symbols.h"
#include "dl_box_noise.h"

static void
initGradient(BaseContainer& bc, Int32 id)
{
	GeData data(CUSTOMDATATYPE_GRADIENT, DEFAULTVALUE);
	Gradient* gradient =
		(Gradient*) data.GetCustomDataType(CUSTOMDATATYPE_GRADIENT);

	if (gradient) {
		GradientKnot k1, k2;

		if (id == BOX_NOISE_GRADIENT_COLOR) {
			k1.col = HSVToRGB(Vector(0, 1, 0.0));
			k1.pos = 0.0;
			k2.col = HSVToRGB(Vector(0, 0, 1.0));
			k2.pos = 1.0;
			gradient->InsertKnot(k1);
			gradient->InsertKnot(k2);
		}
	}

	bc.SetData(id, data);
}

class DLBoxNoise : public ShaderData
{
public:
	virtual Bool Init(GeListNode* node);
	virtual Bool GetDDescription(GeListNode* node,
								 Description* description,
								 DESCFLAGS_DESC& flags);
	virtual Bool GetDEnabling(GeListNode* node,
							  const DescID& id,
							  const GeData& t_data,
							  DESCFLAGS_ENABLE flags,
							  const BaseContainer* itemdesc);
	virtual Bool Message(GeListNode* node, Int32 type, void* data);
	virtual Vector Output(BaseShader* sh, ChannelData* sd);
	static NodeData* Alloc(void)
	{
		return NewObjClear(DLBoxNoise);
	}
};

Bool DLBoxNoise::Init(GeListNode* node)
{
	BaseContainer* data = ((BaseShader*) node)->GetDataInstance();
	data->SetFloat(BOX_NOISE_SCALE, 1.0);
	data->SetFloat(BOX_NOISE_SMOOTHNESS, 0.0);
	data->SetFloat(BOX_NOISE_TIME, 0.0);
	data->SetInt32(BOX_NOISE_SPACE, BOX_NOISE_SPACE_WORLD);
	data->SetInt32(BOX_NOISE_COLOR_BLEND, BOX_NOISE_COLOR_BLEND_ADD);
	data->SetVector(BOX_NOISE_DISTORTION, HSVToRGB(Vector(0, 0, 0)));
	data->SetFloat(BOX_NOISE_DISTORTION_INTENSITY, 1.0);
	data->SetInt32(BOX_NOISE_LAYERS, 1);
	data->SetFloat(BOX_NOISE_LAYERS_PERSISTENCE, 0.7);
	data->SetFloat(BOX_NOISE_LAYERS_SCALE, 0.45);
	data->SetBool(BOX_NOISE_ADJUST_INVERT, FALSE);
	data->SetFloat(BOX_NOISE_ADJUST_AMPLITUDE, 1.0);
	data->SetFloat(BOX_NOISE_ADJUST_CONTRAST, 1.0);
	initGradient(*data, BOX_NOISE_GRADIENT_COLOR);
	return true;
}

Bool DLBoxNoise::GetDDescription(GeListNode* node,
								 Description* description,
								 DESCFLAGS_DESC& flags)
{
	description->LoadDescription(DL_BOX_NOISE);
	flags |= DESCFLAGS_DESC::LOADED;
	BaseContainer* dldata = ((BaseObject*) node)->GetDataInstance();
	// BOX NOISE GROUP
	HideAndShowTextures(BOX_NOISE_SCALE_GROUP_PARAM,
						BOX_NOISE_SCALE,
						BOX_NOISE_SCALE_SHADER,
						BOX_NOISE_SCALE_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(BOX_NOISE_SMOOTHNESS_GROUP_PARAM,
						BOX_NOISE_SMOOTHNESS,
						BOX_NOISE_SMOOTHNESS_SHADER,
						BOX_NOISE_SMOOTHNESS_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(BOX_NOISE_TIME_GROUP_PARAM,
						BOX_NOISE_TIME,
						BOX_NOISE_TIME_SHADER,
						BOX_NOISE_TIME_SHADER_TEMP,
						node,
						description,
						dldata);
	// DISTORTION GROUP
	HideAndShowTextures(BOX_NOISE_DISTORTION_GROUP_PARAM,
						BOX_NOISE_DISTORTION,
						BOX_NOISE_DISTORTION_SHADER,
						BOX_NOISE_DISTORTION_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(BOX_NOISE_DISTORTION_INTENSITY_GROUP_PARAM,
						BOX_NOISE_DISTORTION_INTENSITY,
						BOX_NOISE_DISTORTION_INTENSITY_SHADER,
						BOX_NOISE_DISTORTION_INTENSITY_SHADER_TEMP,
						node,
						description,
						dldata);
	// LAYERS GROUP
	HideAndShowTextures(BOX_NOISE_LAYERS_PERSISTENCE_GROUP_PARAM,
						BOX_NOISE_LAYERS_PERSISTENCE,
						BOX_NOISE_LAYERS_PERSISTENCE_SHADER,
						BOX_NOISE_LAYERS_PERSISTENCE_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(BOX_NOISE_LAYERS_SCALE_GROUP_PARAM,
						BOX_NOISE_LAYERS_SCALE,
						BOX_NOISE_LAYERS_SCALE_SHADER,
						BOX_NOISE_LAYERS_SCALE_SHADER_TEMP,
						node,
						description,
						dldata);
	// ADJUST GROUP
	HideAndShowTextures(BOX_NOISE_ADJUST_AMPLITUDE_GROUP_PARAM,
						BOX_NOISE_ADJUST_AMPLITUDE,
						BOX_NOISE_ADJUST_AMPLITUDE_SHADER,
						BOX_NOISE_ADJUST_AMPLITUDE_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(BOX_NOISE_ADJUST_CONTRAST_GROUP_PARAM,
						BOX_NOISE_ADJUST_CONTRAST,
						BOX_NOISE_ADJUST_CONTRAST_SHADER,
						BOX_NOISE_ADJUST_CONTRAST_SHADER_TEMP,
						node,
						description,
						dldata);
	return TRUE;
}

Bool DLBoxNoise::Message(GeListNode* node, Int32 type, void* data)
{
	if (!node) {
		return false;
	}

	BaseContainer* dldata = ((BaseMaterial*) node)->GetDataInstance();

	if (type == MSG_DESCRIPTION_POPUP) {
		DescriptionPopup* dp = (DescriptionPopup*) data;
		int clicked_button_id = dp->_descId[0].id;

		switch (clicked_button_id) {
		case POPUP_BOX_NOISE_SCALE:
			FillPopupMenu(dldata, dp, BOX_NOISE_SCALE_GROUP_PARAM);
			break;

		case POPUP_BOX_NOISE_SMOOTHNESS:
			FillPopupMenu(dldata, dp, BOX_NOISE_SMOOTHNESS_GROUP_PARAM);
			break;

		case POPUP_BOX_NOISE_TIME:
			FillPopupMenu(dldata, dp, BOX_NOISE_TIME_GROUP_PARAM);
			break;

		case POPUP_BOX_NOISE_DISTORTION:
			FillPopupMenu(dldata, dp, BOX_NOISE_DISTORTION_GROUP_PARAM);
			break;

		case POPUP_BOX_NOISE_DISTORTION_INTENSITY:
			FillPopupMenu(dldata, dp, BOX_NOISE_DISTORTION_INTENSITY_GROUP_PARAM);
			break;

		case POPUP_BOX_NOISE_LAYERS_PERSISTENCE:
			FillPopupMenu(dldata, dp, BOX_NOISE_LAYERS_PERSISTENCE_GROUP_PARAM);
			break;

		case POPUP_BOX_NOISE_LAYERS_SCALE:
			FillPopupMenu(dldata, dp, BOX_NOISE_LAYERS_SCALE_GROUP_PARAM);
			break;

		case POPUP_BOX_NOISE_ADJUST_AMPLITUDE:
			FillPopupMenu(dldata, dp, BOX_NOISE_ADJUST_AMPLITUDE_GROUP_PARAM);
			break;

		case POPUP_BOX_NOISE_ADJUST_CONTRAST:
			FillPopupMenu(dldata, dp, BOX_NOISE_ADJUST_CONTRAST_GROUP_PARAM);
			break;

		default:
			break;
		}
	}

	return true;
}

Bool DLBoxNoise::GetDEnabling(GeListNode* node,
							  const DescID& id,
							  const GeData& t_data,
							  DESCFLAGS_ENABLE flags,
							  const BaseContainer* itemdesc)
{
	if (!node) {
		return false;
	}

	BaseDocument* doc = GetActiveDocument();
	BaseContainer* dldata = ((BaseObject*) node)->GetDataInstance();

	switch (id[0].id) {
	case BOX_NOISE_LAYERS_PERSISTENCE:
	case BOX_NOISE_LAYERS_PERSISTENCE_SHADER:
	case POPUP_BOX_NOISE_LAYERS_PERSISTENCE:
	case BOX_NOISE_LAYERS_SCALE:
	case BOX_NOISE_LAYERS_SCALE_SHADER:
	case POPUP_BOX_NOISE_LAYERS_SCALE:
		return (dldata->GetInt32(BOX_NOISE_LAYERS) > 1);
		break;

	default:
		break;
	}

	return true;
}

Vector
DLBoxNoise::Output(BaseShader* sh, ChannelData* sd)
{
	return Vector(0, 0, 0);
}

Bool RegisterBoxNoiseTexture(void)
{
	return RegisterShaderPlugin(
			   DL_BOX_NOISE, "Box Noise"_s, 0, DLBoxNoise::Alloc, "dl_box_noise"_s, 0);
}
