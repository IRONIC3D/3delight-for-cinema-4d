#include "DLHairAndFur_Translator.h"
#include "3DelightEnvironment.h"
#include "dl_hair_and_fur.h"

Delight_HairAndFur::Delight_HairAndFur()
{
	is_3delight_material = true;
	const char* delightpath = DelightEnv::getDelightEnvironment();
	c_shaderpath =
		delightpath + (std::string) "/osl" + (std::string) "/dlHairAndFur.oso";

	if (c_shaderpath.c_str() && c_shaderpath[0]) {
		m_ids_to_names[Shader_Path] = std::make_pair("", &c_shaderpath[0]);
	}

	m_ids_to_names[HAIR_COLOR_MELANIN] = std::make_pair("", "eumelanine");
	m_ids_to_names[HAIR_COLOR_MELANIN_SHADER] =
		std::make_pair("outColor[0]", "eumelanine");
	m_ids_to_names[HAIR_COLOR_MELANIN_RED] = std::make_pair("", "phenomelanine");
	m_ids_to_names[HAIR_COLOR_MELANIN_RED_SHADER] =
		std::make_pair("outColor[0]", "phenomelanine");
	m_ids_to_names[HAIR_COLOR_DYE] = std::make_pair("", "i_color");
	m_ids_to_names[HAIR_COLOR_DYE_SHADER] = std::make_pair("outColor", "i_color");
	m_ids_to_names[HAIR_COLOR_DYE_WEIGHT] = std::make_pair("", "dye_weight");
	m_ids_to_names[HAIR_COLOR_DYE_WEIGHT_SHADER] =
		std::make_pair("outColor[0]", "dye_weight");
	m_ids_to_names[HAIR_LOOK_SPECULAR_LEVEL] =
		std::make_pair("", "specular_level");
	m_ids_to_names[HAIR_LOOK_SPECULAR_LEVEL_SHADER] =
		std::make_pair("outColor[0]", "specular_level");
	m_ids_to_names[HAIR_LOOK_ROUGHNESS_ALONG] =
		std::make_pair("", "longitudinal_roughness");
	m_ids_to_names[HAIR_LOOK_ROUGHNESS_ALONG_SHADER] =
		std::make_pair("outColor[0]", "longitudinal_roughness");
	m_ids_to_names[HAIR_LOOK_ROUGHNESS_AROUND] =
		std::make_pair("", "azimuthal_roughness");
	m_ids_to_names[HAIR_LOOK_ROUGHNESS_AROUND_SHADER] =
		std::make_pair("outColor[0]", "azimuthal_roughness");
	m_ids_to_names[HAIR_LOOK_ROUGHNESS_AROUND] = std::make_pair("", "medulla");
	m_ids_to_names[HAIR_LOOK_ROUGHNESS_AROUND_SHADER] =
		std::make_pair("outColor[0]", "medulla");
	m_ids_to_names[HAIR_LOOK_SYNTHETIC_FIBER] = std::make_pair("", "synthetic");
	m_ids_to_names[HAIR_LOOK_SYNTHETIC_FIBER_SHADER] =
		std::make_pair("outColor[0]", "synthetic");
	m_ids_to_names[HAIR_VARIATION_MELANIN] =
		std::make_pair("", "variation_melanin");
	m_ids_to_names[HAIR_VARIATION_MELANIN_SHADER] =
		std::make_pair("outColor[0]", "variation_melanin");
	m_ids_to_names[HAIR_VARIATION_MELANIN_RED] =
		std::make_pair("", "variation_melanin_red");
	m_ids_to_names[HAIR_VARIATION_MELANIN_RED_SHADER] =
		std::make_pair("outColor[0]", "variation_melanin_red");
	m_ids_to_names[HAIR_VARIATION_WHITE_HAIR] =
		std::make_pair("", "variation_white_hair");
	m_ids_to_names[HAIR_VARIATION_WHITE_HAIR_SHADER] =
		std::make_pair("outColor[0]", "variation_white_hair");
	m_ids_to_names[HAIR_VARIATION_DYE_HUE] =
		std::make_pair("", "variation_dye_hue");
	m_ids_to_names[HAIR_VARIATION_DYE_HUE_SHADER] =
		std::make_pair("outColor[0]", "variation_dye_hue");
	m_ids_to_names[HAIR_VARIATION_DYE_SATURATION] =
		std::make_pair("", "variation_dye_saturation");
	m_ids_to_names[HAIR_VARIATION_DYE_SATURATION_SHADER] =
		std::make_pair("outColor[0]", "variation_dye_saturation");
	m_ids_to_names[HAIR_VARIATION_DYE_VALUE] =
		std::make_pair("", "variation_dye_value");
	m_ids_to_names[HAIR_VARIATION_DYE_VALUE_SHADER] =
		std::make_pair("outColor[0]", "variation_dye_value");
	m_ids_to_names[HAIR_VARIATION_ROUGHNESS] =
		std::make_pair("", "variation_roughness");
	m_ids_to_names[HAIR_VARIATION_ROUGHNESS_SHADER] =
		std::make_pair("outColor[0]", "variation_roughness");
	m_ids_to_names[HAIR_VARIATION_SPECULAR_LEVEL] =
		std::make_pair("", "variation_specular_level");
	m_ids_to_names[HAIR_VARIATION_SPECULAR_LEVEL_SHADER] =
		std::make_pair("outColor[0]", "variation_specular_level");
	m_ids_to_names[HAIR_BOOST_GLOSSINESS] =
		std::make_pair("", "boost_glossiness");
	m_ids_to_names[HAIR_BOOST_GLOSSINESS_SHADER] =
		std::make_pair("outColor[0]", "boost_glossiness");
	m_ids_to_names[HAIR_BOOST_SPECULAR_LEVEL] =
		std::make_pair("", "boost_specular");
	m_ids_to_names[HAIR_BOOST_SPECULAR_LEVEL_SHADER] =
		std::make_pair("outColor[0]", "boost_specular");
	m_ids_to_names[HAIR_BOOST_TRANSMISSION] =
		std::make_pair("", "boost_transmission");
	m_ids_to_names[HAIR_BOOST_TRANSMISSION_SHADER] =
		std::make_pair("outColor[0]", "boost_transmission");
	m_ids_to_names[DL_AOV_GROUP] = std::make_pair("outColor", "aovGroup");
}
