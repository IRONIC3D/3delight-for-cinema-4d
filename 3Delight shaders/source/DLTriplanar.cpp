#include "IDs.h"
#include "TextureUI_Functions.h"
#include "c4d.h"
#include "c4d_symbols.h"
#include "dl_triplanar.h"

class DLTriplanar : public ShaderData
{
public:
	virtual Bool Init(GeListNode* node);
	virtual Vector Output(BaseShader* sh, ChannelData* sd);
	virtual Bool GetDDescription(GeListNode* node,
								 Description* description,
								 DESCFLAGS_DESC& flags);
	virtual Bool Message(GeListNode* node, Int32 type, void* data);
	static NodeData* Alloc(void)
	{
		return NewObjClear(DLTriplanar);
	}
};

Bool DLTriplanar::Init(GeListNode* node)
{
	BaseContainer* data = ((BaseShader*) node)->GetDataInstance();
	data->SetInt32(SHADER_MODE, MODE_TRIPLANAR);
	data->SetInt32(COLOR_SPACE, COLOR_SPACE_RGB);
	data->SetInt32(FLOAT_COLOR_SPACE, COLOR_SPACE_LINEAR);
	data->SetInt32(HEIGHT_COLOR_SPACE, COLOR_SPACE_LINEAR);
	data->SetFloat(TRANSFORMATION_SCALE, 1.0);
	data->SetInt32(TRANSFORMATION_SPACE, TRANSFORMATION_SPACE_WORLD);
	data->SetFloat(PLANE_SOFTNESS, 0.5);
	data->SetFloat(PLANE_NOISE_INTENSITY, 0.0);
	data->SetFloat(PLANE_NOISE_SCALE, 1.0);
	data->SetBool(PLANE_HEIGHT_BASED_BLENDING, FALSE);
	data->SetFloat(PLANE_VARIATION_OFFSET, 0.0);
	data->SetFloat(PLANE_VARIATION_ROTATION, 0.0);
	data->SetFloat(PLANE_VARIATION_SCALE, 0.0);
	data->SetBool(TILE_REMOVAL_ENABLE, FALSE);
	data->SetFloat(TILE_REMOVAL_SOFTNESS, 0.75);
	data->SetFloat(TILE_REMOVAL_OFFSET, 1);
	data->SetFloat(TILE_REMOVAL_ROTATION, 1);
	data->SetFloat(TILE_REMOVAL_SCALE, 0.5);
	return true;
}

Bool DLTriplanar::GetDDescription(GeListNode* node,
								  Description* description,
								  DESCFLAGS_DESC& flags)
{
	description->LoadDescription(DL_TRIPLANAR);
	flags |= DESCFLAGS_DESC::LOADED;
	BaseContainer* dldata = ((BaseObject*) node)->GetDataInstance();
	HideAndShowTextures(PLANE_SOFTNESS_GROUP_PARAM,
						PLANE_SOFTNESS,
						PLANE_SOFTNESS_SHADER,
						PLANE_SOFTNESS_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(PLANE_NOISE_INTENSITY_GROUP_PARAM,
						PLANE_NOISE_INTENSITY,
						PLANE_NOISE_INTENSITY_SHADER,
						PLANE_NOISE_INTENSITY_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(PLANE_NOISE_SCALE_GROUP_PARAM,
						PLANE_NOISE_SCALE,
						PLANE_NOISE_SCALE_SHADER,
						PLANE_NOISE_SCALE_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(TILE_REMOVAL_SOFTNESS_GROUP_PARAM,
						TILE_REMOVAL_SOFTNESS,
						TILE_REMOVAL_SOFTNESS_SHADER,
						TILE_REMOVAL_SOFTNESS_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(TILE_REMOVAL_OFFSET_GROUP_PARAM,
						TILE_REMOVAL_OFFSET,
						TILE_REMOVAL_OFFSET_SHADER,
						TILE_REMOVAL_OFFSET_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(TILE_REMOVAL_ROTATION_GROUP_PARAM,
						TILE_REMOVAL_ROTATION,
						TILE_REMOVAL_ROTATION_SHADER,
						TILE_REMOVAL_ROTATION_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(TILE_REMOVAL_SCALE_GROUP_PARAM,
						TILE_REMOVAL_SCALE,
						TILE_REMOVAL_SCALE_SHADER,
						TILE_REMOVAL_SCALE_SHADER_TEMP,
						node,
						description,
						dldata);
	return TRUE;
}

Bool DLTriplanar::Message(GeListNode* node, Int32 type, void* data)
{
	if (!node) {
		return false;
	}

	BaseContainer* dldata = ((BaseMaterial*) node)->GetDataInstance();

	if (type == MSG_DESCRIPTION_POPUP) {
		DescriptionPopup* dp = (DescriptionPopup*) data;
		int clicked_button_id = dp->_descId[0].id;

		switch (clicked_button_id) {
		case POPUP_PLANE_SOFTNESS:
			FillPopupMenu(dldata, dp, PLANE_SOFTNESS_GROUP_PARAM);
			break;

		case POPUP_PLANE_NOISE_INTENSITY:
			FillPopupMenu(dldata, dp, PLANE_NOISE_INTENSITY_GROUP_PARAM);
			break;

		case POPUP_PLANE_NOISE_SCALE:
			FillPopupMenu(dldata, dp, PLANE_NOISE_SCALE_GROUP_PARAM);
			break;

		case POPUP_TILE_REMOVAL_SOFTNESS:
			FillPopupMenu(dldata, dp, TILE_REMOVAL_SOFTNESS_GROUP_PARAM);
			break;

		case POPUP_TILE_REMOVAL_OFFSET:
			FillPopupMenu(dldata, dp, TILE_REMOVAL_OFFSET_GROUP_PARAM);
			break;

		case POPUP_TILE_REMOVAL_ROTATION:
			FillPopupMenu(dldata, dp, TILE_REMOVAL_ROTATION_GROUP_PARAM);
			break;

		case POPUP_TILE_REMOVAL_SCALE:
			FillPopupMenu(dldata, dp, TILE_REMOVAL_SCALE_GROUP_PARAM);
			break;

		default:
			break;
		}
	}

	return true;
}

Vector
DLTriplanar::Output(BaseShader* sh, ChannelData* sd)
{
	return Vector(0, 0, 0);
}

Bool RegisterTriplanarTexture(void)
{
	return RegisterShaderPlugin(
			   DL_TRIPLANAR, "Triplanar"_s, 0, DLTriplanar::Alloc, "dl_triplanar"_s, 0);
}
