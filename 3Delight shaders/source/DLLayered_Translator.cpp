#include "DLLayered_Translator.h"
#include "3DelightEnvironment.h"
#include "dl_layered.h"

Delight_Layered::Delight_Layered()
{
	is_3delight_material = true;
	const char* delightpath = DelightEnv::getDelightEnvironment();
	c_shaderpath =
		delightpath + (std::string) "/osl" + (std::string) "/dlLayeredMaterial.oso";

	if (c_shaderpath.c_str() && c_shaderpath[0]) {
		m_ids_to_names[Shader_Path] = std::make_pair("", &c_shaderpath[0]);
	}

	m_ids_to_names[TOP_LAYER_MASK] = std::make_pair("", "top_mask");
	m_ids_to_names[TOP_LAYER_MASK_SHADER] =
		std::make_pair("outColor[0]", "top_mask");
	m_ids_to_names[MIDDLE_LAYER_MASK] = std::make_pair("", "middle_mask");
	m_ids_to_names[MIDDLE_LAYER_MASK_SHADER] =
		std::make_pair("outColor[0]", "middle_mask");
	m_ids_to_names[BOTTOM_LAYER_MASK] = std::make_pair("", "bottom_mask");
	m_ids_to_names[BOTTOM_LAYER_MASK_SHADER] =
		std::make_pair("outColor[0]", "bottom_mask");
	m_ids_to_names[TOP_LAYER_LINK] = std::make_pair("outColor", "i_color");
	m_ids_to_names[MIDDLE_LAYER_LINK] =
		std::make_pair("outColor", "middle_layer");
	m_ids_to_names[BOTTOM_LAYER_LINK] =
		std::make_pair("outColor", "bottom_layer");
	m_ids_to_names[DL_AOV_GROUP] = std::make_pair("outColor", "aovGroup");
}
