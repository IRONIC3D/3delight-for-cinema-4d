#pragma once

#include "DLCustomAOVS.h"
#include "c4d.h"
#define DEFAULT_ID 20
#define TEXTURE_ID 30

// This function is used to show or hide an element in the Materials UI.
inline Bool
ShowDescriptionElement(GeListNode* node,
					   Description* descr,
					   Int32 MyDescID,
					   Bool show)
{
	AutoAlloc<AtomArray> ar;
	ar->Append(static_cast<C4DAtom*>(node));
	BaseContainer* bc = descr->GetParameterI(DescLevel(MyDescID), ar);

	if (!bc) {
		return FALSE;
	}

	bc->SetBool(DESC_HIDE, !show);
	return TRUE;
}

/*
	This function is used to hide and show the paramters value and texture
	depending on the user's choice. If the user chooses Default value from the
	popup button then the paramters value will be shown (Int, Color, etc.)
		Otherwise if Texture is selected then the texture field for the paramter
	will be shown.
		If a shader is selected on a paramter and we are want to use it's value
	instead, we can not do this unless the texture of that paramter is null.
	Since we partially support Cinema4D standard material we don't want to change
	the way how we get material's parameters and checking if we have selected
	default or texture. Instead we are doing everything in this function. For
	each texture we have created a temporary parameter. Whenever we select
	default we assign the current texture (if there is) to the temporary texture
	and make the current one null so we use it's value. Then if we go back to
	texture we assign the temporary value back to the current texture so no
	information of the previous texture is lost.
*/
inline void
HideAndShowTextures(int group_id,
					int value_id,
					int texture_id,
					int temp_texture,
					GeListNode* node,
					Description* description,
					BaseContainer* data)
{
	// if default selected, show parameter value and set texture to null
	if (data->GetInt32(group_id) == DEFAULT_ID) {
		ShowDescriptionElement(node, description, value_id, true);
		ShowDescriptionElement(node, description, texture_id, false);
		BaseList2D* shader = data->GetLink(texture_id, GetActiveDocument());

		if (!shader) {
			return;
		}

		data->SetLink(texture_id, nullptr);
		data->SetLink(temp_texture, shader);
	}

	// if texture selected, show parameter's texture and hide parameter's value
	else if (data->GetInt32(group_id) == TEXTURE_ID) {
		ShowDescriptionElement(node, description, value_id, false);
		ShowDescriptionElement(node, description, texture_id, true);
		BaseList2D* shader = data->GetLink(temp_texture, GetActiveDocument());

		if (!shader) {
			return;
		}

		data->SetLink(texture_id, shader);
		data->SetLink(temp_texture, nullptr);
	}
}

/*
	This function is used to fill the menu of the popup button since we can
	not add them using a resource file. This function is being called in the
	Message function, MSG_DESCRIPTION_POPUP Then we check the id of the clicked
	popup button to add these two values.
*/
inline void
FillPopupMenu(BaseContainer* data, DescriptionPopup* dp, int group_parameter_id)
{
	if (dp->_chosen == 0) {
		dp->_popup.InsData(DEFAULT_ID, "default");
		dp->_popup.InsData(TEXTURE_ID, "texture");
	}

	data->SetInt32(group_parameter_id, dp->_chosen);
}

inline Bool
AddCycleButton(Description* dc,
			   Int32 id,
			   const DescID& groupid,
			   DLCustomAOVS dlAov)
{
	const DescID* singleid = dc->GetSingleDescID();

	if (!singleid || ((DescID) id).IsPartOf(*singleid, NULL)) {
		BaseContainer settings = GetCustomDataTypeDefault(DTYPE_LONG);
		settings.SetString(DESC_NAME, "Aov Group"_s);
		settings.SetInt32(DESC_CUSTOMGUI, CUSTOMGUI_CYCLE);
		settings.SetInt32(DESC_SCALEH, 1);
		// add cycle elements
		BaseContainer items;
		items.SetString(0, "<none>"_s);

		if (dlAov.customAOV.size() > 0) {
			for (int i = 0; i < dlAov.customAOV.size(); i++) {
				items.SetString(i + 1, (maxon::String) dlAov.customAOV[i].name.c_str());
			}
		}

		settings.SetContainer(DESC_CYCLE, items);
		//  Create CycleButton
		return dc->SetParameter(DescLevel(id, DTYPE_LONG, 0), settings, groupid);
	}

	return TRUE;
}

/*
Bool AddPopUp(Description *dc, Int32 id, const DescID &groupid)
{
	const DescID* singleid = dc->GetSingleDescID();

	if (!singleid || ((DescID)id).IsPartOf(*singleid, NULL))
	{
		BaseContainer bc = GetCustomDataTypeDefault(DTYPE_POPUP);

		//  Set CycleButton properties
		bc.SetBool(DESC_ANIMATE, false);
		bc.SetInt32(DESC_CUSTOMGUI, CUSTOMGUI_POPUP);
		bc.SetBool(DESC_GROUPSCALEV, TRUE);
		//  Create CycleButton
		return dc->SetParameter(DescLevel(id, DTYPE_POPUP, 0), bc, groupid);
	}

	return TRUE;
}
*/
