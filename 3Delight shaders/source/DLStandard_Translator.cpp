#include "DLStandard_Translator.h"
#include "3DelightEnvironment.h"
#include "NSIExportMaterial.h"
#include "dl_standard.h"

Delight_Standard::Delight_Standard()
{
	is_3delight_material = true;
	const char* delightpath = DelightEnv::getDelightEnvironment();
	c_shaderpath =
		delightpath + (std::string) "/osl" + (std::string) "/dlStandard.oso";

	if (c_shaderpath.c_str() && c_shaderpath[0]) {
		m_ids_to_names[Shader_Path] = std::make_pair("", &c_shaderpath[0]);
	}

	// Base Group
	m_ids_to_names[BASE_VALUE] = std::make_pair("", "base");
	m_ids_to_names[BASE_VALUE_SHADER] = std::make_pair("outColor[0]", "base");
	m_ids_to_names[BASE_COLOR] = std::make_pair("", "base_color");
	m_ids_to_names[BASE_COLOR_SHADER] = std::make_pair("outColor", "base_color");
	m_ids_to_names[BASE_ROUGHNESS] = std::make_pair("", "diffuse_roughness");
	m_ids_to_names[BASE_ROUGHNESS_SHADER] =
		std::make_pair("outColor[0]", "diffuse_roughness");
	// Specular Group
	m_ids_to_names[SPECULAR_VALUE] = std::make_pair("", "specular");
	m_ids_to_names[SPECULAR_VALUE_SHADER] =
		std::make_pair("outColor[0]", "specular");
	m_ids_to_names[SPECULAR_COLOR] = std::make_pair("", "specular_color");
	m_ids_to_names[SPECULAR_COLOR_SHADER] =
		std::make_pair("outColor", "specular_color");
	m_ids_to_names[SPECULAR_ROUGHNESS] = std::make_pair("", "specular_roughness");
	m_ids_to_names[SPECULAR_ROUGHNESS_SHADER] =
		std::make_pair("outColor[0]", "specular_roughness");
	m_ids_to_names[SPECULAR_METALNESS] = std::make_pair("", "metalness");
	m_ids_to_names[SPECULAR_METALNESS_SHADER] =
		std::make_pair("outColor[0]", "metalness");
	m_ids_to_names[SPECULAR_IOR] = std::make_pair("", "specular_IOR");
	m_ids_to_names[SPECULAR_IOR_SHADER] =
		std::make_pair("outColor[0]", "specular_IOR");
	m_ids_to_names[SPECULAR_ANISOTROPY] =
		std::make_pair("", "specular_anisotropy");
	m_ids_to_names[SPECULAR_ANISOTROPY_SHADER] =
		std::make_pair("outColor[0]", "specular_anisotropy");
	m_ids_to_names[SPECULAR_ROTATION] = std::make_pair("", "specular_rotation");
	m_ids_to_names[SPECULAR_ROTATION_SHADER] =
		std::make_pair("outColor[0]", "specular_rotation");
	// Coat Group
	m_ids_to_names[COAT_VALUE] = std::make_pair("", "coat");
	m_ids_to_names[COAT_VALUE_SHADER] = std::make_pair("outColor[0]", "coat");
	m_ids_to_names[COAT_COLOR] = std::make_pair("", "coat_color");
	m_ids_to_names[COAT_COLOR_SHADER] = std::make_pair("outColor", "coat_color");
	m_ids_to_names[COAT_ROUGHNESS] = std::make_pair("", "coat_roughness");
	m_ids_to_names[COAT_ROUGHNESS_SHADER] =
		std::make_pair("outColor[0]", "coat_roughness");
	m_ids_to_names[COAT_IOR] = std::make_pair("", "coat_IOR");
	m_ids_to_names[COAT_IOR_SHADER] = std::make_pair("outColor[0]", "coat_IOR");
	m_ids_to_names[COAT_AFFECT_COLOR] = std::make_pair("", "coat_affect_color");
	m_ids_to_names[COAT_AFFECT_COLOR_SHADER] =
		std::make_pair("outColor[0]", "coat_affect_color");
	m_ids_to_names[COAT_AFFECT_ROUGHNESS] =
		std::make_pair("", "coat_affect_roughness");
	m_ids_to_names[COAT_AFFECT_ROUGHNESS_SHADER] =
		std::make_pair("outColor[0]", "coat_affect_roughness");
	// Thin Film Group
	m_ids_to_names[THIN_FILM_THICKNESS] =
		std::make_pair("", "thin_film_thickness");
	m_ids_to_names[THIN_FILM_THICKNESS_SHADER] =
		std::make_pair("outColor[0]", "thin_film_thickness");
	m_ids_to_names[THIN_FILM_IOR] = std::make_pair("", "thin_film_IOR");
	m_ids_to_names[THIN_FILM_IOR_SHADER] =
		std::make_pair("outColor[0]", "thin_film_IOR");
	// Subsurface Group
	m_ids_to_names[SUBSURFACE_VALUE] = std::make_pair("", "i_subsurface");
	m_ids_to_names[SUBSURFACE_VALUE_SHADER] =
		std::make_pair("outColor[0]", "i_subsurface");
	m_ids_to_names[SUBSURFACE_COLOR] = std::make_pair("", "subsurface_color");
	m_ids_to_names[SUBSURFACE_COLOR_SHADER] =
		std::make_pair("outColor", "subsurface_color");
	m_ids_to_names[SUBSURFACE_RADIUS] = std::make_pair("", "subsurface_radius");
	m_ids_to_names[SUBSURFACE_RADIUS_SHADER] =
		std::make_pair("outColor", "subsurface_radius");
	m_ids_to_names[SUBSURFACE_SCALE] = std::make_pair("", "subsurface_scale");
	m_ids_to_names[SUBSURFACE_SCALE_SHADER] =
		std::make_pair("outColor[0]", "subsurface_scale");
	m_ids_to_names[SUBSURFACE_ANISOTROPY] =
		std::make_pair("", "subsurface_anisotropy");
	m_ids_to_names[SUBSURFACE_ANISOTROPY_SHADER] =
		std::make_pair("outColor[0]", "subsurface_anisotropy");
	// Transmission Group
	m_ids_to_names[TRANSMISSION_VALUE] = std::make_pair("", "transmission");
	m_ids_to_names[TRANSMISSION_VALUE_SHADER] =
		std::make_pair("outColor[0]", "transmission");
	m_ids_to_names[TRANSMISSION_COLOR] =
		std::make_pair("", "volumetric_transparency_color");
	m_ids_to_names[TRANSMISSION_COLOR_SHADER] =
		std::make_pair("outColor", "volumetric_transparency_color");
	m_ids_to_names[TRANSMISSION_SCATTER] =
		std::make_pair("", "volumetric_scattering_color");
	m_ids_to_names[TRANSMISSION_SCATTER_SHADER] =
		std::make_pair("outColor", "volumetric_scattering_color");
	m_ids_to_names[TRANSMISSION_DEPTH] = std::make_pair("", "volumetric_density");
	m_ids_to_names[TRANSMISSION_DEPTH_SHADER] =
		std::make_pair("outColor[0]", "volumetric_density");
	// Emission Group
	m_ids_to_names[EMISSION_VALUE] = std::make_pair("", "emission_w");
	m_ids_to_names[EMISSION_VALUE_SHADER] =
		std::make_pair("outColor[0]", "emission_w");
	m_ids_to_names[EMISSION_COLOR] = std::make_pair("", "emission_color");
	m_ids_to_names[EMISSION_COLOR_SHADER] =
		std::make_pair("outColor", "emission_color");
	// Geometry Group
	m_ids_to_names[GEOMETRY_OPACITY] = std::make_pair("", "opacity");
	m_ids_to_names[GEOMETRY_OPACITY_SHADER] =
		std::make_pair("outColor", "opacity");
	m_ids_to_names[GEOMETRY_BASE_NORMAL] =
		std::make_pair("outColor", "input_normal");
	m_ids_to_names[GEOMETRY_COAT_NORMAL] =
		std::make_pair("outColor", "coat_normal");
	m_ids_to_names[GEOMETRY_THIN_WALLED] = std::make_pair("", "thin_walled");
}
