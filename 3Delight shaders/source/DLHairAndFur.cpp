#include "DL_Matpreview.h"
#include "IDs.h"
#include "TextureUI_Functions.h"
#include "c4d.h"
#include "c4d_symbols.h"
#include "customgui_matpreview.h"
#include "dl_hair_and_fur.h"

class DL_Hair_Fur : public MaterialData
{
	INSTANCEOF(DL_Hair_Fur, MaterialData)

private:
	Vector color;

public:
	virtual Bool Init(GeListNode* node);
	virtual void CalcSurface(BaseMaterial* mat, VolumeData* vd);
	virtual Bool GetDDescription(GeListNode* node,
								 Description* description,
								 DESCFLAGS_DESC& flags);
	virtual Bool Message(GeListNode* node, Int32 type, void* data);
	virtual INITRENDERRESULT InitRender(BaseMaterial* mat,
										const InitRenderStruct& irs);

	static NodeData* Alloc(void)
	{
		return NewObjClear(DL_Hair_Fur);
	}
};

Bool DL_Hair_Fur::Init(GeListNode* node)
{
	// Set default matpreview scene
	DL_SetDefaultMatpreview((BaseMaterial*) node);
	BaseContainer* data = ((BaseMaterial*) node)->GetDataInstance();
	data->SetFloat(HAIR_COLOR_MELANIN, 0.6);
	data->SetFloat(HAIR_COLOR_MELANIN_RED, 0.1);
	data->SetVector(HAIR_COLOR_DYE,
					HSVToRGB(Vector(38.182 / 360.0, 0.234, 0.94)));
	data->SetFloat(HAIR_COLOR_DYE_WEIGHT, 0);
	data->SetFloat(HAIR_LOOK_SPECULAR_LEVEL, 0.5);
	data->SetFloat(HAIR_LOOK_ROUGHNESS_ALONG, 0.3);
	data->SetFloat(HAIR_LOOK_ROUGHNESS_AROUND, 0.3);
	data->SetFloat(HAIR_LOOK_MEDULLA, 0);
	data->SetFloat(HAIR_LOOK_SYNTHETIC_FIBER, 0);
	data->SetFloat(HAIR_VARIATION_MELANIN, 0);
	data->SetFloat(HAIR_VARIATION_MELANIN_RED, 0);
	data->SetFloat(HAIR_VARIATION_WHITE_HAIR, 0);
	data->SetFloat(HAIR_VARIATION_DYE_HUE, 0);
	data->SetFloat(HAIR_VARIATION_DYE_SATURATION, 0);
	data->SetFloat(HAIR_VARIATION_DYE_VALUE, 0);
	data->SetFloat(HAIR_VARIATION_ROUGHNESS, 0);
	data->SetFloat(HAIR_VARIATION_SPECULAR_LEVEL, 0);
	data->SetFloat(HAIR_BOOST_GLOSSINESS, 0);
	data->SetFloat(HAIR_BOOST_SPECULAR_LEVEL, 0);
	data->SetFloat(HAIR_BOOST_TRANSMISSION, 0);
	return true;
}

Bool DL_Hair_Fur::GetDDescription(GeListNode* node,
								  Description* description,
								  DESCFLAGS_DESC& flags)
{
	description->LoadDescription(DL_HAIRANDFUR);
	flags |= DESCFLAGS_DESC::LOADED;
	BaseContainer* dldata = ((BaseObject*) node)->GetDataInstance();
	HideAndShowTextures(HAIR_COLOR_MELANIN_GROUP_PARAM,
						HAIR_COLOR_MELANIN,
						HAIR_COLOR_MELANIN_SHADER,
						HAIR_COLOR_MELANIN_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(HAIR_COLOR_MELANIN_RED_GROUP_PARAM,
						HAIR_COLOR_MELANIN_RED,
						HAIR_COLOR_MELANIN_RED_SHADER,
						HAIR_COLOR_MELANIN_RED_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(HAIR_COLOR_DYE_GROUP_PARAM,
						HAIR_COLOR_DYE,
						HAIR_COLOR_DYE_SHADER,
						HAIR_COLOR_DYE_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(HAIR_COLOR_DYE_WEIGHT_GROUP_PARAM,
						HAIR_COLOR_DYE_WEIGHT,
						HAIR_COLOR_DYE_WEIGHT_SHADER,
						HAIR_COLOR_DYE_WEIGHT_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(HAIR_LOOK_SPECULAR_LEVEL_GROUP_PARAM,
						HAIR_LOOK_SPECULAR_LEVEL,
						HAIR_LOOK_SPECULAR_LEVEL_SHADER,
						HAIR_LOOK_SPECULAR_LEVEL_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(HAIR_LOOK_ROUGHNESS_ALONG_GROUP_PARAM,
						HAIR_LOOK_ROUGHNESS_ALONG,
						HAIR_LOOK_ROUGHNESS_ALONG_SHADER,
						HAIR_LOOK_ROUGHNESS_ALONG_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(HAIR_LOOK_ROUGHNESS_AROUND_GROUP_PARAM,
						HAIR_LOOK_ROUGHNESS_AROUND,
						HAIR_LOOK_ROUGHNESS_AROUND_SHADER,
						HAIR_LOOK_ROUGHNESS_AROUND_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(HAIR_LOOK_MEDULLA_GROUP_PARAM,
						HAIR_LOOK_MEDULLA,
						HAIR_LOOK_MEDULLA_SHADER,
						HAIR_LOOK_MEDULLA_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(HAIR_LOOK_SYNTHETIC_FIBER_GROUP_PARAM,
						HAIR_LOOK_SYNTHETIC_FIBER,
						HAIR_LOOK_SYNTHETIC_FIBER_SHADER,
						HAIR_LOOK_SYNTHETIC_FIBER_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(HAIR_VARIATION_MELANIN_GROUP_PARAM,
						HAIR_VARIATION_MELANIN,
						HAIR_VARIATION_MELANIN_SHADER,
						HAIR_VARIATION_MELANIN_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(HAIR_VARIATION_MELANIN_RED_GROUP_PARAM,
						HAIR_VARIATION_MELANIN_RED,
						HAIR_VARIATION_MELANIN_RED_SHADER,
						HAIR_VARIATION_MELANIN_RED_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(HAIR_VARIATION_WHITE_HAIR_GROUP_PARAM,
						HAIR_VARIATION_WHITE_HAIR,
						HAIR_VARIATION_WHITE_HAIR_SHADER,
						HAIR_VARIATION_WHITE_HAIR_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(HAIR_VARIATION_DYE_HUE_GROUP_PARAM,
						HAIR_VARIATION_DYE_HUE,
						HAIR_VARIATION_DYE_HUE_SHADER,
						HAIR_VARIATION_DYE_HUE_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(HAIR_VARIATION_DYE_SATURATION_GROUP_PARAM,
						HAIR_VARIATION_DYE_SATURATION,
						HAIR_VARIATION_DYE_SATURATION_SHADER,
						HAIR_VARIATION_DYE_SATURATION_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(HAIR_VARIATION_DYE_VALUE_GROUP_PARAM,
						HAIR_VARIATION_DYE_VALUE,
						HAIR_VARIATION_DYE_VALUE_SHADER,
						HAIR_VARIATION_DYE_VALUE_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(HAIR_VARIATION_ROUGHNESS_GROUP_PARAM,
						HAIR_VARIATION_ROUGHNESS,
						HAIR_VARIATION_ROUGHNESS_SHADER,
						HAIR_VARIATION_ROUGHNESS_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(HAIR_VARIATION_SPECULAR_LEVEL_GROUP_PARAM,
						HAIR_VARIATION_SPECULAR_LEVEL,
						HAIR_VARIATION_SPECULAR_LEVEL_SHADER,
						HAIR_VARIATION_SPECULAR_LEVEL_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(HAIR_BOOST_GLOSSINESS_GROUP_PARAM,
						HAIR_BOOST_GLOSSINESS,
						HAIR_BOOST_GLOSSINESS_SHADER,
						HAIR_BOOST_GLOSSINESS_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(HAIR_BOOST_SPECULAR_LEVEL_GROUP_PARAM,
						HAIR_BOOST_SPECULAR_LEVEL,
						HAIR_BOOST_SPECULAR_LEVEL_SHADER,
						HAIR_BOOST_SPECULAR_LEVEL_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(HAIR_BOOST_TRANSMISSION_GROUP_PARAM,
						HAIR_BOOST_TRANSMISSION,
						HAIR_BOOST_TRANSMISSION_SHADER,
						HAIR_BOOST_TRANSMISSION_SHADER_TEMP,
						node,
						description,
						dldata);
	return TRUE;
}

Bool DL_Hair_Fur::Message(GeListNode* node, Int32 type, void* data)
{
	if (!node) {
		return false;
	}

	BaseContainer* dldata = ((BaseMaterial*) node)->GetDataInstance();

	if (type == MSG_DESCRIPTION_POPUP) {
		DescriptionPopup* dp = (DescriptionPopup*) data;
		int clicked_button_id = dp->_descId[0].id;

		switch (clicked_button_id) {
		case POPUP_HAIR_COLOR_MELANIN:
			FillPopupMenu(dldata, dp, HAIR_COLOR_MELANIN_GROUP_PARAM);
			break;

		case POPUP_HAIR_COLOR_MELANIN_RED:
			FillPopupMenu(dldata, dp, HAIR_COLOR_MELANIN_RED_GROUP_PARAM);
			break;

		case POPUP_HAIR_COLOR_DYE:
			FillPopupMenu(dldata, dp, HAIR_COLOR_DYE_GROUP_PARAM);
			break;

		case POPUP_HAIR_COLOR_DYE_WEIGHT:
			FillPopupMenu(dldata, dp, HAIR_COLOR_DYE_WEIGHT_GROUP_PARAM);
			break;

		case POPUP_HAIR_LOOK_SPECULAR_LEVEL:
			FillPopupMenu(dldata, dp, HAIR_LOOK_SPECULAR_LEVEL_GROUP_PARAM);
			break;

		case POPUP_HAIR_LOOK_ROUGHNESS_ALONG:
			FillPopupMenu(dldata, dp, HAIR_LOOK_ROUGHNESS_ALONG_GROUP_PARAM);
			break;

		case POPUP_HAIR_LOOK_ROUGHNESS_AROUND:
			FillPopupMenu(dldata, dp, HAIR_LOOK_ROUGHNESS_AROUND_GROUP_PARAM);
			break;

		case POPUP_HAIR_LOOK_MEDULLA:
			FillPopupMenu(dldata, dp, HAIR_LOOK_MEDULLA_GROUP_PARAM);
			break;

		case POPUP_HAIR_LOOK_SYNTHETIC_FIBER:
			FillPopupMenu(dldata, dp, HAIR_LOOK_SYNTHETIC_FIBER_GROUP_PARAM);
			break;

		case POPUP_HAIR_VARIATION_MELANIN:
			FillPopupMenu(dldata, dp, HAIR_VARIATION_MELANIN_GROUP_PARAM);
			break;

		case POPUP_HAIR_VARIATION_MELANIN_RED:
			FillPopupMenu(dldata, dp, HAIR_VARIATION_MELANIN_RED_GROUP_PARAM);
			break;

		case POPUP_HAIR_VARIATION_WHITE_HAIR:
			FillPopupMenu(dldata, dp, HAIR_VARIATION_WHITE_HAIR_GROUP_PARAM);
			break;

		case POPUP_HAIR_VARIATION_DYE_HUE:
			FillPopupMenu(dldata, dp, HAIR_VARIATION_DYE_HUE_GROUP_PARAM);
			break;

		case POPUP_HAIR_VARIATION_DYE_SATURATION:
			FillPopupMenu(dldata, dp, HAIR_VARIATION_DYE_SATURATION_GROUP_PARAM);
			break;

		case POPUP_HAIR_VARIATION_DYE_VALUE:
			FillPopupMenu(dldata, dp, HAIR_VARIATION_DYE_VALUE_GROUP_PARAM);
			break;

		case POPUP_HAIR_VARIATION_ROUGHNESS:
			FillPopupMenu(dldata, dp, HAIR_VARIATION_ROUGHNESS_GROUP_PARAM);
			break;

		case POPUP_HAIR_VARIATION_SPECULAR_LEVEL:
			FillPopupMenu(dldata, dp, HAIR_VARIATION_SPECULAR_LEVEL_GROUP_PARAM);
			break;

		case POPUP_HAIR_BOOST_GLOSSINESS:
			FillPopupMenu(dldata, dp, HAIR_BOOST_GLOSSINESS_GROUP_PARAM);
			break;

		case POPUP_HAIR_BOOST_SPECULAR_LEVEL:
			FillPopupMenu(dldata, dp, HAIR_BOOST_SPECULAR_LEVEL_GROUP_PARAM);
			break;

		case POPUP_HAIR_BOOST_TRANSMISSION:
			FillPopupMenu(dldata, dp, HAIR_BOOST_TRANSMISSION_GROUP_PARAM);
			break;

		default:
			break;
		}
	}

	return true;
}

INITRENDERRESULT
DL_Hair_Fur::InitRender(BaseMaterial* mat, const InitRenderStruct& irs)
{
	return INITRENDERRESULT::OK;
}

void DL_Hair_Fur::CalcSurface(BaseMaterial* mat, VolumeData* vd)
{
	Vector diff, spec;
	vd->IlluminanceSimple(&diff, &spec, 0, 0, 0);
	vd->col = 0.8 * diff;
}

Bool RegisterDLHairAndFur(void)
{
	return RegisterMaterialPlugin(DL_HAIRANDFUR,
								  "Hair and Fur"_s,
								  PLUGINFLAG_HIDE,
								  DL_Hair_Fur::Alloc,
								  "dl_hair_and_fur"_s,
								  0);
}
