#include "DLSubstance_Translator.h"
#include "3DelightEnvironment.h"
#include "dl_substance.h"

Delight_Substance::Delight_Substance()
{
	is_3delight_material = true;
	const char* delightpath = DelightEnv::getDelightEnvironment();
	c_shaderpath =
		delightpath + (std::string) "/osl" + (std::string) "/dlSubstance.oso";

	if (c_shaderpath.c_str() && c_shaderpath[0]) {
		m_ids_to_names[Shader_Path] = std::make_pair("", &c_shaderpath[0]);
	}

	m_ids_to_names[SUBSTANCE_COLOR] = std::make_pair("", "i_color");
	m_ids_to_names[SUBSTANCE_COLOR_SHADER] =
		std::make_pair("outColor", "i_color");
	m_ids_to_names[SUBSTANCE_ROUGHNESS] = std::make_pair("", "roughness");
	m_ids_to_names[SUBSTANCE_ROUGHNESS_SHADER] =
		std::make_pair("outColor[0]", "roughness");
	m_ids_to_names[SUBSTANCE_SPECULAR_LEVEL] =
		std::make_pair("", "specular_level");
	m_ids_to_names[SUBSTANCE_SPECULAR_LEVEL_SHADER] =
		std::make_pair("outColor[0]", "specular_level");
	m_ids_to_names[SUBSTANCE_METALLIC] = std::make_pair("", "metallic");
	m_ids_to_names[SUBSTANCE_METALLIC_SHADER] =
		std::make_pair("outColor[0]", "metallic");
	m_ids_to_names[SUBSTANCE_OPACITY] = std::make_pair("", "opacity");
	m_ids_to_names[SUBSTANCE_OPACITY_SHADER] =
		std::make_pair("outColor[0]", "opacity");
	m_ids_to_names[EMISSIVE_COLOR] = std::make_pair("", "emissive");
	m_ids_to_names[EMISSIVE_COLOR_SHADER] =
		std::make_pair("outColor", "emissive");
	m_ids_to_names[EMISSIVE_INTENSITY] = std::make_pair("", "emissive_intensity");
	m_ids_to_names[BUMP_TYPE] = std::make_pair("", "disp_normal_bump_type");
	m_ids_to_names[BUMP_VALUE] =
		std::make_pair("outColor", "disp_normal_bump_value");
	m_ids_to_names[BUMP_INTENSITY] =
		std::make_pair("", "disp_normal_bump_intensity");
	m_ids_to_names[DL_AOV_GROUP] = std::make_pair("outColor", "aovGroup");
}
