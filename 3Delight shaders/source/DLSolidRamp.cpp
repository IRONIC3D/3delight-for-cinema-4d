#include "IDs.h"
#include "TextureUI_Functions.h"
#include "c4d.h"
#include "c4d_symbols.h"
#include "dl_solid_ramp.h"

static void
initGradient(BaseContainer& bc, Int32 id)
{
	GeData data(CUSTOMDATATYPE_GRADIENT, DEFAULTVALUE);
	Gradient* gradient =
		(Gradient*) data.GetCustomDataType(CUSTOMDATATYPE_GRADIENT);

	if (gradient) {
		GradientKnot k1, k2;

		if (id == COLOR_SOLID_RAMP_GRADIENT) {
			k1.col = HSVToRGB(Vector(0, 1, 0.0));
			k1.pos = 0.0;
			k2.col = HSVToRGB(Vector(0, 0, 1.0));
			k2.pos = 1.0;
			gradient->InsertKnot(k1);
			gradient->InsertKnot(k2);
		}
	}

	bc.SetData(id, data);
}

static void
InitRamp(BaseContainer& bc, Int32 id)
{
	GeData data(CUSTOMDATATYPE_SPLINE, DEFAULTVALUE);
	SplineData* spline =
		(SplineData*) data.GetCustomDataType(CUSTOMDATATYPE_SPLINE);

	if (spline) {
		spline->MakeLinearSplineBezier(2); // Make Linear Spline
		Int32 count = spline->GetKnotCount();

		if (count == 2) {
			CustomSplineKnot* knot0 = spline->GetKnot(0);

			if (knot0) {
				knot0->vPos = Vector(0.9, 1, 0);
			}

			CustomSplineKnot* knot1 = spline->GetKnot(1);

			if (knot1) {
				knot1->vPos = Vector(1, 0, 0);
			}
		}
	}

	bc.SetData(id, data);
}

class DLSolidRamp : public ShaderData
{
public:
	virtual Bool Init(GeListNode* node);
	virtual Bool GetDDescription(GeListNode* node,
								 Description* description,
								 DESCFLAGS_DESC& flags);
	virtual Bool GetDEnabling(GeListNode* node,
							  const DescID& id,
							  const GeData& t_data,
							  DESCFLAGS_ENABLE flags,
							  const BaseContainer* itemdesc);
	virtual Bool Message(GeListNode* node, Int32 type, void* data);
	virtual Vector Output(BaseShader* sh, ChannelData* sd);
	static NodeData* Alloc(void)
	{
		return NewObjClear(DLSolidRamp);
	}
};

Bool DLSolidRamp::Init(GeListNode* node)
{
	BaseContainer* data = ((BaseShader*) node)->GetDataInstance();
	data->SetInt32(SOLID_RAMP_SHAPE, SOLID_RAMP_SHAPE_SPHERE);
	data->SetVector(SOLID_RAMP_BACKGROUND_COLOR, HSVToRGB(Vector(0, 0, 0)));
	data->SetInt32(SOLID_RAMP_REVERSE, SOLID_RAMP_REVERSE_NONE);
	data->SetInt32(SOLID_RAMP_OUTSIDE, SOLID_RAMP_OUTSIDE_ONLY_BACK);
	data->SetInt32(SOLID_RAMP_SPACE, SOLID_RAMP_SPACE_WORLD);
	data->SetFloat(SOLID_RAMP_DISTORTION_INTENSITY, 0.0);
	data->SetFloat(SOLID_RAMP_NOISE_INTENSITY, 0);
	data->SetFloat(SOLID_RAMP_NOISE_SCALE, 1.0);
	data->SetFloat(SOLID_RAMP_NOISE_STEPPING, 0.0);
	data->SetFloat(SOLID_RAMP_NOISE_TIME, 0.0);
	data->SetFloat(SOLID_RAMP_NOISE_HUE, 0.5);
	data->SetFloat(SOLID_RAMP_NOISE_SATURATION, 0.5);
	data->SetFloat(SOLID_RAMP_NOISE_VALUE, 0.5);
	data->SetFloat(SOLID_RAMP_NOISE_OPACITY, 0.5);
	data->SetFloat(SOLID_RAMP_NOISE_LAYERS, 1);
	data->SetFloat(SOLID_RAMP_NOISE_LAYERS_PERSISTENCE, 0.7);
	data->SetFloat(SOLID_RAMP_NOISE_LAYERS_SCALE, 0.45);
	data->SetBool(SOLID_RAMP_ADJUST_INVERT, false);
	initGradient(*data, COLOR_SOLID_RAMP_GRADIENT);
	InitRamp(*data, OPACITY_SOLID_RAMP_GRADIENT);
	return true;
}

Bool DLSolidRamp::GetDDescription(GeListNode* node,
								  Description* description,
								  DESCFLAGS_DESC& flags)
{
	description->LoadDescription(DL_SOLID_RAMP);
	flags |= DESCFLAGS_DESC::LOADED;
	BaseContainer* dldata = ((BaseObject*) node)->GetDataInstance();
	// SOLID RAMP TRANSFORM GROUP
	HideAndShowTextures(SOLID_RAMP_BACKGROUND_COLOR_GROUP_PARAM,
						SOLID_RAMP_BACKGROUND_COLOR,
						SOLID_RAMP_BACKGROUND_COLOR_SHADER,
						SOLID_RAMP_BACKGROUND_COLOR_SHADER_TEMP,
						node,
						description,
						dldata);
	// DISTORTION GROUP
	HideAndShowTextures(SOLID_RAMP_DISTORTION_INTENSITY_GROUP_PARAM,
						SOLID_RAMP_DISTORTION_INTENSITY,
						SOLID_RAMP_DISTORTION_INTENSITY_SHADER,
						SOLID_RAMP_DISTORTION_INTENSITY_SHADER_TEMP,
						node,
						description,
						dldata);
	// NOISE GROUP
	HideAndShowTextures(SOLID_RAMP_NOISE_INTENSITY_GROUP_PARAM,
						SOLID_RAMP_NOISE_INTENSITY,
						SOLID_RAMP_NOISE_INTENSITY_SHADER,
						SOLID_RAMP_NOISE_INTENSITY_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(SOLID_RAMP_NOISE_SCALE_GROUP_PARAM,
						SOLID_RAMP_NOISE_SCALE,
						SOLID_RAMP_NOISE_SCALE_SHADER,
						SOLID_RAMP_NOISE_SCALE_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(SOLID_RAMP_NOISE_STEPPING_GROUP_PARAM,
						SOLID_RAMP_NOISE_STEPPING,
						SOLID_RAMP_NOISE_STEPPING_SHADER,
						SOLID_RAMP_NOISE_STEPPING_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(SOLID_RAMP_NOISE_TIME_GROUP_PARAM,
						SOLID_RAMP_NOISE_TIME,
						SOLID_RAMP_NOISE_TIME_SHADER,
						SOLID_RAMP_NOISE_TIME_SHADER_TEMP,
						node,
						description,
						dldata);
	// NOISE VARIATION GROUP
	HideAndShowTextures(SOLID_RAMP_NOISE_HUE_GROUP_PARAM,
						SOLID_RAMP_NOISE_HUE,
						SOLID_RAMP_NOISE_HUE_SHADER,
						SOLID_RAMP_NOISE_HUE_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(SOLID_RAMP_NOISE_SATURATION_GROUP_PARAM,
						SOLID_RAMP_NOISE_SATURATION,
						SOLID_RAMP_NOISE_SATURATION_SHADER,
						SOLID_RAMP_NOISE_SATURATION_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(SOLID_RAMP_NOISE_VALUE_GROUP_PARAM,
						SOLID_RAMP_NOISE_VALUE,
						SOLID_RAMP_NOISE_VALUE_SHADER,
						SOLID_RAMP_NOISE_VALUE_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(SOLID_RAMP_NOISE_OPACITY_GROUP_PARAM,
						SOLID_RAMP_NOISE_OPACITY,
						SOLID_RAMP_NOISE_OPACITY_SHADER,
						SOLID_RAMP_NOISE_OPACITY_SHADER_TEMP,
						node,
						description,
						dldata);
	// LAYERS GROUP
	HideAndShowTextures(SOLID_RAMP_NOISE_LAYERS_PERSISTENCE_GROUP_PARAM,
						SOLID_RAMP_NOISE_LAYERS_PERSISTENCE,
						SOLID_RAMP_NOISE_LAYERS_PERSISTENCE_SHADER,
						SOLID_RAMP_NOISE_LAYERS_PERSISTENCE_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(SOLID_RAMP_NOISE_LAYERS_SCALE_GROUP_PARAM,
						SOLID_RAMP_NOISE_LAYERS_SCALE,
						SOLID_RAMP_NOISE_LAYERS_SCALE_SHADER,
						SOLID_RAMP_NOISE_LAYERS_SCALE_SHADER_TEMP,
						node,
						description,
						dldata);
	return TRUE;
}

Bool DLSolidRamp::GetDEnabling(GeListNode* node,
							   const DescID& id,
							   const GeData& t_data,
							   DESCFLAGS_ENABLE flags,
							   const BaseContainer* itemdesc)
{
	if (!node) {
		return false;
	}

	BaseDocument* doc = GetActiveDocument();
	BaseContainer* dldata = ((BaseObject*) node)->GetDataInstance();

	switch (id[0].id) {
	case SOLID_RAMP_NOISE_HUE:
	case SOLID_RAMP_NOISE_HUE_SHADER:
	case POPUP_SOLID_RAMP_NOISE_HUE:
	case SOLID_RAMP_NOISE_SATURATION:
	case SOLID_RAMP_NOISE_SATURATION_SHADER:
	case POPUP_SOLID_RAMP_NOISE_SATURATION:
	case SOLID_RAMP_NOISE_VALUE:
	case SOLID_RAMP_NOISE_VALUE_SHADER:
	case POPUP_SOLID_RAMP_NOISE_VALUE:
	case SOLID_RAMP_NOISE_OPACITY:
	case SOLID_RAMP_NOISE_OPACITY_SHADER:
	case POPUP_SOLID_RAMP_NOISE_OPACITY:
	case SOLID_RAMP_NOISE_SCALE:
	case SOLID_RAMP_NOISE_SCALE_SHADER:
	case POPUP_SOLID_RAMP_NOISE_SCALE:
	case SOLID_RAMP_NOISE_STEPPING:
	case SOLID_RAMP_NOISE_STEPPING_SHADER:
	case POPUP_SOLID_RAMP_NOISE_STEPPING:
	case SOLID_RAMP_NOISE_TIME:
	case SOLID_RAMP_NOISE_TIME_SHADER:
	case POPUP_SOLID_RAMP_NOISE_TIME:
	case SOLID_RAMP_NOISE_LAYERS:
		return (dldata->GetFloat(SOLID_RAMP_NOISE_INTENSITY) > 0 || dldata->GetLink(SOLID_RAMP_NOISE_INTENSITY_SHADER, doc) != nullptr);
		break;

	case SOLID_RAMP_NOISE_LAYERS_PERSISTENCE:
	case SOLID_RAMP_NOISE_LAYERS_PERSISTENCE_SHADER:
	case POPUP_SOLID_RAMP_NOISE_LAYERS_PERSISTENCE:
	case SOLID_RAMP_NOISE_LAYERS_SCALE:
	case SOLID_RAMP_NOISE_LAYERS_SCALE_SHADER:
	case POPUP_SOLID_RAMP_NOISE_LAYERS_SCALE:
		return (
				   dldata->GetFloat(SOLID_RAMP_NOISE_LAYERS) > 1 && (dldata->GetFloat(SOLID_RAMP_NOISE_INTENSITY) > 0 || dldata->GetLink(SOLID_RAMP_NOISE_INTENSITY_SHADER, doc) != nullptr));
		break;

	default:
		break;
	}

	return true;
}

Bool DLSolidRamp::Message(GeListNode* node, Int32 type, void* data)
{
	if (!node) {
		return false;
	}

	BaseContainer* dldata = ((BaseMaterial*) node)->GetDataInstance();

	if (type == MSG_DESCRIPTION_POPUP) {
		DescriptionPopup* dp = (DescriptionPopup*) data;
		int clicked_button_id = dp->_descId[0].id;

		switch (clicked_button_id) {
		case POPUP_SOLID_RAMP_BACKGROUND_COLOR:
			FillPopupMenu(dldata, dp, SOLID_RAMP_BACKGROUND_COLOR_GROUP_PARAM);
			break;

		case POPUP_SOLID_RAMP_DISTORTION_INTENSITY:
			FillPopupMenu(dldata, dp, SOLID_RAMP_DISTORTION_INTENSITY_GROUP_PARAM);
			break;

		case POPUP_SOLID_RAMP_NOISE_INTENSITY:
			FillPopupMenu(dldata, dp, SOLID_RAMP_NOISE_INTENSITY_GROUP_PARAM);
			break;

		case POPUP_SOLID_RAMP_NOISE_SCALE:
			FillPopupMenu(dldata, dp, SOLID_RAMP_NOISE_SCALE_GROUP_PARAM);
			break;

		case POPUP_SOLID_RAMP_NOISE_STEPPING:
			FillPopupMenu(dldata, dp, SOLID_RAMP_NOISE_STEPPING_GROUP_PARAM);
			break;

		case POPUP_SOLID_RAMP_NOISE_TIME:
			FillPopupMenu(dldata, dp, SOLID_RAMP_NOISE_TIME_GROUP_PARAM);
			break;

		case POPUP_SOLID_RAMP_NOISE_HUE:
			FillPopupMenu(dldata, dp, SOLID_RAMP_NOISE_HUE_GROUP_PARAM);
			break;

		case POPUP_SOLID_RAMP_NOISE_SATURATION:
			FillPopupMenu(dldata, dp, SOLID_RAMP_NOISE_SATURATION_GROUP_PARAM);
			break;

		case POPUP_SOLID_RAMP_NOISE_VALUE:
			FillPopupMenu(dldata, dp, SOLID_RAMP_NOISE_VALUE_GROUP_PARAM);
			break;

		case POPUP_SOLID_RAMP_NOISE_OPACITY:
			FillPopupMenu(dldata, dp, SOLID_RAMP_NOISE_OPACITY_GROUP_PARAM);
			break;

		case POPUP_SOLID_RAMP_NOISE_LAYERS_PERSISTENCE:
			FillPopupMenu(
				dldata, dp, SOLID_RAMP_NOISE_LAYERS_PERSISTENCE_GROUP_PARAM);
			break;

		case POPUP_SOLID_RAMP_NOISE_LAYERS_SCALE:
			FillPopupMenu(dldata, dp, SOLID_RAMP_NOISE_LAYERS_SCALE_GROUP_PARAM);
			break;

		default:
			break;
		}
	}

	return true;
}

Vector
DLSolidRamp::Output(BaseShader* sh, ChannelData* sd)
{
	return Vector(0, 0, 0);
}

Bool RegisterSolidRampTexture(void)
{
	return RegisterShaderPlugin(
			   DL_SOLID_RAMP, "Solid Ramp"_s, 0, DLSolidRamp::Alloc, "dl_solid_ramp"_s, 0);
}
