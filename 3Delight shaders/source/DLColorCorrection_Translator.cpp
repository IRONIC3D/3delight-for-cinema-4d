#include "DLColorCorrection_Translator.h"
#include "3DelightEnvironment.h"
#include "dl_color_correction.h"

Delight_ColorCorrection::Delight_ColorCorrection()
{
	is_3delight_shader = true;
	const char* delightpath = DelightEnv::getDelightEnvironment();
	m_shaderpath =
		delightpath + (std::string) "/osl" + (std::string) "/dlColorCorrection.oso";

	if (m_shaderpath.c_str() && m_shaderpath[0]) {
		m_ids_to_names[Shader_Path] = std::make_pair("", &m_shaderpath[0]);
	}

	m_ids_to_names[INPUT_COLOR] = std::make_pair("", "input");
	m_ids_to_names[INPUT_COLOR_SHADER] = std::make_pair("outColor", "input");
	m_ids_to_names[INPUT_MASK] = std::make_pair("", "mask");
	m_ids_to_names[INPUT_MASK_SHADER] = std::make_pair("outColor[0]", "mask");
	m_ids_to_names[CORRECTION_GAMMA] = std::make_pair("", "gamma");
	m_ids_to_names[CORRECTION_GAMMA_SHADER] =
		std::make_pair("outColor[0]", "gamma");
	m_ids_to_names[CORRECTION_HUE_SHIFT] = std::make_pair("", "hueShift");
	m_ids_to_names[CORRECTION_HUE_SHIFT_SHADER] =
		std::make_pair("outColor[0]", "hueShift");
	m_ids_to_names[CORRECTION_SATURATION] = std::make_pair("", "saturation");
	m_ids_to_names[CORRECTION_SATURATION_SHADER] =
		std::make_pair("outColor[0]", "saturation");
	m_ids_to_names[CORRECTION_VIBRANCE] = std::make_pair("", "vibrance");
	m_ids_to_names[CORRECTION_VIBRANCE_SHADER] =
		std::make_pair("outColor[0]", "vibrance");
	m_ids_to_names[CORRECTION_CONSTRAST] = std::make_pair("", "contrast");
	m_ids_to_names[CORRECTION_CONSTRAST_SHADER] =
		std::make_pair("outColor[0]", "contrast");
	m_ids_to_names[CORRECTION_CONSTRAST_PIVOT] =
		std::make_pair("", "contrastPivot");
	m_ids_to_names[CORRECTION_CONSTRAST_PIVOT_SHADER] =
		std::make_pair("outColor[0]", "contrastPivot");
	m_ids_to_names[CORRECTION_EXPOSURE] = std::make_pair("", "exposure");
	m_ids_to_names[CORRECTION_EXPOSURE_SHADER] =
		std::make_pair("outColor[0]", "exposure");
	m_ids_to_names[CORRECTION_GAIN] = std::make_pair("", "gain");
	m_ids_to_names[CORRECTION_GAIN_SHADER] = std::make_pair("outColor", "gain");
	m_ids_to_names[CORRECTION_OFFSET] = std::make_pair("", "offset");
	m_ids_to_names[CORRECTION_OFFSET_SHADER] =
		std::make_pair("outColor", "offset");
	m_ids_to_names[CORRECTION_INVERT] = std::make_pair("", "invert");
}
