#include "DLColorVariation_Translator.h"
#include "3DelightEnvironment.h"
#include "dl_color_variation.h"

Delight_ColorVariation::Delight_ColorVariation()
{
	is_3delight_shader = true;
	const char* delightpath = DelightEnv::getDelightEnvironment();
	m_shaderpath =
		delightpath + (std::string) "/osl" + (std::string) "/dlColorVariation.oso";

	if (m_shaderpath.c_str() && m_shaderpath[0]) {
		m_ids_to_names[Shader_Path] = std::make_pair("", &m_shaderpath[0]);
	}

	m_ids_to_names[INPUT_COLOR] = std::make_pair("", "_color");
	m_ids_to_names[INPUT_COLOR_SHADER] = std::make_pair("outColor", "_color");
	m_ids_to_names[VARIATION_HUE] = std::make_pair("", "hueVariation");
	m_ids_to_names[VARIATION_HUE_SHADER] =
		std::make_pair("outColor[0]", "hueVariation");
	m_ids_to_names[VARIATION_SATURATION] =
		std::make_pair("", "saturationVariation");
	m_ids_to_names[VARIATION_SATURATION_SHADER] =
		std::make_pair("outColor[0]", "saturationVariation");
	m_ids_to_names[VARIATION_BRIGHTNESS] =
		std::make_pair("", "brightnessVariation");
	m_ids_to_names[VARIATION_BRIGHTNESS_SHADER] =
		std::make_pair("outColor[0]", "brightnessVariation");
	m_ids_to_names[RANDOMNESS_TYPE] = std::make_pair("", "randomness");
	m_ids_to_names[SOURCE_TYPE] = std::make_pair("outColor[0]", "randomSource");
	m_ids_to_names[SEED] = std::make_pair("", "seed");
}
