#pragma once

#include "NSIExportShader.h"

class Delight_Triplanar : public NSI_Export_Shader
{
public:
	virtual void CreateNSINodes(const char* Handle,
								const char* ParentTransformHandle,
								BaseList2D* C4DNode,
								BaseDocument* doc,
								DL_SceneParser* parser);
	virtual void SampleAttributes(DL_SampleInfo* info,
								  const char* Handle,
								  BaseList2D* C4DNode,
								  BaseDocument* doc,
								  DL_SceneParser* parser);
	Delight_Triplanar();

private:
	std::string m_handle;
	std::string m_attributes_handle;
	std::string m_transform_handle;
	std::string m_shader_handle;
};
