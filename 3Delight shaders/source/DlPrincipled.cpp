#include <iostream>
#include <string>
#include "DLCustomAOVS.h"
#include "DL_Matpreview.h"
#include "IDs.h"
#include "TextureUI_Functions.h"
#include "c4d.h"
#include "dl_principled.h"

/*
        Dl_Principled class is inhertied from MaterialData which
        is a class for creating a material plugin in this case 3Delight
        Principled material.

        Init() function is used to initialize the material's UI
        In this function we define the default valuse for each
        attribute that is part of the currently registered material

        GetDEnabling() function is used to unable or disable current
        attributes that we want based on different criteria again decided by us

        RegisterMaterialPlugin() function is used to register a material plugin.
*/

class DL_Principled : public MaterialData
{
	INSTANCEOF(DL_Principled, MaterialData)

private:
	Vector color;

public:
	virtual Bool Init(GeListNode* node);
	virtual INITRENDERRESULT InitRender(BaseMaterial* mat,
										const InitRenderStruct& irs);
	virtual Bool GetDDescription(GeListNode* node,
								 Description* description,
								 DESCFLAGS_DESC& flags);
	virtual Bool GetDEnabling(GeListNode* node,
							  const DescID& id,
							  const GeData& t_data,
							  DESCFLAGS_ENABLE flags,
							  const BaseContainer* itemdesc);
	virtual Bool Message(GeListNode* node, Int32 type, void* data);
	virtual void CalcSurface(BaseMaterial* mat, VolumeData* vd);
	static NodeData* Alloc()
	{
		return NewObjClear(DL_Principled);
	}
	DLCustomAOVS dlcustom;
	DLCustomAOVS* selectedDLCustom = nullptr;
	void CreateDynamicDescriptions(Description* const description);
};

Bool DL_Principled::Init(GeListNode* node)
{
	BaseContainer* data = ((BaseMaterial*) node)->GetDataInstance();
	// Set default matpreview scene
	DL_SetDefaultMatpreview((BaseMaterial*) node);
	data->SetFloat(COATING_LAYER_THICKNESS, 0);
	Vector coating_color(1, 0.5, 0.1);
	data->SetVector(COATING_LAYER_COLOR, coating_color);
	data->SetFloat(COATING_LAYER_ROUGHNESS, 0);
	data->SetFloat(COATING_LAYER_SPECULAR_LEVEL, 0.5);
	Vector base_color(0.8, 0.8, 0.8);
	data->SetVector(BASE_LAYER_COLOR, base_color);
	data->SetFloat(BASE_LAYER_ROUGHNESS, 0.3);
	data->SetFloat(BASE_LAYER_SPECULAR_LEVEL, 0.5);
	data->SetFloat(BASE_LAYER_METALLIC, 0);
	data->SetFloat(BASE_LAYER_ANISOTROPY, 0);
	data->SetFloat(BASE_LAYER_ANISOTROPY, 0);
	Vector anisotropy_direction(0.5, 1, 0);
	data->SetVector(BASE_LAYER_ANISOTROPY_DIRECTION, anisotropy_direction);
	data->SetFloat(BASE_LAYER_OPACITY, 1);
	Vector subsurface_color(0.729, 0.874, 1);
	data->SetVector(SUBSURFACE_COLOR, subsurface_color);
	data->SetFloat(SUBSURFACE_SCALE, 0.1);
	data->SetFloat(SUBSURFACE_WEIGHT, 0.0);
	data->SetFloat(SUBSURFACE_ANISOTROPY, 0.0);
	data->SetFloat(REFRACTION_WEIGHT, 0.0);
	data->SetVector(REFRACTION_COLOR, HSVToRGB(Vector(0, 0, 1.0)));
	data->SetVector(REFRACTION_SCATTER, HSVToRGB(Vector(0, 0, 0)));
	data->SetFloat(REFRACTION_IOR, 1.3);
	data->SetFloat(REFRACTION_ROUGHNESS, 0.0);
	data->SetFloat(REFRACTION_DENSITY, 1.0);
	data->SetFloat(INCANDESCENCE_INTENSITY, 1);
	data->SetInt32(BUMP_TYPE, BUMP_MAP);
	data->SetFloat(BUMP_INTENSITY, 1);
	data->SetInt32(BUMP_LAYERS_AFFECTED, AFFECT_BOTH_LAYERS);
	data->SetInt32(THICKNESS_GROUP_PARAM, 2);
	data->SetInt32(COLOR_GROUP_PARAM, 2);
	data->SetInt32(ROUGHNESS_GROUP_PARAM, 2);
	data->SetInt32(SPECULAR_GROUP_PARAM, 2);
	data->SetInt32(BASE_COLOR_GROUP_PARAM, 2);
	data->SetInt32(BASE_ROUGHNESS_GROUP_PARAM, 2);
	// color = data->GetVector(SIMPLEMATERIAL_COLOR);
	return true;
}

Bool DL_Principled::GetDEnabling(GeListNode* node,
								 const DescID& id,
								 const GeData& t_data,
								 DESCFLAGS_ENABLE flags,
								 const BaseContainer* itemdesc)
{
	if (!node) {
		return false;
	}

	BaseDocument* doc = GetActiveDocument();
	BaseContainer* dldata = ((BaseObject*) node)->GetDataInstance();

	switch (id[0].id) {
	case SUBSURFACE_COLOR:
	case SUBSURFACE_COLOR_SHADER:
	case POPUP_SUBSURFACE_COLOR:
	case SUBSURFACE_SCALE:
	case SUBSURFACE_SCALE_SHADER:
	case SUBSURFACE_ANISOTROPY:
	case SUBSURFACE_ANISOTROPY_SHADER:
	case POPUP_SUBSURFACE_SCALE:
		return (dldata->GetFloat(SUBSURFACE_WEIGHT) > 0 || dldata->GetLink(SUBSURFACE_WEIGHT_SHADER, doc) != nullptr); // Enable All the above parameters if Use
		// Subsurface is checked
		break;

	case COATING_LAYER_COLOR:
	case COATING_LAYER_ROUGHNESS:
	case COATING_LAYER_SPECULAR_LEVEL:
	case COATING_LAYER_COLOR_SHADER:
	case COATING_LAYER_ROUGHNESS_SHADER:
	case COATING_LAYER_SPECULAR_LEVEL_SHADER:
	case POPUP_COLOR:
	case POPUP_ROUGHNESS:
	case POPUP_SPECULAR:
		return (
				   dldata->GetFloat(COATING_LAYER_THICKNESS) > 0 || (dldata->GetLink(COATING_LAYER_THICKNESS_SHADER, doc) != nullptr));
		break;

	// case AOV_GO:
	// case DL_CUSTOM_COLORS:
	// return (dldata->GetInt32(AOV_POPUP_VALUE) != 0);
	default:
		break;
	}

	return true;
}

Bool DL_Principled::GetDDescription(GeListNode* node,
									Description* description,
									DESCFLAGS_DESC& flags)
{
	description->LoadDescription(DL_PRINCIPLED);
	flags |= DESCFLAGS_DESC::LOADED;
	BaseContainer* dldata = ((BaseObject*)node)->GetDataInstance();
	//AddCycleButton(description, AOV_POPUP_VALUE, DescID(AOV_POPUP), dlcustom);
	HideAndShowTextures(THICKNESS_GROUP_PARAM, COATING_LAYER_THICKNESS, COATING_LAYER_THICKNESS_SHADER, COATING_LAYER_THICKNESS_SHADER_TEMP, node, description, dldata);
	HideAndShowTextures(COLOR_GROUP_PARAM, COATING_LAYER_COLOR, COATING_LAYER_COLOR_SHADER, COATING_LAYER_COLOR_SHADER_TEMP, node, description, dldata);
	HideAndShowTextures(ROUGHNESS_GROUP_PARAM, COATING_LAYER_ROUGHNESS, COATING_LAYER_ROUGHNESS_SHADER, COATING_LAYER_ROUGHNESS_SHADER_TEMP, node, description, dldata);
	HideAndShowTextures(SPECULAR_GROUP_PARAM, COATING_LAYER_SPECULAR_LEVEL, COATING_LAYER_SPECULAR_LEVEL_SHADER, COATING_LAYER_SPECULAR_LEVEL_SHADER_TEMP, node, description, dldata);
	HideAndShowTextures(BASE_COLOR_GROUP_PARAM, BASE_LAYER_COLOR, BASE_LAYER_COLOR_SHADER, BASE_LAYER_COLOR_SHADER_TEMP, node, description, dldata);
	HideAndShowTextures(BASE_ROUGHNESS_GROUP_PARAM, BASE_LAYER_ROUGHNESS, BASE_LAYER_ROUGHNESS_SHADER, BASE_LAYER_ROUGHNESS_SHADER_TEMP, node, description, dldata);
	HideAndShowTextures(BASE_SPECULAR_GROUP_PARAM, BASE_LAYER_SPECULAR_LEVEL, BASE_LAYER_SPECULAR_LEVEL_SHADER, BASE_LAYER_SPECULAR_LEVEL_SHADER_TEMP, node, description, dldata);
	HideAndShowTextures(BASE_METALLIC_GROUP_PARAM, BASE_LAYER_METALLIC, BASE_LAYER_METALLIC_SHADER, BASE_LAYER_METALLIC_SHADER_TEMP, node, description, dldata);
	HideAndShowTextures(BASE_ANISOTROPY_GROUP_PARAM, BASE_LAYER_ANISOTROPY, BASE_LAYER_ANISOTROPY_SHADER, BASE_LAYER_ANISOTROPY_SHADER_TEMP, node, description, dldata);
	HideAndShowTextures(BASE_ANISOTROPY_DIR_GROUP_PARAM, BASE_LAYER_ANISOTROPY_DIRECTION, BASE_LAYER_ANISOTROPY_DIRECTION_SHADER, BASE_LAYER_ANISOTROPY_DIRECTION_SHADER_TEMP, node, description, dldata);
	HideAndShowTextures(BASE_OPACITY_GROUP_PARAM, BASE_LAYER_OPACITY, BASE_LAYER_OPACITY_SHADER, BASE_LAYER_OPACITY_SHADER_TEMP, node, description, dldata);
	HideAndShowTextures(SUBSURFACE_COLOR_GROUP_PARAM, SUBSURFACE_COLOR, SUBSURFACE_COLOR_SHADER, SUBSURFACE_COLOR_SHADER_TEMP, node, description, dldata);
	HideAndShowTextures(SUBSURFACE_SCALE_GROUP_PARAM, SUBSURFACE_SCALE, SUBSURFACE_SCALE_SHADER, SUBSURFACE_SCALE_SHADER_TEMP, node, description, dldata);
	HideAndShowTextures(SUBSURFACE_WEIGHT_GROUP_PARAM, SUBSURFACE_WEIGHT, SUBSURFACE_WEIGHT_SHADER, SUBSURFACE_WEIGHT_SHADER_TEMP,node, description, dldata);
	HideAndShowTextures(SUBSURFACE_ANISOTROPY_GROUP_PARAM, SUBSURFACE_ANISOTROPY, SUBSURFACE_ANISOTROPY_SHADER, SUBSURFACE_ANISOTROPY_SHADER_TEMP,node, description, dldata);
	HideAndShowTextures(REFRACTION_WEIGHT_GROUP_PARAM, REFRACTION_WEIGHT, REFRACTION_WEIGHT_SHADER, REFRACTION_WEIGHT_SHADER_TEMP,node, description, dldata);
	HideAndShowTextures(REFRACTION_COLOR_GROUP_PARAM, REFRACTION_COLOR, REFRACTION_COLOR_SHADER, REFRACTION_COLOR_SHADER_TEMP,node, description, dldata);
	HideAndShowTextures(REFRACTION_SCATTER_GROUP_PARAM, REFRACTION_SCATTER, REFRACTION_SCATTER_SHADER, REFRACTION_SCATTER_SHADER_TEMP,node, description, dldata);
	HideAndShowTextures(REFRACTION_IOR_GROUP_PARAM, REFRACTION_IOR, REFRACTION_IOR_SHADER, REFRACTION_IOR_SHADER_TEMP,node, description, dldata);
	HideAndShowTextures(REFRACTION_ROUGHNESS_GROUP_PARAM, REFRACTION_ROUGHNESS, REFRACTION_ROUGHNESS_SHADER, REFRACTION_ROUGHNESS_SHADER_TEMP,node, description, dldata);
	HideAndShowTextures(REFRACTION_DENSITY_GROUP_PARAM, REFRACTION_DENSITY, REFRACTION_DENSITY_SHADER, REFRACTION_DENSITY_SHADER_TEMP,node, description, dldata);
	HideAndShowTextures(INCANDESCENCE_COLOR_GROUP_PARAM, INCANDESCENCE_COLOR, INCANDESCENCE_COLOR_SHADER, INCANDESCENCE_COLOR_SHADER_TEMP,node, description, dldata);
	return TRUE;
}

Bool DL_Principled::Message(GeListNode* node, Int32 type, void* data)
{
	if (!node) {
		return false;
	}

	BaseContainer* dldata = ((BaseMaterial*) node)->GetDataInstance();

	if (type == MSG_DESCRIPTION_POPUP) {
		DescriptionPopup* dp = (DescriptionPopup*) data;
		int clicked_button_id = dp->_descId[0].id;

		switch (clicked_button_id) {
		case POPUP_COLOR:
			FillPopupMenu(dldata, dp, COLOR_GROUP_PARAM);
			break;

		case POPUP_THICKNESS:
			FillPopupMenu(dldata, dp, THICKNESS_GROUP_PARAM);
			break;

		case POPUP_ROUGHNESS:
			FillPopupMenu(dldata, dp, ROUGHNESS_GROUP_PARAM);
			break;

		case POPUP_SPECULAR:
			FillPopupMenu(dldata, dp, SPECULAR_GROUP_PARAM);
			break;

		case POPUP_BASECOLOR:
			FillPopupMenu(dldata, dp, BASE_COLOR_GROUP_PARAM);
			break;

		case POPUP_BASEROUGHNESS:
			FillPopupMenu(dldata, dp, BASE_ROUGHNESS_GROUP_PARAM);
			break;

		case POPUP_BASESPECULAR:
			FillPopupMenu(dldata, dp, BASE_SPECULAR_GROUP_PARAM);
			break;

		case POPUP_BASEMETALLIC:
			FillPopupMenu(dldata, dp, BASE_METALLIC_GROUP_PARAM);
			break;

		case POPUP_BASEANISOTROPY:
			FillPopupMenu(dldata, dp, BASE_ANISOTROPY_GROUP_PARAM);
			break;

		case POPUP_BASEANISOTROPY_DIRECTION:
			FillPopupMenu(dldata, dp, BASE_ANISOTROPY_DIR_GROUP_PARAM);
			break;

		case POPUP_BASEOPACITY:
			FillPopupMenu(dldata, dp, BASE_OPACITY_GROUP_PARAM);
			break;

		case POPUP_SUBSURFACE_COLOR:
			FillPopupMenu(dldata, dp, SUBSURFACE_COLOR_GROUP_PARAM);
			break;

		case POPUP_SUBSURFACE_SCALE:
			FillPopupMenu(dldata, dp, SUBSURFACE_SCALE_GROUP_PARAM);
			break;

		case POPUP_SUBSURFACE_WEIGHT:
			FillPopupMenu(dldata, dp, SUBSURFACE_WEIGHT_GROUP_PARAM);
			break;

		case POPUP_SUBSURFACE_ANISOTROPY:
			FillPopupMenu(dldata, dp, SUBSURFACE_ANISOTROPY_GROUP_PARAM);
			break;

		case POPUP_REFRACTION_WEIGHT:
			FillPopupMenu(dldata, dp, REFRACTION_WEIGHT_GROUP_PARAM);
			break;

		case POPUP_REFRACTION_COLOR:
			FillPopupMenu(dldata, dp, REFRACTION_COLOR_GROUP_PARAM);
			break;

		case POPUP_REFRACTION_SCATTER:
			FillPopupMenu(dldata, dp, REFRACTION_SCATTER_GROUP_PARAM);
			break;

		case POPUP_REFRACTION_IOR:
			FillPopupMenu(dldata, dp, REFRACTION_IOR_GROUP_PARAM);
			break;

		case POPUP_REFRACTION_ROUGHNESS:
			FillPopupMenu(dldata, dp, REFRACTION_ROUGHNESS_GROUP_PARAM);
			break;

		case POPUP_REFRACTION_DENSITY:
			FillPopupMenu(dldata, dp, REFRACTION_DENSITY_GROUP_PARAM);
			break;

		case POPUP_INCANDESCENCE_COLOR:
			FillPopupMenu(dldata, dp, INCANDESCENCE_COLOR_GROUP_PARAM);
			break;

		default:
			break;
		}
	}

	return true;
}

// Initializes resources for rendering.
INITRENDERRESULT
DL_Principled::InitRender(BaseMaterial* mat, const InitRenderStruct& irs)
{
	return INITRENDERRESULT::OK;
}

void DL_Principled::CalcSurface(BaseMaterial* mat, VolumeData* vd)
{
	Vector diff, spec;
	vd->IlluminanceSimple(&diff, &spec, 0, 0, 0);
	vd->col = 0.8 * diff;
}

Bool RegisterDLPrincipled(void)
{
	return RegisterMaterialPlugin(DL_PRINCIPLED,
								  "Principled"_s,
								  PLUGINFLAG_HIDE,
								  DL_Principled::Alloc,
								  "Dl_principled"_s,
								  0);
}
