#include <DL_Utilities.h>
#include "IDs.h"
#include "c4d.h"

class DL_Metal_command : public CommandData
{
public:
	int shelf_used = 0;
	virtual Bool Execute(BaseDocument* doc, GeDialog* parentManager);
};

Bool DL_Metal_command::Execute(BaseDocument* doc, GeDialog* parentManager)
{
	CreateMaterial(shelf_used, DL_METAL, doc);
	return true;
}

Bool Register_Metal_Object(void)
{
	DL_Metal_command* new_metal = NewObjClear(DL_Metal_command);
	RegisterCommandPlugin(DL_METAL_COMMAND,
						  "Metal"_s,
						  PLUGINFLAG_HIDEPLUGINMENU,
						  AutoBitmap("shelf_dlMetal_200.png"_s),
						  String("Assign new Metal"),
						  NewObjClear(DL_Metal_command));

	if (RegisterCommandPlugin(DL_METAL_SHELF_COMMAND,
							  "Metal"_s,
							  PLUGINFLAG_HIDEPLUGINMENU,
							  AutoBitmap("shelf_dlMetal_200.png"_s),
							  String("Assign new Metal Material"),
							  new_metal)) {
		new_metal->shelf_used = 1;
	}

	return true;
}
