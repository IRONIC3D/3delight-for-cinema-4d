#include <DL_Utilities.h>
#include "IDs.h"
#include "c4d.h"

class DLHairAndFur_command : public CommandData
{
public:
	int shelf_used = 0;
	virtual Bool Execute(BaseDocument* doc, GeDialog* parentManager);
};

Bool DLHairAndFur_command::Execute(BaseDocument* doc, GeDialog* parentManager)
{
	CreateMaterial(shelf_used, DL_HAIRANDFUR, doc);
	return true;
}

Bool Register_HairAndFur_Object(void)
{
	DLHairAndFur_command* new_hair_and_fur = NewObjClear(DLHairAndFur_command);
	RegisterCommandPlugin(DL_HAIRANDFUR_COMMAND,
						  "Hair and Fur"_s,
						  PLUGINFLAG_HIDEPLUGINMENU,
						  AutoBitmap("shelf_dlHairAndFur_200.png"_s),
						  String("Assign new Hair and Fur"_s),
						  NewObjClear(DLHairAndFur_command));

	if (RegisterCommandPlugin(DL_HAIRANDFUR_SHELF_COMMAND,
							  "Hair and Fur"_s,
							  PLUGINFLAG_HIDEPLUGINMENU,
							  AutoBitmap("shelf_dlHairAndFur_200.png"_s),
							  String("Assign new Hair and Fur Material"_s),
							  new_hair_and_fur)) {
		new_hair_and_fur->shelf_used = 1;
	}

	return true;
}
