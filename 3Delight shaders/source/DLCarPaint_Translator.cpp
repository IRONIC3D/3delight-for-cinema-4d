#include "DLCarPaint_Translator.h"
#include "3DelightEnvironment.h"
#include "NSIExportMaterial.h"
#include "dl_car_paint.h"

Delight_CarPaint::Delight_CarPaint()
{
	is_3delight_material = true;
	const char* delightpath = DelightEnv::getDelightEnvironment();
	c_shaderpath =
		delightpath + (std::string) "/osl" + (std::string) "/dlCarPaint.oso";

	if (c_shaderpath.c_str() && c_shaderpath[0]) {
		m_ids_to_names[Shader_Path] = std::make_pair("", &c_shaderpath[0]);
	}

	m_ids_to_names[COATING_LAYER_THICKNESS] =
		std::make_pair("", "coating_thickness");
	m_ids_to_names[COATING_LAYER_THICKNESS_SHADER] =
		std::make_pair("outColor[0]", "coating_thickness");
	m_ids_to_names[COATING_LAYER_COLOR] = std::make_pair("", "coating_color");
	m_ids_to_names[COATING_LAYER_COLOR_SHADER] =
		std::make_pair("outColor", "coating_color");
	m_ids_to_names[COATING_LAYER_ROUGHNESS] =
		std::make_pair("", "coating_roughness");
	m_ids_to_names[COATING_LAYER_ROUGHNESS_SHADER] =
		std::make_pair("outColor[0]", "coating_roughness");
	m_ids_to_names[COATING_LAYER_SPECULAR_LEVEL] =
		std::make_pair("", "coating_specular_level");
	m_ids_to_names[COATING_LAYER_SPECULAR_LEVEL_SHADER] =
		std::make_pair("outColor[0]", "coating_specular_level");
	m_ids_to_names[FLAKES_DENSITY] = std::make_pair("", "flake_density");
	m_ids_to_names[FLAKES_DENSITY_SHADER] =
		std::make_pair("outColor[0]", "flake_density");
	m_ids_to_names[FLAKES_COLOR] = std::make_pair("", "flake_color");
	m_ids_to_names[FLAKES_COLOR_SHADER] =
		std::make_pair("outColor", "flake_color");
	m_ids_to_names[FLAKES_ROUGHNESS] = std::make_pair("", "flake_roughness");
	m_ids_to_names[FLAKES_ROUGHNESS_SHADER] =
		std::make_pair("outColor[0]", "flake_roughness");
	m_ids_to_names[FLAKES_SCALE] = std::make_pair("", "flake_scale");
	m_ids_to_names[FLAKES_SCALE_SHADER] =
		std::make_pair("outColor[0]", "flake_scale");
	m_ids_to_names[FLAKES_RANDOMNESS] = std::make_pair("", "flake_randomness");
	m_ids_to_names[FLAKES_RANDOMNESS_SHADER] =
		std::make_pair("outColor[0]", "flake_randomness");
	m_ids_to_names[BASE_LAYER_COLOR] = std::make_pair("", "i_color");
	m_ids_to_names[BASE_LAYER_COLOR_SHADER] =
		std::make_pair("outColor", "i_color");
	m_ids_to_names[BASE_LAYER_ROUGHNESS] = std::make_pair("", "roughness");
	m_ids_to_names[BASE_LAYER_ROUGHNESS_SHADER] =
		std::make_pair("outColor", "roughness");
	m_ids_to_names[BASE_LAYER_SPECULAR_LEVEL] =
		std::make_pair("", "specular_level");
	m_ids_to_names[BASE_LAYER_SPECULAR_LEVEL_SHADER] =
		std::make_pair("outColor[0]", "specular_level");
	m_ids_to_names[BASE_LAYER_METALLIC] = std::make_pair("", "metallic");
	m_ids_to_names[BASE_LAYER_METALLIC_SHADER] =
		std::make_pair("outColor[0]", "metallic");
	m_ids_to_names[BUMP_TYPE] = std::make_pair("", "disp_normal_bump_type");
	m_ids_to_names[BUMP_VALUE] =
		std::make_pair("outColor", "disp_normal_bump_value");
	m_ids_to_names[BUMP_INTENSITY] =
		std::make_pair("", "disp_normal_bump_intensity");
	m_ids_to_names[BUMP_LAYERS_AFFECTED] =
		std::make_pair("", "normal_bump_affect_layer");
	m_ids_to_names[DL_AOV_GROUP] = std::make_pair("outColor", "aovGroup");
}
