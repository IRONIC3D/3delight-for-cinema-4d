#include "DL_Matpreview.h"
#include "IDs.h"
#include "TextureUI_Functions.h"
#include "c4d.h"
#include "c4d_symbols.h"
#include "dl_layered.h"
class DL_Layered : public MaterialData
{
	INSTANCEOF(DL_Layered, MaterialData)

private:
	Vector color;

public:
	virtual Bool Init(GeListNode* node);
	virtual INITRENDERRESULT InitRender(BaseMaterial* mat,
										const InitRenderStruct& irs);
	virtual Bool GetDDescription(GeListNode* node,
								 Description* description,
								 DESCFLAGS_DESC& flags);
	virtual Bool Message(GeListNode* node, Int32 type, void* data);
	virtual void CalcSurface(BaseMaterial* mat, VolumeData* vd);
	static NodeData* Alloc(void)
	{
		return NewObjClear(DL_Layered);
	}
};

Bool DL_Layered::Init(GeListNode* node)
{
	// Set default matpreview scene
	DL_SetDefaultMatpreview((BaseMaterial*) node);
	BaseContainer* data = ((BaseMaterial*) node)->GetDataInstance();
	data->SetFloat(TOP_LAYER_MASK, 1.0);
	data->SetFloat(BOTTOM_LAYER_MASK, 1.0);
	data->SetFloat(MIDDLE_LAYER_MASK, 1.0);
	return true;
}

Bool DL_Layered::GetDDescription(GeListNode* node,
								 Description* description,
								 DESCFLAGS_DESC& flags)
{
	description->LoadDescription(DL_LAYERED);
	flags |= DESCFLAGS_DESC::LOADED;
	BaseContainer* dldata = ((BaseObject*) node)->GetDataInstance();
	HideAndShowTextures(TOP_LAYER_MASK_GROUP_PARAM,
						TOP_LAYER_MASK,
						TOP_LAYER_MASK_SHADER,
						TOP_LAYER_MASK_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(MIDDLE_LAYER_MASK_GROUP_PARAM,
						MIDDLE_LAYER_MASK,
						MIDDLE_LAYER_MASK_SHADER,
						MIDDLE_LAYER_MASK_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(BOTTOM_LAYER_MASK_GROUP_PARAM,
						BOTTOM_LAYER_MASK,
						BOTTOM_LAYER_MASK_SHADER,
						BOTTOM_LAYER_MASK_SHADER_TEMP,
						node,
						description,
						dldata);
	return TRUE;
}

Bool DL_Layered::Message(GeListNode* node, Int32 type, void* data)
{
	if (!node) {
		return false;
	}

	BaseContainer* dldata = ((BaseMaterial*) node)->GetDataInstance();

	if (type == MSG_DESCRIPTION_POPUP) {
		DescriptionPopup* dp = (DescriptionPopup*) data;
		int clicked_button_id = dp->_descId[0].id;

		switch (clicked_button_id) {
		case POPUP_TOP_LAYER_MASK:
			FillPopupMenu(dldata, dp, TOP_LAYER_MASK_GROUP_PARAM);
			break;

		case POPUP_MIDDLE_LAYER_MASK:
			FillPopupMenu(dldata, dp, MIDDLE_LAYER_MASK_GROUP_PARAM);
			break;

		case POPUP_BOTTOM_LAYER_MASK:
			FillPopupMenu(dldata, dp, BOTTOM_LAYER_MASK_GROUP_PARAM);
			break;

		default:
			break;
		}
	}

	return true;
}

// Initializes resources for rendering.
INITRENDERRESULT
DL_Layered::InitRender(BaseMaterial* mat, const InitRenderStruct& irs)
{
	return INITRENDERRESULT::OK;
}

void DL_Layered::CalcSurface(BaseMaterial* mat, VolumeData* vd)
{
	Vector diff, spec;
	vd->IlluminanceSimple(&diff, &spec, 0, 0, 0);
	vd->col = 0.8 * diff;
}

Bool RegisterDLLayered(void)
{
	return RegisterMaterialPlugin(DL_LAYERED,
								  "Layered"_s,
								  PLUGINFLAG_HIDE,
								  DL_Layered::Alloc,
								  "dl_layered"_s,
								  0);
}
