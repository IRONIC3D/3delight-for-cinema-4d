#include "DLBoxNoise_Translator.h"
#include "3DelightEnvironment.h"
#include "dl_box_noise.h"

Delight_Box_Noise::Delight_Box_Noise()
{
	is_3delight_shader = true;
	const char* delightpath = DelightEnv::getDelightEnvironment();
	m_shaderpath =
		delightpath + (std::string) "/osl" + (std::string) "/dlBoxNoise.oso";

	if (m_shaderpath.c_str() && m_shaderpath[0]) {
		m_ids_to_names[Shader_Path] = std::make_pair("", &m_shaderpath[0]);
	}

	m_ids_to_names[BOX_NOISE_SCALE] = std::make_pair("", "scale");
	m_ids_to_names[BOX_NOISE_SCALE_SHADER] =
		std::make_pair("outColor[0]", "scale");
	m_ids_to_names[BOX_NOISE_SMOOTHNESS] = std::make_pair("", "smoothness");
	m_ids_to_names[BOX_NOISE_SMOOTHNESS_SHADER] =
		std::make_pair("outColor[0]", "smoothness");
	m_ids_to_names[BOX_NOISE_TIME] = std::make_pair("", "i_time");
	m_ids_to_names[BOX_NOISE_TIME_SHADER] =
		std::make_pair("outColor[0]", "i_time");
	m_ids_to_names[BOX_NOISE_SPACE] = std::make_pair("", "space");
	m_ids_to_names[BOX_NOISE_COLOR_BLEND] =
		std::make_pair("", "color_blend_mode");
	m_ids_to_names[BOX_NOISE_GRADIENT_COLOR] = std::make_pair("", "color");
	m_ids_to_names[BOX_NOISE_DISTORTION] = std::make_pair("", "distortion");
	m_ids_to_names[BOX_NOISE_DISTORTION_SHADER] =
		std::make_pair("outColor", "distortion");
	m_ids_to_names[BOX_NOISE_DISTORTION_INTENSITY] =
		std::make_pair("", "distortion_intensity");
	m_ids_to_names[BOX_NOISE_DISTORTION_INTENSITY_SHADER] =
		std::make_pair("outColor[0]", "distortion_intensity");
	m_ids_to_names[BOX_NOISE_LAYERS] = std::make_pair("", "layers");
	m_ids_to_names[BOX_NOISE_LAYERS_PERSISTENCE] =
		std::make_pair("", "layer_persistence");
	m_ids_to_names[BOX_NOISE_LAYERS_PERSISTENCE_SHADER] =
		std::make_pair("outColor[0]", "layer_persistence");
	m_ids_to_names[BOX_NOISE_LAYERS_SCALE] = std::make_pair("", "layer_scale");
	m_ids_to_names[BOX_NOISE_LAYERS_SCALE_SHADER] =
		std::make_pair("outColor[0]", "layer_scale");
	m_ids_to_names[BOX_NOISE_ADJUST_INVERT] = std::make_pair("", "invert");
	m_ids_to_names[BOX_NOISE_ADJUST_AMPLITUDE] = std::make_pair("", "amplitude");
	m_ids_to_names[BOX_NOISE_ADJUST_AMPLITUDE_SHADER] =
		std::make_pair("outColor[0]", "amplitude");
	m_ids_to_names[BOX_NOISE_ADJUST_CONTRAST] = std::make_pair("", "contrast");
	m_ids_to_names[BOX_NOISE_ADJUST_CONTRAST_SHADER] =
		std::make_pair("outColor[0]", "contrast");
}
