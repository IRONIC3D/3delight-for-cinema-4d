#include "NSIExportShader.h"
#include "3DelightEnvironment.h"
#include "DL_TypeConversions.h"
#include "DL_Utilities.h"
#include "nsi.hpp"
/*
        NSI_Export_Shader Translater class is the class for exporting C4D
   shaders to NSI shaders. Same as with the material this class with it's
   functions will only be executed as many times as the number of shaders and
   subshader created.

        CreateNSINodes() function will create the NSI shader for the current
   shader with a unique name each time there is a new material and connect it
   with it's corresponding OSL shader containing attributes of the current
   shader.

        ConnectNSINodes() will check for the if any of the used shaders on the
   material have a subshader linked with them.
*/

// Convert Cinema4D Interpolation to the corresponding 3Delight one.
int C4DTo3DelightInterpolation(int c4d_interp)
{
	if (c4d_interp == GRADIENT_INTERPOLATION_NONE) {
		return 0;

	} else if (c4d_interp == GRADIENT_INTERPOLATION_LINEARKNOT) {
		return 1;
	}

	return 2;
}

void NSI_Export_Shader::CreateNSINodes(const char* Handle,
									   const char* ParentTransformHandle,
									   BaseList2D* C4DNode,
									   BaseDocument* doc,
									   DL_SceneParser* parser)
{
	NSI::Context& ctx(parser->GetContext());
	BaseShader* shader = (BaseShader*) C4DNode;

	if (!shader) {
		return;
	}

	std::string shader_handle = (std::string)
								Handle; // parser->GetUniqueName(shader->GetTypeName().GetCStringCopy());
	ctx.Create(shader_handle, "shader");
	// path for shaders used only on C4D
	const char* delightpath = DelightEnv::getDelightEnvironment();
	std::string c_shaderpath =
		delightpath + (std::string) "/cinema4d" + (std::string) "/osl" + (std::string) "/" + (std::string) shader->GetTypeName().GetCStringCopy();

	if (m_ids_to_names.size() > 0) {
		c_shaderpath = m_ids_to_names[0].second;
	}

	ctx.SetAttribute(
		shader_handle,
		NSI::StringArg("shaderfilename", std::string(&c_shaderpath[0])));
}

void NSI_Export_Shader::ConnectNSINodes(const char* Handle,
										const char* ParentTransformHandle,
										BaseList2D* C4DNode,
										BaseDocument* doc,
										DL_SceneParser* parser)
{
	/*
	            In this function we search the applied shaders if they have any
	   other shader connected to them. If so we find which is this subshader and
	   connect it with the parent shader.
	    */
	std::string shader_handle = (std::string) Handle;
	NSI::Context& ctx(parser->GetContext());
	BaseShader* shader = (BaseShader*) C4DNode;

	if (!shader) {
		return;
	}

	BaseContainer* shader_container = shader->GetDataInstance();
	Int32 Id;
	GeData* data = nullptr;
	BrowseContainer browse(shader_container);

	while (browse.GetNext(&Id, &data)) {
		if (data->GetType() != DA_ALIASLINK) {
			continue;
		}

		std::string osl_parameter_name;
		std::string osl_source_attr;

		if (m_ids_to_names.count(Id) == 1) {
			osl_parameter_name = m_ids_to_names[Id].second;
			osl_source_attr = m_ids_to_names[Id].first;

		} else {
			osl_parameter_name = "_" + std::to_string(Id);
			std::vector<float> col = { 1, 1, 1 };
			ctx.SetAttribute(shader_handle,
							 NSI::ColorArg(osl_parameter_name, &col[0]));
			osl_source_attr = "";
		}

		BaseList2D* shaderlink = data->GetLink(doc);

		if (!shaderlink) {
			continue;
		}

		std::string link_shader =
			parser->GetHandleName(shaderlink); // parser->GetAssociatedHandle(shader);
		ctx.Connect(
			link_shader, osl_source_attr, shader_handle, osl_parameter_name);
		//#ifdef VERBOSE
		//	ApplicationOutput("ShaderParent @ Parameter ID @, Shader @",
		// m_shader_handle.c_str(), Id, link_shader.c_str()); #endif // VERBOSE
	}
}

void NSI_Export_Shader::SampleAttributes(DL_SampleInfo* info,
		const char* Handle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser)
{
	NSI::Context& ctx(parser->GetContext());
	BaseShader* shader = (BaseShader*) C4DNode;

	if (!shader) {
		return;
	}

	std::string shader_handle = (std::string) Handle;
	Int32 Id;
	GeData* data = nullptr;
	BaseContainer* shader_container = shader->GetDataInstance();
	BrowseContainer browse(shader_container);
	NSI::ArgumentList args;

	while (browse.GetNext(&Id, &data)) {
		std::string osl_parameter_name = "_" + std::to_string(Id);

		if (m_ids_to_names.count(Id) == 1) {
			osl_parameter_name = m_ids_to_names[Id].second;
		}

		/*
			                Here we avoid setting non existent parameters to
			   3Delight shaders. For supporting Cinema4D starndard shaders we are
			   using _+parameter_id. There are  additional IDs like this on our
			   3Delight shaders, which are not part of the respective osl shader, so
			   we just ignore them to avoid warning messages. Warning messages will
			   show when using C4D built-in shaders since we are only supporting
			   some of it's parameters on some of the shaders (checkerboars, bitmap
			   etc.)
			        */
		else if (is_3delight_shader) {
			continue;
		}

		switch (data->GetType()) {
		case DA_LONG: {
			Int32 value = data->GetInt32();
			args.Add(new NSI::IntegerArg(osl_parameter_name, value));
			break;
		}

		case DA_REAL: { // Float data type
			float float_value = (float) data->GetFloat();
			args.Add(new NSI::FloatArg(osl_parameter_name, float_value));
			break;
		}

		case DA_VECTOR: { // Vector data type
			Vector c4d_vector_value = toLinear(data->GetVector(), doc);
			const float vector_val[3] = { (float) c4d_vector_value.x,
										  (float) c4d_vector_value.y,
										  (float) c4d_vector_value.z
										};
			args.Add(new NSI::ColorArg(osl_parameter_name, &vector_val[0]));
			break;
		}

		case DA_FILENAME: { // Filename. For texture mapping
			Filename texturefile = data->GetFilename();
			Filename texturefile_path;
			GenerateTexturePath(
				doc->GetDocumentPath(), texturefile, Filename(), &texturefile_path);
			std::string texturename =
				StringToStdString(texturefile_path.GetString());
			args.Add(new NSI::StringArg(osl_parameter_name, texturename));
			break;
		}

		case CUSTOMDATATYPE_GRADIENT: {
			if (shader->GetType() == Xbitmap) {
				break;
			}

			GeData shader_data = shader_container->GetData(Id);
			Gradient* gradient =
				(Gradient*) shader_data.GetCustomDataType(CUSTOMDATATYPE_GRADIENT);
			int knot_count = gradient->GetKnotCount();
			float* knots = new float[knot_count];
			float* colors = new float[knot_count * 3];
			int* interpolations = new int[knot_count];

			for (int i = 0; i < knot_count; i++) {
				GradientKnot Knot = gradient->GetKnot(i);
				knots[i] = (float) Knot.pos;
				Vector KnotColor = toLinear((Knot.col), doc);
				colors[i * 3] = (float) KnotColor.x;
				colors[i * 3 + 1] = (float) KnotColor.y;
				colors[i * 3 + 2] = (float) KnotColor.z;
				interpolations[i] = C4DTo3DelightInterpolation(Knot.interpolation);
			}

			args.Add(NSI::Argument::New(osl_parameter_name + "_Position")
					 ->SetArrayType(NSITypeFloat, knot_count)
					 ->CopyValue((float*) &knots[0], knot_count * sizeof(float)));
			args.Add(
				NSI::Argument::New(osl_parameter_name + "_ColorValue")
				->SetArrayType(NSITypeColor, knot_count)
				->CopyValue((float*) &colors[0], knot_count * sizeof(float) * 3));
			args.Add(
				NSI::Argument::New(osl_parameter_name + "_Interp")
				->SetArrayType(NSITypeInteger, knot_count)
				->CopyValue((int*) &interpolations[0], knot_count * sizeof(int)));
			break;
		}

		default:
			break;
		}
	};

	ctx.SetAttribute(shader_handle, args);
}
