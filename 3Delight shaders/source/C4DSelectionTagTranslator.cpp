#include "C4DSelectionTagTranslator.h"
#include "nsi.hpp"

void C4DSelectionTagTranslator::CreateNSINodes(const char* Handle,
		const char* ParentTransformHandle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser)
{
	NSI::Context& ctx(parser->GetContext());
	SelectionTag* selectedTag = (SelectionTag*) C4DNode;
	BaseObject* object = selectedTag->GetObject();
	std::string object_handle = parser->GetHandleName(object);
	std::string handle =
		parser->GetHandleName(selectedTag) + std::string("faceset");
	BaseSelect* bs = selectedTag->GetBaseSelect();
	Int32 seg = 0, a, b, i;
	std::vector<int> faces;

	if (bs) {
		while (bs->GetRange(seg++, LIMIT<Int32>::MAX, &a, &b)) {
			for (i = a; i <= b; ++i) {
				faces.push_back(i);
			}
		}

		if (faces.size() > 0) {
			ctx.Create(handle, "faceset");
			NSI::Argument args("faces");
			args.SetType(NSITypeInteger);
			args.SetCount(faces.size());
			args.SetValuePointer(&faces[0]);
			ctx.SetAttribute(handle, args);

		} else {
			ApplicationOutput("No faces are selected for the Selection Tag " + String(handle.c_str()));
		}
	}
}
