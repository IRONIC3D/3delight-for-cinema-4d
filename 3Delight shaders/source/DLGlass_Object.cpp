#include <DL_Utilities.h>
#include "IDs.h"
#include "c4d.h"

class DL_Glass_command : public CommandData
{
public:
	int shelf_used = 0;
	virtual Bool Execute(BaseDocument* doc, GeDialog* parentManager);
};

Bool DL_Glass_command::Execute(BaseDocument* doc, GeDialog* parentManager)
{
	CreateMaterial(shelf_used, DL_GLASS, doc);
	return true;
}

Bool Register_DlGlass_Object(void)
{
	DL_Glass_command* new_glass = NewObjClear(DL_Glass_command);
	RegisterCommandPlugin(DL_GLASS_COMMAND,
						  "Glass"_s,
						  PLUGINFLAG_HIDEPLUGINMENU | PLUGINFLAG_TOOL_SINGLECLICK,
						  AutoBitmap("shelf_dlGlass_200.png"_s),
						  String("Assign new Glass"),
						  NewObjClear(DL_Glass_command));

	if (RegisterCommandPlugin(DL_GLASS_SHELF_COMMAND,
							  "Glass"_s,
							  PLUGINFLAG_HIDEPLUGINMENU | PLUGINFLAG_TOOL_SINGLECLICK,
							  AutoBitmap("shelf_dlGlass_200.png"_s),
							  String("Assign new Glass Material"),
							  new_glass)) {
		new_glass->shelf_used = 1;
	}

	return true;
}
