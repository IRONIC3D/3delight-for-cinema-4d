#include "DL_Matpreview.h"
#include "IDs.h"
#include "TextureUI_Functions.h"
#include "c4d.h"
#include "c4d_symbols.h"
#include "dl_toon.h"

static void
initGradient(BaseContainer& bc, Int32 id)
{
	GeData data(CUSTOMDATATYPE_GRADIENT, DEFAULTVALUE);
	Gradient* gradient =
		(Gradient*) data.GetCustomDataType(CUSTOMDATATYPE_GRADIENT);

	if (gradient) {
		GradientKnot k1;

		if (id == TOON_COLOR) {
			k1.col = HSVToRGB(Vector(0, 0, 0.0));
			k1.pos = 0.0;
			gradient->InsertKnot(k1);
		}
	}

	bc.SetData(id, data);
}

class DL_Toon : public MaterialData
{
	INSTANCEOF(DL_Toon, MaterialData)

private:
	Vector color;

public:
	virtual Bool Init(GeListNode* node);
	virtual INITRENDERRESULT InitRender(BaseMaterial* mat,
										const InitRenderStruct& irs);
	virtual Bool GetDDescription(GeListNode* node,
								 Description* description,
								 DESCFLAGS_DESC& flags);
	virtual Bool GetDEnabling(GeListNode* node,
							  const DescID& id,
							  const GeData& t_data,
							  DESCFLAGS_ENABLE flags,
							  const BaseContainer* itemdesc);
	virtual Bool Message(GeListNode* node, Int32 type, void* data);
	virtual void CalcSurface(BaseMaterial* mat, VolumeData* vd);
	static NodeData* Alloc()
	{
		return NewObjClear(DL_Toon);
	}
};

Bool DL_Toon::Init(GeListNode* node)
{
	// Set default matpreview scene
	DL_SetDefaultMatpreview((BaseMaterial*) node);
	BaseContainer* data = ((BaseMaterial*) node)->GetDataInstance();
	initGradient(*data, TOON_COLOR);
	data->SetVector(TOON_TINT, HSVToRGB(Vector(0, 0, 1.0)));
	data->SetFloat(TOON_WEIGHT, 1.0);
	data->SetFloat(TOON_OPACITY, 1.0);
	data->SetVector(SPECULAR_COLOR, HSVToRGB(Vector(0, 0, 1)));
	data->SetFloat(SPECULAR_WEIGHT, 1.0);
	data->SetFloat(SPECULAR_ROUGHNESS, 0.3);
	data->SetFloat(OUTLINES_FADE, 1.0);
	data->SetBool(SILHOUETTES_ENABLE, FALSE);
	data->SetVector(SILHOUETTES_COLOR, HSVToRGB(Vector(0, 0, 0.2)));
	data->SetFloat(SILHOUETTES_OPACITY, 1.0);
	data->SetFloat(SILHOUETTES_WIDTH, 1);
	data->SetFloat(SILHOUETTES_SENSITIVITY, 1.0);
	data->SetBool(SILHOUETTES_TINT_COLOR, FALSE);
	data->SetBool(FOLDS_ENABLE, TRUE);
	data->SetVector(FOLDS_COLOR, HSVToRGB(Vector(0, 0, 0.5)));
	data->SetFloat(FOLDS_OPACITY, 1.0);
	data->SetFloat(FOLDS_WIDTH, 1);
	data->SetFloat(FOLDS_SENSITIVITY, 0.3);
	data->SetBool(FOLDS_TINT_COLOR, FALSE);
	data->SetBool(CREASES_ENABLE, FALSE);
	data->SetVector(CREASES_COLOR, HSVToRGB(Vector(0, 0, 0.5)));
	data->SetFloat(CREASES_OPACITY, 1.0);
	data->SetFloat(CREASES_WIDTH, 1);
	data->SetBool(CREASES_TINT_COLOR, FALSE);
	data->SetBool(OBJECTS_ENABLE, FALSE);
	data->SetVector(OBJECTS_COLOR, HSVToRGB(Vector(0, 0, 0.5)));
	data->SetFloat(OBJECTS_OPACITY, 1.0);
	data->SetFloat(OBJECTS_WIDTH, 1);
	data->SetBool(OBJECTS_TINT_COLOR, FALSE);
	data->SetBool(TEXTURE_ENABLE, FALSE);
	data->SetVector(TEXTURE_COLOR_ATTR, HSVToRGB(Vector(0, 0, 0.5)));
	data->SetVector(TEXTURE_FILE_ATTR, HSVToRGB(Vector(0, 0, 0)));
	data->SetFloat(TEXTURE_OPACITY, 1.0);
	data->SetFloat(TEXTURE_WIDTH_ATTR, 1.0);
	data->SetFloat(TEXTURE_SENSITIVITY, 0.1);
	data->SetBool(TEXTURE_TINT_COLOR, FALSE);
	data->SetInt32(BUMP_TYPE, BUMP_MAP);
	data->SetFloat(BUMP_INTENSITY, 1);
	// color = data->GetVector(SIMPLEMATERIAL_COLOR);
	return true;
}

Bool DL_Toon::GetDEnabling(GeListNode* node,
						   const DescID& id,
						   const GeData& t_data,
						   DESCFLAGS_ENABLE flags,
						   const BaseContainer* itemdesc)
{
	if (!node) {
		return false;
	}

	BaseContainer* dldata = ((BaseObject*) node)->GetDataInstance();

	switch (id[0].id) {
	case SILHOUETTES_COLOR:
	case SILHOUETTES_COLOR_SHADER:
	case SILHOUETTES_OPACITY:
	case SILHOUETTES_OPACITY_SHADER:
	case SILHOUETTES_WIDTH:
	case SILHOUETTES_WIDTH_SHADER:
	case SILHOUETTES_SENSITIVITY:
	case SILHOUETTES_SENSITIVITY_SHADER:
	case SILHOUETTES_TINT_COLOR:
		return (dldata->GetBool(SILHOUETTES_ENABLE) == TRUE);
		break;

	case FOLDS_COLOR:
	case FOLDS_COLOR_SHADER:
	case FOLDS_OPACITY:
	case FOLDS_OPACITY_SHADER:
	case FOLDS_WIDTH:
	case FOLDS_WIDTH_SHADER:
	case FOLDS_SENSITIVITY:
	case FOLDS_SENSITIVITY_SHADER:
	case FOLDS_TINT_COLOR:
		return (dldata->GetBool(FOLDS_ENABLE) == TRUE);
		break;

	case CREASES_COLOR:
	case CREASES_COLOR_SHADER:
	case CREASES_OPACITY:
	case CREASES_OPACITY_SHADER:
	case CREASES_WIDTH:
	case CREASES_WIDTH_SHADER:
	case CREASES_TINT_COLOR:
		return (dldata->GetBool(CREASES_ENABLE) == TRUE);
		break;

	case OBJECTS_COLOR:
	case OBJECTS_COLOR_SHADER:
	case OBJECTS_OPACITY:
	case OBJECTS_OPACITY_SHADER:
	case OBJECTS_WIDTH:
	case OBJECTS_WIDTH_SHADER:
	case OBJECTS_TINT_COLOR:
		return (dldata->GetBool(OBJECTS_ENABLE) == TRUE);
		break;

	case TEXTURE_COLOR_ATTR:
	case TEXTURE_COLOR_SHADER:
	case TEXTURE_FILE_ATTR:
	case TEXTURE_FILE_SHADER:
	case TEXTURE_OPACITY:
	case TEXTURE_OPACITY_SHADER:
	case TEXTURE_WIDTH_ATTR:
	case TEXTURE_WIDTH_SHADER:
	case TEXTURE_SENSITIVITY:
	case TEXTURE_SENSITIVITY_SHADER:
	case TEXTURE_TINT_COLOR:
		return (dldata->GetBool(TEXTURE_ENABLE) == TRUE);
		break;

	default:
		break;
	}

	return true;
}

Bool DL_Toon::GetDDescription(GeListNode* node,
							  Description* description,
							  DESCFLAGS_DESC& flags)
{
	description->LoadDescription(DL_TOON);
	flags |= DESCFLAGS_DESC::LOADED;
	BaseContainer* dldata = ((BaseObject*) node)->GetDataInstance();
	HideAndShowTextures(TOON_TINT_GROUP_PARAM,
						TOON_TINT,
						TOON_TINT_SHADER,
						TOON_TINT_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(TOON_WEIGHT_GROUP_PARAM,
						TOON_WEIGHT,
						TOON_WEIGHT_SHADER,
						TOON_WEIGHT_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(TOON_OPACITY_GROUP_PARAM,
						TOON_OPACITY,
						TOON_OPACITY_SHADER,
						TOON_OPACITY_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(SPECULAR_COLOR_GROUP_PARAM,
						SPECULAR_COLOR,
						SPECULAR_COLOR_SHADER,
						SPECULAR_COLOR_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(SPECULAR_WEIGHT_GROUP_PARAM,
						SPECULAR_WEIGHT,
						SPECULAR_WEIGHT_SHADER,
						SPECULAR_WEIGHT_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(SPECULAR_ROUGHNESS_GROUP_PARAM,
						SPECULAR_WEIGHT,
						SPECULAR_WEIGHT_SHADER,
						SPECULAR_WEIGHT_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(OUTLINES_FADE_GROUP_PARAM,
						OUTLINES_FADE,
						OUTLINES_FADE_SHADER,
						OUTLINES_FADE_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(SILHOUETTES_COLOR_GROUP_PARAM,
						SILHOUETTES_COLOR,
						SILHOUETTES_COLOR_SHADER,
						SILHOUETTES_COLOR_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(SILHOUETTES_OPACITY_GROUP_PARAM,
						SILHOUETTES_OPACITY,
						SILHOUETTES_OPACITY_SHADER,
						SILHOUETTES_OPACITY_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(SILHOUETTES_WIDTH_GROUP_PARAM,
						SILHOUETTES_WIDTH,
						SILHOUETTES_WIDTH_SHADER,
						SILHOUETTES_WIDTH_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(SILHOUETTES_SENSITIVITY_GROUP_PARAM,
						SILHOUETTES_SENSITIVITY,
						SILHOUETTES_SENSITIVITY_SHADER,
						SILHOUETTES_SENSITIVITY_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(FOLDS_COLOR_GROUP_PARAM,
						FOLDS_COLOR,
						FOLDS_COLOR_SHADER,
						FOLDS_COLOR_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(FOLDS_OPACITY_GROUP_PARAM,
						FOLDS_OPACITY,
						FOLDS_OPACITY_SHADER,
						FOLDS_OPACITY_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(FOLDS_WIDTH_GROUP_PARAM,
						FOLDS_WIDTH,
						FOLDS_WIDTH_SHADER,
						FOLDS_WIDTH_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(FOLDS_SENSITIVITY_GROUP_PARAM,
						FOLDS_SENSITIVITY,
						FOLDS_SENSITIVITY_SHADER,
						FOLDS_SENSITIVITY_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(CREASES_COLOR_GROUP_PARAM,
						CREASES_COLOR,
						CREASES_COLOR_SHADER,
						CREASES_COLOR_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(CREASES_OPACITY_GROUP_PARAM,
						CREASES_OPACITY,
						CREASES_OPACITY_SHADER,
						CREASES_OPACITY_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(CREASES_WIDTH_GROUP_PARAM,
						CREASES_WIDTH,
						CREASES_WIDTH_SHADER,
						CREASES_WIDTH_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(OBJECTS_COLOR_GROUP_PARAM,
						OBJECTS_COLOR,
						OBJECTS_COLOR_SHADER,
						OBJECTS_COLOR_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(OBJECTS_OPACITY_GROUP_PARAM,
						OBJECTS_OPACITY,
						OBJECTS_OPACITY_SHADER,
						OBJECTS_OPACITY_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(OBJECTS_WIDTH_GROUP_PARAM,
						OBJECTS_WIDTH,
						OBJECTS_WIDTH_SHADER,
						OBJECTS_WIDTH_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(TEXTURE_COLOR_GROUP_PARAM,
						TEXTURE_COLOR_ATTR,
						TEXTURE_COLOR_SHADER,
						TEXTURE_COLOR_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(TEXTURE_FILE_GROUP_PARAM,
						TEXTURE_FILE_ATTR,
						TEXTURE_FILE_SHADER,
						TEXTURE_FILE_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(TEXTURE_OPACITY_GROUP_PARAM,
						TEXTURE_OPACITY,
						TEXTURE_OPACITY_SHADER,
						TEXTURE_OPACITY_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(TEXTURE_WIDTH_GROUP_PARAM,
						TEXTURE_WIDTH_ATTR,
						TEXTURE_WIDTH_SHADER,
						TEXTURE_WIDTH_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(TEXTURE_SENSITIVITY_GROUP_PARAM,
						TEXTURE_SENSITIVITY,
						TEXTURE_SENSITIVITY_SHADER,
						TEXTURE_SENSITIVITY_SHADER_TEMP,
						node,
						description,
						dldata);
	return TRUE;
}

Bool DL_Toon::Message(GeListNode* node, Int32 type, void* data)
{
	if (!node) {
		return false;
	}

	BaseContainer* dldata = ((BaseMaterial*) node)->GetDataInstance();

	if (type == MSG_DESCRIPTION_POPUP) {
		DescriptionPopup* dp = (DescriptionPopup*) data;
		int clicked_button_id = dp->_descId[0].id;

		switch (clicked_button_id) {
		case POPUP_TOON_TINT:
			FillPopupMenu(dldata, dp, TOON_TINT_GROUP_PARAM);
			break;

		case POPUP_TOON_WEIGHT:
			FillPopupMenu(dldata, dp, TOON_WEIGHT_GROUP_PARAM);
			break;

		case POPUP_TOON_OPACITY:
			FillPopupMenu(dldata, dp, TOON_OPACITY_GROUP_PARAM);
			break;

		case POPUP_SPECULAR_COLOR:
			FillPopupMenu(dldata, dp, SPECULAR_COLOR_GROUP_PARAM);
			break;

		case POPUP_SPECULAR_WEIGHT:
			FillPopupMenu(dldata, dp, SPECULAR_WEIGHT_GROUP_PARAM);
			break;

		case POPUP_SPECULAR_ROUGHNESS:
			FillPopupMenu(dldata, dp, SPECULAR_ROUGHNESS_GROUP_PARAM);
			break;

		case POPUP_OUTLINES_FADE:
			FillPopupMenu(dldata, dp, OUTLINES_FADE_GROUP_PARAM);
			break;

		case POPUP_SILHOUETTES_COLOR:
			FillPopupMenu(dldata, dp, SILHOUETTES_COLOR_GROUP_PARAM);
			break;

		case POPUP_SILHOUETTES_OPACITY:
			FillPopupMenu(dldata, dp, SILHOUETTES_OPACITY_GROUP_PARAM);
			break;

		case POPUP_SILHOUETTES_WIDTH:
			FillPopupMenu(dldata, dp, SILHOUETTES_WIDTH_GROUP_PARAM);
			break;

		case POPUP_SILHOUETTES_SENSITIVITY:
			FillPopupMenu(dldata, dp, SILHOUETTES_SENSITIVITY_GROUP_PARAM);
			break;

		case POPUP_FOLDS_COLOR:
			FillPopupMenu(dldata, dp, FOLDS_COLOR_GROUP_PARAM);
			break;

		case POPUP_FOLDS_OPACITY:
			FillPopupMenu(dldata, dp, FOLDS_OPACITY_GROUP_PARAM);
			break;

		case POPUP_FOLDS_WIDTH:
			FillPopupMenu(dldata, dp, FOLDS_WIDTH_GROUP_PARAM);
			break;

		case POPUP_FOLDS_SENSITIVITY:
			FillPopupMenu(dldata, dp, FOLDS_SENSITIVITY_GROUP_PARAM);
			break;

		case POPUP_CREASES_COLOR:
			FillPopupMenu(dldata, dp, CREASES_COLOR_GROUP_PARAM);
			break;

		case POPUP_CREASES_OPACITY:
			FillPopupMenu(dldata, dp, CREASES_OPACITY_GROUP_PARAM);
			break;

		case POPUP_CREASES_WIDTH:
			FillPopupMenu(dldata, dp, CREASES_WIDTH_GROUP_PARAM);
			break;

		case POPUP_OBJECTS_COLOR:
			FillPopupMenu(dldata, dp, OBJECTS_COLOR_GROUP_PARAM);
			break;

		case POPUP_OBJECTS_OPACITY:
			FillPopupMenu(dldata, dp, OBJECTS_OPACITY_GROUP_PARAM);
			break;

		case POPUP_OBJECTS_WIDTH:
			FillPopupMenu(dldata, dp, OBJECTS_WIDTH_GROUP_PARAM);
			break;

		case POPUP_TEXTURE_COLOR:
			FillPopupMenu(dldata, dp, TEXTURE_COLOR_GROUP_PARAM);
			break;

		case POPUP_TEXTURE_FILE:
			FillPopupMenu(dldata, dp, TEXTURE_FILE_GROUP_PARAM);
			break;

		case POPUP_TEXTURE_OPACITY:
			FillPopupMenu(dldata, dp, TEXTURE_OPACITY_GROUP_PARAM);
			break;

		case POPUP_TEXTURE_WIDTH:
			FillPopupMenu(dldata, dp, TEXTURE_WIDTH_GROUP_PARAM);
			break;

		case POPUP_TEXTURE_SENSITIVITY:
			FillPopupMenu(dldata, dp, TEXTURE_SENSITIVITY_GROUP_PARAM);
			break;

		default:
			break;
		}
	}

	return true;
}

// Initializes resources for rendering.
INITRENDERRESULT
DL_Toon::InitRender(BaseMaterial* mat, const InitRenderStruct& irs)
{
	return INITRENDERRESULT::OK;
}

void DL_Toon::CalcSurface(BaseMaterial* mat, VolumeData* vd)
{
	Vector diff, spec;
	vd->IlluminanceSimple(&diff, &spec, 0, 0, 0);
	vd->col = 0.8 * diff;
}

Bool RegisterDLToon(void)
{
	return RegisterMaterialPlugin(
			   DL_TOON, "Toon"_s, PLUGINFLAG_HIDE, DL_Toon::Alloc, "dl_toon"_s, 0);
}
